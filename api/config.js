const credentials = require("./cre/googleCredentials.json");
const conJSON = require("./config.json");

// Combine config.json and config.js together

/** 
 * Path in AWS S3
 * **/
const s3_dir = {
    p_img: {
        name: "p_img",
        original: 'p_img/o/',
        thumb: 'p_img/t/'
    },
    static_image: {
        name: 's_img',
        icon: 's_img/icon/',
        home: {
            name: 's_img/home',
            slider: 's_img/home/s/',
            body: 's_img/home/b/',
            test: 's_img/home/t/'
        },
        error: 's_img/error/'
    }
}

exports.s3_dir = s3_dir;

/** 
 * Path in server
 * **/
exports.server_dir = {
    tmp: "tmp/"
}

/** 
 * URL in client side
 * **/
const static_base_url = conJSON.static_base_url + "/";

exports.url = {
    static_base_url: static_base_url,
    uploaded_img: {
        pro: {
            original: static_base_url + s3_dir.p_img.original,
            thumb: static_base_url + s3_dir.p_img.thumb
        }
    },
    client_side_static: {
        img: {
            icon: {
                bed: static_base_url + s3_dir.static_image.icon + "bedroom.svg",
                bath: static_base_url + s3_dir.static_image.icon + "bathroom.svg",
                gar: static_base_url + s3_dir.static_image.icon + "garage.svg"
            },
            home: {
                slider: buildStaticImgUrl(static_base_url + s3_dir.static_image.home.slider, ["s1.JPG", "s2.jpg", "s3.jpg"]),
                body: buildStaticImgUrl(static_base_url + s3_dir.static_image.home.body, ["b1.jpg", "b2.jpg", "b3.jpg"]),
                test: buildStaticImgUrl(static_base_url + s3_dir.static_image.home.test, ["t1.jpg", "t2.jpg", "t3.jpg"])
            },
            error: static_base_url + s3_dir.static_image.error + "error.jpg"
        },
        code: {
            icon: { url: '/favicon.ico', path: '/favicon.ico' },
            lib: { url: '/lib', path: '/lib' },
            css: { url: '/css', path: '/css' },
            js: { url: '/js', path: '/js' },
            error: { url: '/error', path: '/error.html' },
            home: { url: '/', path: '/index.html' },
            login: { url: '/login', path: '/login.html' },
            listing: { url: '/listing', path: '/listingProperty.html' },
            proDetail: { url: '/proDetail', path: '/propertyDetail.html' },
            profile: { url: '/profile', path: '/userProfile.html' },
            search: { url: '/search', path: '/search.html' },
            search1: { url: '/search1', path: '/search1.html' },
            search2: { url: '/search2', path: '/search2.html' }
        }
    },
    docs: static_base_url
}

exports.permission = {
    "admin": 0,
    "seller": 1,
    "buyer": 2
};


exports.auth_id = {
    "home": 0,
    "google": 1,
    "facebook": 2
};

exports.img_size = {
    thumb: {
        width: 250,
        height: 250
    }
};

exports.max_pro_img_num = 10;

exports.cros = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "allowedHeaders": ["X-Requested-With", "content-type", "authorization"],
    "preflightContinue": false,
    "optionsSuccessStatus": 200,
    // "credentials": true,
};

exports.google_api = {
    client_id: credentials.web.client_id,
    iss_values: ["accounts.google.com", "https://accounts.google.com"]
};

exports.mysql = conJSON.mysql;

exports.aws = conJSON.aws;
exports.aws.dynamodb.table = {
    "loginStatus": {
        "name": "loginStatus",
        "key": "user_encrypt_key"
    },
    "verifyCode": {
        "name": "verifyCode",
        "key": "email"
    }
}
exports.ttl = {
    "session": {
        "day": 1,
        "hour": 0,
        "min": 0
    },
    "verfyCode": {
        "day": 0,
        "hour": 0,
        "min": 1
    }
}


exports.email = conJSON.email
exports.email.template = {
    "head": "<h1>Homely</h1>",
    "new_property": {
        "subject": "Homely new property online"
    },
    "forgot_pass": {
        "subject": "Homely change password varification",
        "html_insert_index": 1,
        "html": ["<p>You varification code is", "use this code to set a new password.</p>"]
    },
    "register": {
        "subject": "Homely registration varification",
        "html_insert_index": 1,
        "html": ["<p>You varification code is", "use this code to verify your email address.</p>"]
    }
    ,
    "register_as_agent": {
        "to_admin": {
            "subject": "Homely agent registration",
            "html_insert_index": 0,
            "html": ["<p>wants to register as an agent</p>"]
        },
        "accept": {
            "subject": "Homely registration accepted",
            "html_insert_index": 0,
            "html": ["<p>Your registration as agent has been approved!</p>"]
        },
        "decline": {
            "subject": "Homely registration declined",
            "html_insert_index": 0,
            "html": ["<p>Your registration as agent has been declined!</p>"]
        }
    },
    "register_as_landlord": {
        "to_admin": {
            "subject": "Homely landlord registration",
            "html_insert_index": 0,
            "html": ["<p>wants to register as a landlord</p>"]
        },
        "accept": {
            "subject": "Homely registration accepted",
            "html_insert_index": 0,
            "html": ["<p>Your registration as landlord has been approved!</p>"]
        },
        "decline": {
            "subject": "Homely registration declined",
            "html_insert_index": 0,
            "html": ["<p>Your registration as landlord has been declined!</p>"]
        }
    }
}

function buildStaticImgUrl(bas_url, Imgs) {
    var data = [];
    Imgs.forEach(element => {
        data.push(bas_url + element);
    });
    return data;
}