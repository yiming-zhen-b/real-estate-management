const uuidv1 = require('uuid/v1');
const dateTime = require('node-datetime');

for(var i = 0; i < 12; i ++){
    var v1options = {
        node: [0x01, 0x23, 0x45, 0x67, 0x89, 0xab],
        clockseq: i,
        msecs: dateTime.create(),
        nsecs: i
      };
    console.log(uuidv1(v1options)); // ⇨ '45745c60-7b1a-11e8-9c9c-2d42b21b1a3e'
}