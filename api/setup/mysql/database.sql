CREATE DATABASE  IF NOT EXISTS `real_estate_management` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `real_estate_management`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: real_estate_management
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additional_product`
--

DROP TABLE IF EXISTS `additional_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `additional_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(45) NOT NULL,
  `product_cost` decimal(4,2) NOT NULL,
  `product_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `additional_product`
--

LOCK TABLES `additional_product` WRITE;
/*!40000 ALTER TABLE `additional_product` DISABLE KEYS */;
INSERT INTO `additional_product` VALUES (1,'Feature combo',40.00,NULL),(2,'Feature',30.00,NULL),(3,'Hightlight',20.00,NULL),(4,'Bold title',20.00,NULL);
/*!40000 ALTER TABLE `additional_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `entity_id` int(11) NOT NULL,
  `licence_number` varchar(1000) NOT NULL,
  `on_hold` enum('true','false') NOT NULL DEFAULT 'true',
  PRIMARY KEY (`entity_id`),
  CONSTRAINT `entity_agent_fk` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent`
--

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
INSERT INTO `agent` VALUES (2,'asdwadsda','false'),(3,'asdqwer','false');
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL,
  PRIMARY KEY (`category_id`,`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Apartment'),(3,'House'),(4,'Townhouse'),(5,'Unit');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(45) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `region_id_fk_idx` (`region_id`),
  CONSTRAINT `region_id_fk` FOREIGN KEY (`region_id`) REFERENCES `region` (`region_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Far North',1),(2,'Kaipara',1),(3,'Whangarei',1),(4,'Auckland City',2),(5,'Franklin',2),(6,'Hauraki Gulf Islands',2),(7,'Manukau City',2),(8,'North Shore City',2),(9,'Papakura',2),(10,'Rodney',2),(11,'Waiheke Island',2),(12,'Waitakere City',2),(13,'Hamilton',3),(14,'Hauraki',3),(15,'Matamata-Piako',3),(16,'Otorohanga',3),(17,'South Waikato',3),(18,'Taupo',3),(19,'Thames-Coromandel',3),(20,'Waikato',3),(21,'Waipa',3),(22,'Waitomo',3),(23,'Kawerau',4),(24,'Opotiki',4),(25,'Rotorua',4),(26,'Tauranga',4),(27,'Western Bay Of Plenty',4),(28,'Whakatane',4),(29,'Gisborne',5),(30,'Central Hawke\'s Bay',6),(31,'Hastings',6),(32,'Napier',6),(33,'Wairoa',6),(34,'New Plymouth',7),(35,'South Taranaki',7),(36,'Stratford',7),(37,'Horowhenua',8),(38,'Manawatu',8),(39,'Palmerston North',8),(40,'Rangitikei',8),(41,'Ruapehu',8),(42,'Tararua',8),(43,'Wanganui',8),(44,'Carterton',9),(45,'Kapiti Coast',9),(46,'Lower Hutt',9),(47,'Masterton',9),(48,'Porirua',9),(49,'South Wairarapa',9),(50,'Upper Hutt',9),(51,'Wellington',9),(52,'Nelson',10),(53,'Tasman',10),(54,'Blenheim',11),(55,'Kaikoura',11),(56,'Marlborough',11),(57,'Buller',12),(58,'Grey',12),(59,'Westland',12),(60,'Ashburton',13),(61,'Banks Peninsula',13),(62,'Christchurch City',13),(63,'Hurunui',13),(64,'Mackenzie',13),(65,'Selwyn',13),(66,'Timaru',13),(67,'Waimakariri',13),(68,'Waimate',13),(69,'Central Otago',14),(70,'Clutha',14),(71,'Dunedin',14),(72,'Queenstown-Lakes',14),(73,'South Otago',14),(74,'Waitaki',14),(75,'Wanaka',14),(76,'Catlins',15),(77,'Gore',15),(78,'Invercargill',15),(79,'Southland',15);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `attr_name` varchar(100) NOT NULL,
  `int_value` int(11) DEFAULT '0',
  PRIMARY KEY (`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('max_bath_num',10),('max_bed_num',10),('max_rent',1000);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `originalname` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `document_entity_fk_idx` (`entity_id`),
  KEY `document_type_fk_idx` (`document_type_id`),
  CONSTRAINT `document_entity_fk` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `document_type_fk` FOREIGN KEY (`document_type_id`) REFERENCES `documents_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (142,164,2,'84ad60d88b12c6cd2ecf65cdfdb583bd5fcacd6f24e0c406af6720cf4f00c575.jpeg','WechatIMG81.jpeg'),(143,164,2,'e72b2161179d9f6d1abd3de6d5e97b3516a58e8c71b645d5ec15965160ff07e8.jpeg','WechatIMG82.jpeg'),(144,164,3,'1a86f4fe88ec64ad11bfa2449117c3ec892346419f31a9ad26880998ff1fe9a0.pdf','CV FULL.pdf'),(145,164,4,'cd2241488bd941c88a915df4ded9c56e5244ba468b0fda56194f7639691dfa27.json','package-lock.json'),(146,164,4,'2cd967aa7bbc2a155a8d38967117c7e1b2edbe3162d5e5ee56ae85c87c062d5e.bin','hs_err_pid38122.log'),(147,164,1,'a7530d9979b1827a350c7a77df10743ec1e46e8eaaad624692598b3f4f76b4de.jpeg','63A44DFC-4BD8-4058-A910-41A2DB3C6CFC.JPG');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_types`
--

DROP TABLE IF EXISTS `documents_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `max_value` int(11) NOT NULL,
  `valid_extname` varchar(45) NOT NULL,
  `s3_path` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_types`
--

LOCK TABLES `documents_types` WRITE;
/*!40000 ALTER TABLE `documents_types` DISABLE KEYS */;
INSERT INTO `documents_types` VALUES (1,'Personal photo',10,'.png,.jpg,.gif,.bmp,.jpeg,.pdf','docs/personal/'),(2,'Licence photo',2,'.png,.jpg,.gif,.bmp,.jpeg,.pdf','docs/licence/'),(3,'CV',10,'.png,.jpg,.gif,.bmp,.jpeg,.doc,.docx,.pdf','docs/cv/'),(4,'Other',50,'','docs/other/');
/*!40000 ALTER TABLE `documents_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity`
--

DROP TABLE IF EXISTS `entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_token` varchar(1000) DEFAULT '',
  `email_address` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(45) DEFAULT '',
  `is_admin` enum('true','false') NOT NULL DEFAULT 'false',
  `is_landlord` enum('true','false') NOT NULL DEFAULT 'false',
  `is_buyer` enum('true','false') NOT NULL DEFAULT 'false',
  `is_tenant` enum('true','false') DEFAULT 'false',
  `is_agent` enum('true','false') NOT NULL DEFAULT 'false',
  `password` varchar(45) NOT NULL,
  `img_url` varchar(45) DEFAULT '',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auth_id` int(11) DEFAULT '0',
  `mobile_number` varchar(45) DEFAULT '',
  `landline_number` varchar(45) DEFAULT '',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`),
  UNIQUE KEY `email_address_UNIQUE` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity`
--

LOCK TABLES `entity` WRITE;
/*!40000 ALTER TABLE `entity` DISABLE KEYS */;
INSERT INTO `entity` VALUES (2,'d30619464ede7c6bccd46d4180dc8255a141e249df1017be14936a3ada3b436b2f5ae223da9e2cac9efcbedd415e308008d1d39ef86efd896830c14c009cb038237f56996d28b7c8328f5a3137c0b2ccac1e1cfd5d7b0be361192e31fdd52a3d854ffcbf7bca9b26ae8c9c05a6953f85082ede6d13835d94449d1323bd770033893a8ab0f93a123e2b6fafb0fbca6f0b973053eed38d53d465bc0fc475d0738ae5965dd8aac74ef0d4375d61c7bd4c347d92d280d4cf2129a56693062605fcef9934eacc108ae6ec0d89c1ae9ff5597cd38a169b3ef9211f7608e1034424d3c5598610b0356211ec03aa6e8150803fe24dec76c4eea8f9c5413ed5d55b666e8bc0a04e5d3d6517009141204c1d739a0d','jordie.messiter@gmail.com','Jordan','Messiter','true','false','false','false','true','jordan123','','2019-02-08 15:22:46',0,'',''),(3,'','owenmooneynz@gmail.com','Owne','Mooney','true','false','false','false','true','owen123','','2019-02-08 15:25:59',0,'',''),(164,'','yiming.zhen.app@gmail.com','Yiming','Zhen','false','false','false','true','false','111','','2019-02-20 15:15:58',2,'274281217','0274281217'),(165,'','smmw999@gmail.com','Yiming','Zhen','false','false','false','true','false','111','','2019-02-20 15:23:53',1,'','');
/*!40000 ALTER TABLE `entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lanlord`
--

DROP TABLE IF EXISTS `lanlord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lanlord` (
  `entity_id` int(11) NOT NULL,
  `on_hold` enum('false','true') NOT NULL DEFAULT 'true',
  PRIMARY KEY (`entity_id`),
  CONSTRAINT `landlord_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lanlord`
--

LOCK TABLES `lanlord` WRITE;
/*!40000 ALTER TABLE `lanlord` DISABLE KEYS */;
/*!40000 ALTER TABLE `lanlord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_for` enum('sale','rent') NOT NULL DEFAULT 'rent',
  `category_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `disabled` enum('true','false') DEFAULT 'false',
  `show_landline` enum('true','false') NOT NULL DEFAULT 'false',
  `show_mobile` enum('true','false') NOT NULL DEFAULT 'false',
  `agent_id` int(11) DEFAULT NULL,
  `rent_pw` decimal(15,2) NOT NULL,
  `street_number` varchar(45) DEFAULT '',
  `unit_flat_number` varchar(45) DEFAULT '',
  `street_name` varchar(45) DEFAULT '',
  `suburb_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `available` varchar(45) NOT NULL,
  `listed_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bedrooms` int(11) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `max_tenants` int(11) DEFAULT NULL,
  `pets_allowed` enum('don`t know','yes','no','negotiable') DEFAULT 'don`t know',
  `smoking_allowed` enum('don`t know','yes','no','negotiable') DEFAULT 'don`t know',
  `smoke_alarm` enum('don`t know','yes','no') DEFAULT 'don`t know',
  `dishwasher` enum('don`t know','yes','no') DEFAULT 'don`t know',
  `heating` varchar(100) DEFAULT NULL,
  `fibre` varchar(100) DEFAULT NULL,
  `parking` varchar(4000) DEFAULT NULL,
  `details` varchar(4000) DEFAULT NULL,
  `contact_time` varchar(45) DEFAULT NULL,
  `furnishings` varchar(4000) DEFAULT NULL COMMENT '0\n',
  `amenities` varchar(4000) DEFAULT NULL,
  `ideal_tenants` varchar(4000) DEFAULT NULL,
  `notes` varchar(4000) DEFAULT NULL,
  `video_tour_link` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`property_id`),
  UNIQUE KEY `property_address` (`street_number`,`unit_flat_number`,`street_name`,`suburb_id`,`city_id`,`region_id`),
  KEY `seller_id` (`entity_id`),
  KEY `category_id` (`category_id`),
  KEY `suburb_idfk_idx` (`suburb_id`),
  KEY `city_idfk_idx` (`city_id`),
  KEY `region_idfk_idx` (`region_id`),
  CONSTRAINT `property_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  CONSTRAINT `property_city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `property_entity_id_fk` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `property_region_id_fk` FOREIGN KEY (`region_id`) REFERENCES `region` (`region_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `property_suburb_id_fk` FOREIGN KEY (`suburb_id`) REFERENCES `suburb` (`suburb_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property`
--

LOCK TABLES `property` WRITE;
/*!40000 ALTER TABLE `property` DISABLE KEYS */;
INSERT INTO `property` VALUES (1,'rent',1,2,'false','false','false',1,380.00,'317B',NULL,'Cobham Drive',551,13,3,'26/01/2018','2019-02-01 19:31:47',2,1,NULL,'yes','yes','no','don`t know',NULL,NULL,'Single garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1842704224.htm?rsqid=150b0ca8f52a4e49b1a34b5c5ebf3302'),(2,'rent',3,2,'false','false','false',NULL,540.00,'2',NULL,'Commodore Avenue',540,13,3,'18/01/2018','2019-02-02 19:31:47',3,2,NULL,'no','no','don`t know','don`t know',NULL,NULL,'Double garage',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'rent',3,2,'false','false','false',NULL,380.00,'317A',NULL,'Cobham Drive',551,13,3,'26/01/2018','2019-02-03 19:31:47',2,1,3,'no','yes','no','no','Heatpump','don`t know','Single garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1842704224.htm?rsqid=150b0ca8f52a4e49b1a34b5c5ebf3302'),(4,'rent',3,2,'false','false','false',NULL,540.00,'1',NULL,'Commodore Avenue',540,13,3,'18/01/2018','2019-02-03 19:31:47',3,2,NULL,'no','no','yes','yes','Heatpump','don`t know','Double garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/Browse/Listing.aspx?id=1835774072&rsqid=2683932b8dab4de8809482330452e57b'),(5,'rent',3,2,'false','false','false',NULL,575.00,'6',NULL,'Wake Street',531,13,3,'28/11/2018','2019-02-03 19:31:47',4,5,NULL,'no','no','no','yes','Gas heater','Yes','Double garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1834432928.htm?rsqid=8404db952c1848659d607ee9f8b025ae'),(6,'rent',3,2,'false','false','false',NULL,540.00,'5/3',NULL,'Charlemont Street',545,13,3,'16/11/2018','2019-02-01 19:31:47',3,3,4,'no','don`t know','don`t know','don`t know','Heatpump','don`t know','Double garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1840146201.htm?rsqid=79d9d7833bfe4264b795025d3fbca0be'),(7,'sale',3,3,'false','false','false',NULL,470.00,'26',NULL,'Peachgrove Road',546,13,3,'23/11/2018','2019-01-03 19:31:47',3,1,4,'no','no','don`t know','yes','Fireplace','don`t know','Off street	',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1842745033.htm?rsqid=73c5c3296a384c798faa31078cad8fb1'),(8,'rent',3,3,'false','false','false',NULL,495.00,'13',NULL,'Hibiscus Avenue',547,13,3,'01/12/2018','2018-02-03 19:31:47',3,2,4,'no','no','don`t know','yes','Gas heater','don`t know','Off street	',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1826848278.htm?rsqid=8c7c843a873846249deed1d9643937e3'),(9,'rent',3,3,'false','false','false',NULL,300.00,'3/17',NULL,'May Street',546,13,3,'27/01/2018','2019-02-02 19:31:47',2,1,3,'no','no','don`t know','no','Heatpump','don`t know','Off street	',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1835780223.htm?rsqid=2456756ff7dc43b0a573c98f113297c6'),(10,'rent',3,3,'false','false','false',NULL,480.00,'12',NULL,'Balmerino Crescent',558,13,3,'15/12/2018','2017-02-03 19:31:47',4,1,4,'negotiable','don`t know','don`t know','don`t know','Heatpump','don`t know','Double garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1842881092.htm?rsqid=d37aac00422e483582498bcd052b3f7f'),(11,'sale',3,2,'false','false','false',NULL,640.00,'10A',NULL,'Riverlea Road',551,13,3,'21/12/2018','2015-02-03 19:31:47',4,2,NULL,'yes','no','don`t know','yes','Gas fire','don`t know','Double garage',NULL,NULL,NULL,NULL,NULL,NULL,'https://www.trademe.co.nz/property/residential-property-to-rent/auction-1835668202.htm?rsqid=d37aac00422e483582498bcd052b3f7f');
/*!40000 ALTER TABLE `property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_image`
--

DROP TABLE IF EXISTS `property_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_image` (
  `img_id` int(1) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT NULL,
  `org_img` varchar(1000) NOT NULL,
  `thumb_img` varchar(1000) NOT NULL,
  `caption` varchar(100) DEFAULT NULL,
  `img_type` int(11) NOT NULL DEFAULT '0',
  `is_cover` enum('true','false') NOT NULL DEFAULT 'false',
  `originalname` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  KEY `type_fk_idx` (`img_type`),
  KEY `property_if_fk_idx` (`property_id`),
  CONSTRAINT `property_if_fk` FOREIGN KEY (`property_id`) REFERENCES `property` (`property_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `type_fk` FOREIGN KEY (`img_type`) REFERENCES `property_image_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=528 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_image`
--

LOCK TABLES `property_image` WRITE;
/*!40000 ALTER TABLE `property_image` DISABLE KEYS */;
INSERT INTO `property_image` VALUES (6,2,'1A.jpg','1A.jpg','',0,'true',NULL),(7,2,'1B.jpg','1B.jpg',NULL,0,'',NULL),(8,2,'1C.jpg','1C.jpg',NULL,0,'',NULL),(9,2,'1D.jpg','1D.jpg','',0,'false',NULL),(10,2,'1E.jpg','1E.jpg',NULL,0,'',NULL),(11,3,'2A.jpg','2A.jpg',NULL,0,'true',NULL),(12,3,'2B.jpg','2B.jpg',NULL,0,'',NULL),(13,3,'2C.jpg','2C.jpg',NULL,0,'',NULL),(14,3,'2D.jpg','2D.jpg',NULL,0,'',NULL),(15,3,'2E.jpg','2E.jpg',NULL,0,'',NULL),(16,4,'3A.jpg','3A.jpg','',0,'true',NULL),(17,4,'3B.jpg','3B.jpg','',0,'false',NULL),(18,4,'3C.jpg','3C.jpg','',0,'false',NULL),(19,4,'3D.jpg','3D.jpg','',0,'false',NULL),(20,4,'3E.jpg','3E.jpg','',0,'false',NULL),(26,6,'5A.jpg','5A.jpg','',0,'true',NULL),(27,6,'5B.jpg','5B.jpg',NULL,0,'',NULL),(28,6,'5C.jpg','5C.jpg','',0,'false',NULL),(29,6,'5D.jpg','5D.jpg',NULL,0,'',NULL),(30,7,'6A.jpg','6A.jpg',NULL,0,'true',NULL),(31,7,'6B.jpg','6B.jpg',NULL,0,'',NULL),(32,7,'6C.jpg','6C.jpg',NULL,0,'',NULL),(33,7,'6D.jpg','6D.jpg',NULL,0,'',NULL),(34,7,'6E.jpg','6E.jpg',NULL,0,'',NULL),(35,5,'7A.jpg','7A.jpg','',0,'true',NULL),(36,5,'7B.jpg','7B.jpg',NULL,0,'',NULL),(37,5,'7C.jpg','7C.jpg',NULL,0,'',NULL),(38,5,'7D.jpg','7D.jpg',NULL,0,'',NULL),(39,5,'7E.jpg','7E.jpg',NULL,0,'',NULL),(40,8,'8A.jpg','8A.jpg',NULL,0,'true',NULL),(41,8,'8B.jpg','8B.jpg',NULL,0,'',NULL),(42,8,'8C.jpg','8C.jpg',NULL,0,'',NULL),(43,8,'8D.jpg','8D.jpg',NULL,0,'',NULL),(44,8,'8E.jpg','8E.jpg',NULL,0,'',NULL),(45,9,'9A.jpg','9A.jpg',NULL,0,'true',NULL),(46,9,'9B.jpg','9B.jpg',NULL,0,'',NULL),(47,9,'9C.jpg','9C.jpg',NULL,0,'',NULL),(48,9,'9D.jpg','9D.jpg',NULL,0,'',NULL),(49,9,'9E.jpg','9E.jpg',NULL,0,'',NULL),(50,10,'4A.jpg','4A.jpg',NULL,0,'true',NULL),(51,10,'4B.jpg','4B.jpg',NULL,0,'',NULL),(52,10,'4C.jpg','4C.jpg',NULL,0,'',NULL),(53,10,'4D.jpg','4D.jpg',NULL,0,'',NULL),(54,10,'4E.jpg','4E.jpg',NULL,0,'',NULL),(55,11,'6A.jpg','6A.jpg',NULL,0,'true',NULL),(56,11,'6B.jpg','6B.jpg',NULL,0,'',NULL),(57,11,'6C.jpg','6C.jpg',NULL,0,'',NULL),(58,11,'6D.jpg','6D.jpg',NULL,0,'',NULL),(59,11,'6E.jpg','6E.jpg',NULL,0,'',NULL),(67,1,'8A.jpg','8A.jpg','SSS',0,'true',NULL),(68,1,'8B.jpg','8B.jpg','DDD',0,'',NULL),(69,1,'8C.jpg','8C.jpg','EEE',0,'',NULL),(70,1,'8D.jpg','8D.jpg','CCC',0,'',NULL),(71,1,'8E.jpg','8E.jpg','DDD',0,'',NULL),(72,1,'8H.jpeg','8H.jpeg','ASDASDASD',1,'',NULL),(73,1,'8F.jpg','8F.jpg','T',2,'',NULL),(74,1,'8G.jpg','8G.jpg','J',2,'',NULL);
/*!40000 ALTER TABLE `property_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_image_type`
--

DROP TABLE IF EXISTS `property_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_image_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_image_type`
--

LOCK TABLES `property_image_type` WRITE;
/*!40000 ALTER TABLE `property_image_type` DISABLE KEYS */;
INSERT INTO `property_image_type` VALUES (0,'Normal'),(1,'Panoramas'),(2,'Photosphere');
/*!40000 ALTER TABLE `property_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(45) NOT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'Northland'),(2,'Auckland'),(3,'Waikato'),(4,'Bay Of Plenty'),(5,'Gisborne'),(6,'Hawke\'s Bay'),(7,'Taranaki'),(8,'Manawatu / Wanganui'),(9,'Wellington'),(10,'Nelson / Tasman'),(11,'Marlborough'),(12,'West Coast'),(13,'Canterbury'),(14,'Otago'),(15,'Southland');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `room_name` varchar(45) NOT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `normal_photo_link` varchar(45) DEFAULT NULL,
  `panorrama_link` varchar(45) DEFAULT NULL,
  `photo_share_link` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`property_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `school_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `schoold_name` varchar(45) NOT NULL,
  PRIMARY KEY (`school_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `school_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school`
--

LOCK TABLES `school` WRITE;
/*!40000 ALTER TABLE `school` DISABLE KEYS */;
/*!40000 ALTER TABLE `school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller` (
  `entity_id` int(11) NOT NULL,
  `licenced_agent` varchar(45) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `on_hold` enum('true','false') NOT NULL DEFAULT 'true',
  PRIMARY KEY (`entity_id`),
  CONSTRAINT `seller_id` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller`
--

LOCK TABLES `seller` WRITE;
/*!40000 ALTER TABLE `seller` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suburb`
--

DROP TABLE IF EXISTS `suburb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suburb` (
  `suburb_id` int(11) NOT NULL AUTO_INCREMENT,
  `suburb_name` varchar(45) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`suburb_id`),
  KEY `city_id_fk_idx` (`city_id`),
  CONSTRAINT `city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2025 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suburb`
--

LOCK TABLES `suburb` WRITE;
/*!40000 ALTER TABLE `suburb` DISABLE KEYS */;
INSERT INTO `suburb` VALUES (1,'Ahipara',1),(2,'Awanui',1),(3,'Broadwood',1),(4,'Cable Bay',1),(5,'Cape Reinga',1),(6,'Cavalli Islands',1),(7,'Coopers Beach',1),(8,'Diggers Valley',1),(9,'Fairburn',1),(10,'Haruru',1),(11,'Henderson Bay',1),(12,'Herekino',1),(13,'Hihi',1),(14,'Hokianga',1),(15,'Hokianga Harbour',1),(16,'Horeke',1),(17,'Houhora',1),(18,'Kaeo',1),(19,'Kaikohe',1),(20,'Kaimaumau',1),(21,'Kaingaroa',1),(22,'Kaitaia',1),(23,'Karetu',1),(24,'Karikari Peninsula',1),(25,'Kawakawa',1),(26,'Kerikeri',1),(27,'Kohukohu',1),(28,'Koutu ',1),(29,'Lake Ohia',1),(30,'Mangamuka',1),(31,'Mangatoetoe',1),(32,'Mangonui',1),(33,'Maromaku',1),(34,'Mitimiti',1),(35,'Moerewa',1),(36,'Motutangi',1),(37,'Ngataki',1),(38,'Ohaeawai',1),(39,'Okaihau',1),(40,'Omanaia',1),(41,'Omapere',1),(42,'Opononi',1),(43,'Opua',1),(44,'Paihia',1),(45,'Pakaraka',1),(46,'Pamapuria',1),(47,'Panguru',1),(48,'Paranui',1),(49,'Peria',1),(50,'Pukenui',1),(51,'Pukepoto',1),(52,'Rangiahua',1),(53,'Rangiputa',1),(54,'Rawene',1),(55,'Russell',1),(56,'Taheke',1),(57,'Taipa',1),(58,'Takahue',1),(59,'Taupo Bay',1),(60,'Tauranga Bay',1),(61,'Tokerau Beach',1),(62,'Totara North',1),(63,'Towai',1),(64,'Victoria Valley',1),(65,'Waiharara',1),(66,'Waima',1),(67,'Waimamaku',1),(68,'Waimate North',1),(69,'Waipapa',1),(70,'Waipapakauri',1),(71,'Whangape Harbour',1),(72,'Whangaroa',1),(73,'Whatuwhiwhi',1),(74,'Whirinaki ',1),(75,'Aranga',2),(76,'Arapohue',2),(77,'Baylys Beach',2),(78,'Dargaville',2),(79,'Hakaru',2),(80,'Kaiwaka',2),(81,'Kellys Bay',2),(82,'Mangawhai',2),(83,'Mangawhai Heads',2),(84,'Mangawhare',2),(85,'Matakohe',2),(86,'Maungaturoto',2),(87,'Pahi',2),(88,'Paparoa',2),(89,'Pouto',2),(90,'Ruawai',2),(91,'Tangiteroria',2),(92,'Tangowahine',2),(93,'Te Kopuru',2),(94,'Tinopai',2),(95,'Waipoua',2),(96,'Whakapirau',2),(97,'Avenues',3),(98,'Bream Bay',3),(99,'Central Whangarei',3),(100,'Glenbervie',3),(101,'Helena Bay',3),(102,'Hikurangi',3),(103,'Horahora',3),(104,'Hukerenui',3),(105,'Kamo',3),(106,'Kauri',3),(107,'Kensington',3),(108,'Kokopu',3),(109,'Langs Beach',3),(110,'Mairtown',3),(111,'Mangapai',3),(112,'Marsden Point',3),(113,'Marua',3),(114,'Matapouri',3),(115,'Maungakaramea',3),(116,'Maungatapere',3),(117,'Maunu',3),(118,'McLeod Bay',3),(119,'Morningside',3),(120,'Ngunguru',3),(121,'Oakleigh',3),(122,'Oakura Coast',3),(123,'One Tree Point',3),(124,'Onerahi',3),(125,'Otaika',3),(126,'Otangarei',3),(127,'Parahaki',3),(128,'Parua Bay',3),(129,'Pataua',3),(130,'Pataua North',3),(131,'Pataua South',3),(132,'Poroti',3),(133,'Port Whangarei',3),(134,'Portland',3),(135,'Raumanga',3),(136,'Regent',3),(137,'Riponui',3),(138,'Riverside',3),(139,'Ruakaka',3),(140,'Ruatangata',3),(141,'Sherwood Rise',3),(142,'Springs Flat',3),(143,'Tamaterau',3),(144,'Three Mile Bush',3),(145,'Tikipunga',3),(146,'Titoki',3),(147,'Toetoe',3),(148,'Tutukaka',3),(149,'Vinetown',3),(150,'Waikaraka',3),(151,'Waikiekie',3),(152,'Waiotira',3),(153,'Waipu',3),(154,'Waipu Cove',3),(155,'Whakapara',3),(156,'Whananaki',3),(157,'Whangarei',3),(158,'Whangarei Heads',3),(159,'Whangaruru',3),(160,'Whareora',3),(161,'Whatitiri',3),(162,'Whau Valley',3),(163,'Wheki Valley',3),(164,'Woodhill',3),(165,'Arch Hill',4),(166,'Avondale',4),(167,'Balmoral',4),(168,'Blockhouse Bay',4),(169,'City Centre',4),(170,'Coxs Bay',4),(171,'Eden Terrace',4),(172,'Ellerslie',4),(173,'Epsom',4),(174,'Fairview Heights',4),(175,'Freemans Bay',4),(176,'Glen Innes',4),(177,'Glendowie',4),(178,'Grafton',4),(179,'Greenlane',4),(180,'Grey Lynn',4),(181,'Herne Bay',4),(182,'Hillsborough',4),(183,'Kingsland',4),(184,'Kohimarama',4),(185,'Lynfield',4),(186,'Meadowbank',4),(187,'Mission Bay',4),(188,'Morningside',4),(189,'Mount Albert',4),(190,'Mount Eden',4),(191,'Mount Roskill',4),(192,'Mount Wellington',4),(193,'New Windsor',4),(194,'Newmarket',4),(195,'Newton',4),(196,'One Tree Hill',4),(197,'Onehunga',4),(198,'Orakei',4),(199,'Oranga',4),(200,'Otahuhu',4),(201,'Owairaka',4),(202,'Panmure',4),(203,'Parnell',4),(204,'Penrose',4),(205,'Point Chevalier',4),(206,'Point England',4),(207,'Ponsonby',4),(208,'Remuera',4),(209,'Royal Oak',4),(210,'Saint Heliers',4),(211,'Saint Johns',4),(212,'Saint Johns Park',4),(213,'Saint Marys Bay',4),(214,'Sandringham',4),(215,'St Lukes',4),(216,'Stonefields',4),(217,'Tamaki',4),(218,'Te Papapa',4),(219,'Three Kings',4),(220,'Wai O Taiki Bay',4),(221,'Waikowhai',4),(222,'Waterview',4),(223,'Wesley',4),(224,'Western Springs',4),(225,'Westmere',4),(226,'Wynyard Quarter',4),(227,'Aka Aka',5),(228,'Ararimu',5),(229,'Awhitu',5),(230,'Bombay',5),(231,'Buckland',5),(232,'Clarks Beach',5),(233,'Glenbrook',5),(234,'Harrisville',5),(235,'Hunua',5),(236,'Kaiaua',5),(237,'Karaka',5),(238,'Karaka Harbourside',5),(239,'Kingseat',5),(240,'Mangatangi',5),(241,'Mangatawhiri',5),(242,'Maramarua',5),(243,'Mauku',5),(244,'Mercer',5),(245,'Onewhero',5),(246,'Otaua',5),(247,'Paerata',5),(248,'Paparimu',5),(249,'Patumahoe',5),(250,'Pokeno',5),(251,'Pollok',5),(252,'Port Waikato',5),(253,'Pukekawa',5),(254,'Pukekohe',5),(255,'Pukekohe East',5),(256,'Puni',5),(257,'Ramarama',5),(258,'Te Kohanga',5),(259,'Tuakau',5),(260,'Waiau Pa',5),(261,'Waiuku',5),(262,'Whakatiwai',5),(263,'Wharekawa',5),(264,'Great Barrier Island',6),(265,'Kawau Island',6),(266,'Rakino Island',6),(267,'Alfriston',7),(268,'Beachlands',7),(269,'Botany Downs',7),(270,'Brookby',7),(271,'Bucklands Beach',7),(272,'Burswood',7),(273,'Chapel Downs',7),(274,'City Centre',7),(275,'Clendon Park',7),(276,'Clevedon',7),(277,'Clover Park',7),(278,'Cockle Bay',7),(279,'Cumbria Downs',7),(280,'Dannemora',7),(281,'East Tamaki',7),(282,'East Tamaki Heights',7),(283,'Eastern Beach',7),(284,'Farm Cove',7),(285,'Favona',7),(286,'Flat Bush',7),(287,'Golflands',7),(288,'Goodwood Heights',7),(289,'Half Moon Bay',7),(290,'Highland Park',7),(291,'Hill Park',7),(292,'Howick',7),(293,'Huntington Park',7),(294,'Kawakawa Bay',7),(295,'Mahia Park',7),(296,'Mangere',7),(297,'Mangere Bridge',7),(298,'Mangere East',7),(299,'Manukau',7),(300,'Manukau Heights',7),(301,'Manurewa',7),(302,'Manurewa East',7),(303,'Maraetai',7),(304,'Matingarahi',7),(305,'Meadowlands',7),(306,'Mellons Bay',7),(307,'Mission Heights',7),(308,'Northpark',7),(309,'Omana Beach',7),(310,'Orere',7),(311,'Orere Point',7),(312,'Otara',7),(313,'Pakuranga',7),(314,'Pakuranga Heights',7),(315,'Papatoetoe',7),(316,'Point View Park',7),(317,'Puhinui',7),(318,'Randwick Park',7),(319,'Shamrock Park',7),(320,'Shelly Park',7),(321,'Somerville',7),(322,'Sunnyhills',7),(323,'The Gardens',7),(324,'Totara Heights',7),(325,'Wattle Downs',7),(326,'Weymouth',7),(327,'Whitford',7),(328,'Wiri',7),(329,'Albany',8),(330,'Bayswater',8),(331,'Bayview',8),(332,'Beach Haven',8),(333,'Belmont',8),(334,'Birkdale',8),(335,'Birkenhead',8),(336,'Browns Bay',8),(337,'Campbells Bay',8),(338,'Castor Bay',8),(339,'Chatswood',8),(340,'Chester Park',8),(341,'Crown Hill',8),(342,'Devonport',8),(343,'Forrest Hill',8),(344,'Glenfield',8),(345,'Greenhithe',8),(346,'Hauraki',8),(347,'Hillcrest',8),(348,'Long Bay',8),(349,'Lucas Heights',8),(350,'Mairangi Bay',8),(351,'Marlborough',8),(352,'Meadowood',8),(353,'Milford',8),(354,'Murrays Bay',8),(355,'Narrow Neck',8),(356,'North Harbour',8),(357,'Northcote',8),(358,'Northcote Point',8),(359,'Northcross',8),(360,'Okura',8),(361,'Oteha',8),(362,'Paremoremo',8),(363,'Pinehill',8),(364,'Rosedale',8),(365,'Rothesay Bay',8),(366,'Schnapper Rock',8),(367,'Stanley Bay',8),(368,'Sunnynook',8),(369,'Takapuna',8),(370,'Torbay',8),(371,'Totara Vale',8),(372,'Unsworth Heights',8),(373,'Waiake',8),(374,'Wainoni',8),(375,'Wairau Valley',8),(376,'Westlake',8),(377,'Windsor Park',8),(378,'Ardmore',9),(379,'Conifer Grove',9),(380,'Drury',9),(381,'Opaheke',9),(382,'Pahurehure',9),(383,'Papakura',9),(384,'Red Hill',9),(385,'Rosehill',9),(386,'Takanini',9),(387,'Ahuroa',10),(388,'Albany Heights',10),(389,'Algies Bay',10),(390,'Arkles Bay',10),(391,'Army Bay',10),(392,'Big Manly',10),(393,'Coatesville',10),(394,'Dairy Flat',10),(395,'Glorit',10),(396,'Gulf Harbour',10),(397,'Hatfields Beach',10),(398,'Helensville',10),(399,'Hobbs Bay',10),(400,'Hoteo',10),(401,'Huapai',10),(402,'Kaipara Flats',10),(403,'Kaukapakapa',10),(404,'Kumeu',10),(405,'Leigh',10),(406,'Little Manly',10),(407,'Mahurangi',10),(408,'Mahurangi East',10),(409,'Mahurangi West',10),(410,'Makarau',10),(411,'Manly',10),(412,'Matakana',10),(413,'Matakatia Bay',10),(414,'Maygrove',10),(415,'Millwater',10),(416,'Muriwai Beach',10),(417,'Muriwai Valley',10),(418,'Omaha',10),(419,'Omaha Flats',10),(420,'Orewa',10),(421,'Pakiri',10),(422,'Parakai',10),(423,'Parkhurst',10),(424,'Point Wells',10),(425,'Puhoi',10),(426,'Red Beach',10),(427,'Redvale',10),(428,'Riverhead',10),(429,'Sandspit',10),(430,'Shelly Beach',10),(431,'Silverdale',10),(432,'Snells Beach',10),(433,'South Head',10),(434,'Stanmore Bay',10),(435,'Stillwater',10),(436,'Streamlands',10),(437,'Tapora',10),(438,'Tauhoa',10),(439,'Taupaki',10),(440,'Tawharanui Peninsula',10),(441,'Te Arai',10),(442,'Te Hana',10),(443,'The Grange',10),(444,'Tindalls Beach',10),(445,'Tomarata',10),(446,'Topuni',10),(447,'Wade Heads',10),(448,'Waikoukou Valley',10),(449,'Waimauku',10),(450,'Wainui',10),(451,'Waitoki',10),(452,'Waiwera',10),(453,'Warkworth',10),(454,'Wellsford',10),(455,'Whangaparaoa',10),(456,'Whangaripo',10),(457,'Wharehine',10),(458,'Woodcocks',10),(459,'Woodhill',10),(460,'Awaawaroa Bay',11),(461,'Blackpool',11),(462,'Church Bay',11),(463,'Connells Bay',11),(464,'Cowes Bay',11),(465,'Enclosure Bay',11),(466,'Hekerua Bay',11),(467,'Kennedy Point',11),(468,'Little Oneroa',11),(469,'Matiatia Bay',11),(470,'Omiha',11),(471,'Oneroa',11),(472,'Onetangi',11),(473,'Orapiu',11),(474,'Ostend',11),(475,'Palm Beach',11),(476,'Rocky Bay',11),(477,'Sandy Bay',11),(478,'Surfdale',11),(479,'Te Whau',11),(480,'Te Whau Bay',11),(481,'Waikopou Bay',11),(482,'Woodside Bay',11),(483,'Anawhata',12),(484,'Bethells Beach',12),(485,'Cornwallis',12),(486,'French Bay',12),(487,'Glen Eden',12),(488,'Glendene',12),(489,'Green Bay',12),(490,'Harbour View',12),(491,'Henderson',12),(492,'Henderson Valley',12),(493,'Herald Island',12),(494,'Hobsonville',12),(495,'Huia',12),(496,'Karekare',12),(497,'Kelston',12),(498,'Konini',12),(499,'Laingholm',12),(500,'Little Huia',12),(501,'Massey',12),(502,'Massey East',12),(503,'Massey North',12),(504,'Massey West',12),(505,'Mclaren Park',12),(506,'New Lynn',12),(507,'Oratia',12),(508,'Palm Heights',12),(509,'Parau',12),(510,'Piha',12),(511,'Ranui',12),(512,'Royal Heights',12),(513,'Sunnyvale',12),(514,'Swanson',12),(515,'Te Atatu Peninsula',12),(516,'Te Atatu South',12),(517,'Titirangi',12),(518,'Waiatarua',12),(519,'Waima',12),(520,'Waimanu Bay',12),(521,'Waitakere',12),(522,'West Harbour',12),(523,'Western Heights',12),(524,'Westgate',12),(525,'Whatipu',12),(526,'Whenuapai',12),(527,'Wood Bay',12),(528,'Woodlands Park',12),(529,'Bader',13),(530,'Beerescourt',13),(531,'Chartwell',13),(532,'Chedworth Park',13),(533,'Claudelands',13),(534,'Deanwell',13),(535,'Dinsdale',13),(536,'Enderley',13),(537,'Fairfield',13),(538,'Fairview Downs',13),(539,'Fitzroy',13),(540,'Flagstaff',13),(541,'Forest Lake',13),(542,'Frankton',13),(543,'Glenview',13),(544,'Grandview Heights',13),(545,'Hamilton City Central',13),(546,'Hamilton East',13),(547,'Hamilton Lake',13),(548,'Hamilton North',13),(549,'Hamilton West',13),(550,'Harrowfield',13),(551,'Hillcrest',13),(552,'Horsham Downs',13),(553,'Huntington',13),(554,'Livingstone',13),(555,'Maeroa',13),(556,'Melville',13),(557,'Nawton',13),(558,'Pukete',13),(559,'Queenwood',13),(560,'Riverlea',13),(561,'Rotokauri ',13),(562,'Rototuna',13),(563,'Ruakura',13),(564,'Rukuhia',13),(565,'Saint Andrews',13),(566,'Silverdale',13),(567,'Te Rapa',13),(568,'Temple View',13),(569,'Thornton Estate',13),(570,'Western Heights',13),(571,'Whitiora',13),(572,'Karangahake',14),(573,'Kerepehi',14),(574,'Mangatarata',14),(575,'Netherton',14),(576,'Ngatea',14),(577,'Paeroa',14),(578,'Turua',14),(579,'Waihi',14),(580,'Waihi East',14),(581,'Waikino',14),(582,'Waitakaruru',14),(583,'Whiritoa',14),(584,'Mangateparu',15),(585,'Matamata',15),(586,'Morrinsville',15),(587,'Patetonga',15),(588,'Springdale',15),(589,'Tahuna',15),(590,'Te Aroha',15),(591,'Te Poi',15),(592,'Waharoa',15),(593,'Waihou',15),(594,'Waitoa',15),(595,'Walton',15),(596,'Aotea Harbour',16),(597,'Kawhia',16),(598,'Otorohanga',16),(599,'Arapuni',17),(600,'Hodderville',17),(601,'Lichfield',17),(602,'Ngatira',17),(603,'Putaruru',17),(604,'Tirau',17),(605,'Tokoroa',17),(606,'Wiltsdown',17),(607,'Acacia Bay',18),(608,'Broadlands Forest',18),(609,'Hatepe',18),(610,'Hilltop',18),(611,'Invergarry',18),(612,'Kinloch',18),(613,'Kuratau',18),(614,'Lake Taupo',18),(615,'Mangakino',18),(616,'Marotiri',18),(617,'Motuoapa',18),(618,'Nukuhau',18),(619,'Omori',18),(620,'Oruanui',18),(621,'Oruatua',18),(622,'Pukawa',18),(623,'Rainbow Point',18),(624,'Rangatira Park',18),(625,'Richmond Heights',18),(626,'Tauhara',18),(627,'Te Rangiita',18),(628,'Tongariro',18),(629,'Town Centre',18),(630,'Turangi',18),(631,'Two Mile Bay',18),(632,'Waipahihi',18),(633,'Wairakei',18),(634,'Waitahanui',18),(635,'Waitetoko',18),(636,'Whareroa',18),(637,'Wharewaka',18),(638,'Cooks Beach',19),(639,'Coroglen',19),(640,'Coromandel',19),(641,'Flaxmill Bay',19),(642,'Hahei',19),(643,'Hikuai',19),(644,'Hikutaia ',19),(645,'Hot Water Beach',19),(646,'Kaimarama',19),(647,'Kopu',19),(648,'Kuaotunu Beach',19),(649,'Matarangi',19),(650,'Matatoki ',19),(651,'Onemana',19),(652,'Opito',19),(653,'Otama',19),(654,'Pauanui',19),(655,'Port Charles',19),(656,'Puriri ',19),(657,'Tairua',19),(658,'Tapu',19),(659,'Te Puru',19),(660,'Thames',19),(661,'Tuateawa',19),(662,'Whangamata',19),(663,'Whangapoua',19),(664,'Whenuakite',19),(665,'Whitianga',19),(666,'Arapuni',20),(667,'Eureka',20),(668,'Glen Murray',20),(669,'Gordonton',20),(670,'Hampton Downs',20),(671,'Horotiu',20),(672,'Huntly',20),(673,'Kainui',20),(674,'Matangi',20),(675,'Meremere',20),(676,'Miranda',20),(677,'Newstead',20),(678,'Ngaruawahia',20),(679,'Ohinewai',20),(680,'Oparau',20),(681,'Orini',20),(682,'Pukeatua',20),(683,'Puketaha',20),(684,'Raglan',20),(685,'Rangiriri',20),(686,'Rotongaro',20),(687,'Tamahere',20),(688,'Taupiri',20),(689,'Tauwhare',20),(690,'Te Akau',20),(691,'Te Hoe',20),(692,'Te Kauwhata',20),(693,'Te Kowhai',20),(694,'Te Uku',20),(695,'Tirohanga',20),(696,'Waerenga',20),(697,'Waikato Surrounds',20),(698,'Waiterimu',20),(699,'Whakamaru',20),(700,'Whatawhata',20),(701,'Whitikahu',20),(702,'Cambridge',21),(703,'Horahora',21),(704,'Kaipaki',21),(705,'Karamu',21),(706,'Karapiro',21),(707,'Kihikihi',21),(708,'Koromatua',21),(709,'Leamington',21),(710,'Ngahinapouri',21),(711,'Ohaupo',21),(712,'Pirongia',21),(713,'Pukekura',21),(714,'Rotoorangi',21),(715,'Rukuhia ',21),(716,'Te Awamutu',21),(717,'Te Pahu',21),(718,'Te Rore',21),(719,'Awakino',22),(720,'Benneydale',22),(721,'Marokopa',22),(722,'Piopio',22),(723,'Te Kuiti',22),(724,'Kawerau',23),(725,'Hospital Hill',24),(726,'Kutarere',24),(727,'Opotiki',24),(728,'Te Kaha',24),(729,'Waiotahi',24),(730,'Atiamuri',25),(731,'Awahou',25),(732,'Brunswick',25),(733,'Fairy Springs',25),(734,'Fenton Park',25),(735,'Fordlands',25),(736,'Glenholme',25),(737,'Hamurana',25),(738,'Hannahs Bay',25),(739,'Hillcrest',25),(740,'Holdens Bay',25),(741,'Horohoro',25),(742,'Kaharoa',25),(743,'Kawaha Point',25),(744,'Koutu',25),(745,'Lake Okareka',25),(746,'Lake Rotoehu',25),(747,'Lake Rotoma',25),(748,'Lake Tarawera',25),(749,'Lynmore',25),(750,'Mamaku',25),(751,'Mangakakahi',25),(752,'Matipo Heights',25),(753,'Mourea',25),(754,'Ngakuru',25),(755,'Ngapuna',25),(756,'Ngongotaha',25),(757,'Ngongotaha Valley',25),(758,'Ohinemutu',25),(759,'Okere Falls',25),(760,'Owhata',25),(761,'Pleasant Heights',25),(762,'Pukehangi',25),(763,'Reporoa',25),(764,'Rerewhakaaitu',25),(765,'Rotoiti',25),(766,'Rotorua',25),(767,'Selwyn Heights',25),(768,'Springfield',25),(769,'Sunnybrook',25),(770,'Te Ngae',25),(771,'Tihiotonga',25),(772,'Tikitere',25),(773,'Utuhina',25),(774,'Victoria',25),(775,'Waikite',25),(776,'Waikite Valley',25),(777,'Westbrook',25),(778,'Western Heights',25),(779,'Whakarewarewa',25),(780,'Arataki',26),(781,'Avenues',26),(782,'Bayfair',26),(783,'Bellevue',26),(784,'Bethlehem',26),(785,'Brookfield',26),(786,'Bureta',26),(787,'Cambridge Heights',26),(788,'Cherrywood',26),(789,'City Centre',26),(790,'Gate Pa',26),(791,'Greerton',26),(792,'Hairini',26),(793,'Judea',26),(794,'Kairua',26),(795,'Matapihi',26),(796,'Matua',26),(797,'Maungatapu',26),(798,'Merivale',26),(799,'Mount Maunganui',26),(800,'Ohauiti',26),(801,'Omanu',26),(802,'Oropi',26),(803,'Otumoetai',26),(804,'Papamoa',26),(805,'Papamoa Beach',26),(806,'Parkvale',26),(807,'Poike',26),(808,'Pyes Pa',26),(809,'Tauranga South',26),(810,'Tauriko',26),(811,'Te Maunga',26),(812,'Welcome Bay',26),(813,'Aongatete',27),(814,'Apata',27),(815,'Athenree',27),(816,'Katikati',27),(817,'Lower Kaimai',27),(818,'Maketu',27),(819,'Matakana Island',27),(820,'Omanawa',27),(821,'Omokoroa',27),(822,'Oropi',27),(823,'Paengaroa',27),(824,'Pahoia',27),(825,'Pongakawa',27),(826,'Pukehina',27),(827,'Pyes Pa',27),(828,'Rangiuru',27),(829,'Te Puke',27),(830,'Te Puna',27),(831,'Waihi Beach',27),(832,'Whakamarama',27),(833,'Awakaponga',28),(834,'Awakeri',28),(835,'Coastlands',28),(836,'Edgecumbe',28),(837,'Galatea',28),(838,'Matata',28),(839,'Murupara',28),(840,'Ohope',28),(841,'Onepu',28),(842,'Otakiri',28),(843,'Pahou',28),(844,'Paroa',28),(845,'Pikowai',28),(846,'Poroporo',28),(847,'Port Ohope',28),(848,'Ruatoki North',28),(849,'Taneatua',28),(850,'Te Teko',28),(851,'Thornton',28),(852,'Waimana',28),(853,'Whakatane',28),(854,'Awapuni',29),(855,'City Centre',29),(856,'Elgin',29),(857,'Hangaroa',29),(858,'Hexton',29),(859,'Hicks Bay',29),(860,'Inner Kaiti',29),(861,'Kaiti',29),(862,'Makaraka ',29),(863,'Makauri',29),(864,'Mangapapa',29),(865,'Manutuke',29),(866,'Matawai',29),(867,'Matawhero',29),(868,'Matokitoki',29),(869,'Motu',29),(870,'Muriwai',29),(871,'Ngatapa',29),(872,'Okitu',29),(873,'Ormond',29),(874,'Otoko',29),(875,'Outer Kaiti',29),(876,'Patutahi',29),(877,'Pouawa',29),(878,'Rere',29),(879,'Riverdale',29),(880,'Ruatoria',29),(881,'Tamarau',29),(882,'Te Hapara',29),(883,'Te Karaka',29),(884,'Tiniroto',29),(885,'Tokomaru Bay',29),(886,'Tolaga Bay',29),(887,'Waerengaokuri',29),(888,'Waihau Bay',29),(889,'Waimata',29),(890,'Waingake',29),(891,'Wainui',29),(892,'Waipaoa',29),(893,'Whangara',29),(894,'Wharekopae',29),(895,'Whatatutu',29),(896,'Whataupoko',29),(897,'Central Hawke\'s Bay Country',30),(898,'Ongaonga',30),(899,'Patangata',30),(900,'Porangahau',30),(901,'Takapau',30),(902,'Tikokino',30),(903,'Waipawa',30),(904,'Waipukurau',30),(905,'Wanstead',30),(906,'Otane',30),(907,'Elsthorpe',30),(908,'Akina',31),(909,'Bridge Pa',31),(910,'Camberley',31),(911,'Clive',31),(912,'Fernhill',31),(913,'Flaxmere',31),(914,'Frimley',31),(915,'Hastings Central',31),(916,'Haumoana',31),(917,'Havelock North',31),(918,'Kahuranaki',31),(919,'Karamu',31),(920,'Kereru',31),(921,'Mahora',31),(922,'Mangatahi',31),(923,'Omahu',31),(924,'Mangateretere',31),(925,'Maraekakaho',31),(926,'Mayfair',31),(927,'Moteo',31),(928,'Otamauri',31),(929,'Pakipaki',31),(930,'Pakowhai',31),(931,'Parkvale',31),(932,'Patangata',31),(933,'Patoka',31),(934,'Poukawa',31),(935,'Puketapu',31),(936,'Putorino',31),(937,'Raukawa',31),(938,'Raureka',31),(939,'Rissington',31),(940,'Longlands',31),(941,'Saint Leonards',31),(942,'Sherenden',31),(943,'Stortford Lodge',31),(944,'Tangoio',31),(945,'Te Awanga',31),(946,'Te Hauke',31),(947,'Te Pohue',31),(948,'Eskdale',31),(949,'Tomoana',31),(950,'Tutira',31),(951,'Twyford',31),(952,'Waimarama',31),(953,'Waipatu',31),(954,'Waiwhare',31),(955,'Puketitiri',31),(956,'Whakatu',31),(957,'Whanawhana',31),(958,'Whirinaki',31),(959,'Crownthorpe',31),(960,'Ahuriri',32),(961,'Awatoto',32),(962,'Bay View',32),(963,'Bluff Hill',32),(964,'City Centre',32),(965,'Greenmeadows',32),(966,'Hospital Hill',32),(967,'Jervoistown',32),(968,'Maraenui',32),(969,'Marewa',32),(970,'Meeanee',32),(971,'Mission View',32),(972,'Napier South',32),(973,'Onekawa',32),(974,'Pukehamoamoa',32),(975,'Tamatea',32),(976,'Taradale',32),(977,'Te Awa',32),(978,'Westshore',32),(979,'Poraiti',32),(980,'Pirimai',32),(981,'Frasertown',33),(982,'Kotemaori',33),(983,'Mahanga',33),(984,'Mahia',33),(985,'Mahia Beach',33),(986,'Marumaru',33),(987,'Mohaka',33),(988,'Morere',33),(989,'Nuhaka',33),(990,'Ruakituri',33),(991,'Te Reinga',33),(992,'Tuai',33),(993,'Wairoa',33),(994,'Whakaki',33),(995,'Bell Block',34),(996,'Blagdon',34),(997,'Brixton',34),(998,'Brooklands',34),(999,'City Centre',34),(1000,'Egmont Village',34),(1001,'Ferndale',34),(1002,'Fitzroy',34),(1003,'Frankleigh Park',34),(1004,'Glen Avon',34),(1005,'Highlands Park',34),(1006,'Hillsborough',34),(1007,'Hurworth',34),(1008,'Inglewood',34),(1009,'Koru',34),(1010,'Lepperton',34),(1011,'Lynmouth',34),(1012,'Mangorei',34),(1013,'Marfell',34),(1014,'Merrilands',34),(1015,'Mokau',34),(1016,'Moturoa',34),(1017,'New Plymouth',34),(1018,'Oakura',34),(1019,'Okato',34),(1020,'Omata',34),(1021,'Onaero',34),(1022,'Royal Heights',34),(1023,'Spotswood',34),(1024,'Strandon',34),(1025,'Hurdon',34),(1026,'Tariki',34),(1027,'Tikorangi',34),(1028,'Urenui',34),(1029,'Vogeltown',34),(1030,'Waitara',34),(1031,'Welbourn',34),(1032,'Westown',34),(1033,'Whalers Gate',34),(1034,'Alton',35),(1035,'Eltham',35),(1036,'Hawera',35),(1037,'Kaponga',35),(1038,'Manaia',35),(1039,'Normanby',35),(1040,'Okaiawa',35),(1041,'Opunake',35),(1042,'Patea',35),(1043,'Pungarehu',35),(1044,'Tokaora',35),(1045,'Waitotara',35),(1046,'Warea',35),(1047,'Waverley',35),(1048,'Midhurst',36),(1049,'Stratford',36),(1050,'Stratford East',36),(1051,'Stratford West',36),(1052,'Toko',36),(1053,'Foxton',37),(1054,'Foxton Beach',37),(1055,'Heatherlea',37),(1056,'Hokio Beach',37),(1057,'Ihakara',37),(1058,'Kuku',37),(1059,'Levin',37),(1060,'Manakau',37),(1061,'Muhunoa East',37),(1062,'Ohau',37),(1063,'Opiki',37),(1064,'Shannon',37),(1065,'Tokomaru',37),(1066,'Waikawa Beach',37),(1067,'Waitarere',37),(1068,'Waitarere Beach',37),(1069,'Apiti',38),(1070,'Awahuri',38),(1071,'Cheltenham',38),(1072,'Colyton',38),(1073,'Feilding',38),(1074,'Halcombe',38),(1075,'Highbury',38),(1076,'Himatangi',38),(1077,'Hiwinui',38),(1078,'Kelvin Grove',38),(1079,'Kimbolton',38),(1080,'Mt Biggs',38),(1081,'Newbury',38),(1082,'Ohakea',38),(1083,'Pohangina',38),(1084,'Rangiotu',38),(1085,'Rangiwahia',38),(1086,'Rongotea',38),(1087,'Sanson',38),(1088,'Tangimoana',38),(1089,'Waituna West',38),(1090,'Aokautere',39),(1091,'Ashhurst',39),(1092,'Awapuni',39),(1093,'Bunnythorpe',39),(1094,'City Centre',39),(1095,'Cloverlea',39),(1096,'Fitzherbert',39),(1097,'Highbury',39),(1098,'Hokowhitu',39),(1099,'Kairanga',39),(1100,'Kelvin Grove',39),(1101,'Linton',39),(1102,'Longburn',39),(1103,'Milson',39),(1104,'Papaioea',39),(1105,'Roslyn',39),(1106,'Takaro',39),(1107,'Terrace End',39),(1108,'Turitea',39),(1109,'West End',39),(1110,'Westbrook',39),(1111,'Whakarongo',39),(1112,'Bulls',40),(1113,'Hunterville',40),(1114,'Mangaweka',40),(1115,'Marton',40),(1116,'Ohingaiti',40),(1117,'Taihape',40),(1118,'Turakina',40),(1119,'National Park',41),(1120,'Ohakune',41),(1121,'Ohura',41),(1122,'Owhango',41),(1123,'Raetihi',41),(1124,'Raurimu',41),(1125,'Taumarunui',41),(1126,'Waiouru',41),(1127,'Akitio',42),(1128,'Dannevirke',42),(1129,'Eketahuna',42),(1130,'Pahiatua',42),(1131,'Pongaroa',42),(1132,'Woodville',42),(1133,'Aramoho',43),(1134,'Bastia Hill',43),(1135,'Brunswick',43),(1136,'Castlecliff',43),(1137,'City Centre',43),(1138,'College Estate',43),(1139,'Durie Hill',43),(1140,'Fordell',43),(1141,'Gonville',43),(1142,'Kaitoke',43),(1143,'Makirikiri ',43),(1144,'Mangamahu ',43),(1145,'Marybank',43),(1146,'Maxwell',43),(1147,'Mosston',43),(1148,'Okoia',43),(1149,'Otamatea',43),(1150,'Parikino',43),(1151,'Putiki',43),(1152,'Kai Iwi',43),(1153,'Rapanui',43),(1154,'Saint Johns Hill',43),(1155,'Springvale',43),(1156,'Tawhero',43),(1157,'Upokongaro',43),(1158,'Wanganui East',43),(1159,'Westmere',43),(1160,'Whangaehu',43),(1161,'Carterton',44),(1162,'Otaihanga',45),(1163,'Otaki',45),(1164,'Otaki Beach',45),(1165,'Paekakariki',45),(1166,'Paraparaumu',45),(1167,'Paraparaumu Beach',45),(1168,'Raumati Beach',45),(1169,'Raumati South',45),(1170,'Te Horo',45),(1171,'Waikanae',45),(1172,'Waikanae Beach',45),(1173,'Peka Peka',45),(1174,'Alicetown',46),(1175,'Avalon',46),(1176,'Belmont',46),(1177,'Boulcott',46),(1178,'Days Bay',46),(1179,'Eastbourne',46),(1180,'Epuni',46),(1181,'Fairfield',46),(1182,'Gracefield',46),(1183,'Harbour View',46),(1184,'Haywards',46),(1185,'Holborn',46),(1186,'Kelson',46),(1187,'Korokoro',46),(1188,'Lower Hutt',46),(1189,'Lowry Bay',46),(1190,'Mahina Bay',46),(1191,'Manor Park',46),(1192,'Maungaraki',46),(1193,'Melling',46),(1194,'Moera',46),(1195,'Muritai',46),(1196,'Naenae',46),(1197,'Normandale',46),(1198,'Park Avenue',46),(1199,'Petone',46),(1200,'Rona Bay',46),(1201,'Seaview',46),(1202,'Stokes Valley',46),(1203,'Sunshine Bay',46),(1204,'Taita',46),(1205,'Tirohanga',46),(1206,'Wainuiomata',46),(1207,'Waiwhetu',46),(1208,'Waterloo',46),(1209,'Woburn',46),(1210,'York Bay',46),(1211,'Castlepoint',47),(1212,'Flat Point',47),(1213,'Masterton',47),(1214,'Mataikona',47),(1215,'Mauriceville',47),(1216,'Riversdale Beach',47),(1217,'Aotea',48),(1218,'Ascot Park',48),(1219,'Camborne',48),(1220,'Cannons Creek',48),(1221,'Elsdon',48),(1222,'Karehana Bay',48),(1223,'Linden',48),(1224,'Mana',48),(1225,'Papakowhai',48),(1226,'Paremata',48),(1227,'Pauatahanui',48),(1228,'Plimmerton',48),(1229,'Porirua',48),(1230,'Porirua East',48),(1231,'Pukerua Bay',48),(1232,'Ranui Heights',48),(1233,'Takapu Valley',48),(1234,'Takapuwahia',48),(1235,'Titahi Bay',48),(1236,'Waitangirua',48),(1237,'Whitby',48),(1238,'Cape Palliser',49),(1239,'Featherston',49),(1240,'Gladstone',49),(1241,'Greytown',49),(1242,'Lake Ferry',49),(1243,'Martinborough',49),(1244,'Ngawi',49),(1245,'Tora',49),(1246,'Akatarawa',50),(1247,'Birchville',50),(1248,'Blue Mountains',50),(1249,'Brown Owl',50),(1250,'Clouston Park',50),(1251,'Ebdentown',50),(1252,'Heretaunga',50),(1253,'Kaitoke',50),(1254,'Kingsley Heights',50),(1255,'Maoribank',50),(1256,'Maymorn',50),(1257,'Moonshine Valley',50),(1258,'Mount Marua',50),(1259,'Pakuratahi',50),(1260,'Pinehaven',50),(1261,'Riverstone Terraces',50),(1262,'Silverstream',50),(1263,'Te Marua',50),(1264,'The Plateau',50),(1265,'Timberlea',50),(1266,'Totara Park',50),(1267,'Mangaroa',50),(1268,'Trentham',50),(1269,'Upper Hutt',50),(1270,'Wallaceville',50),(1271,'Whitemans Valley',50),(1272,'Elderslea',50),(1273,'Aro Valley',51),(1274,'Berhampore',51),(1275,'Broadmeadows',51),(1276,'Brooklyn',51),(1277,'Breaker Bay',51),(1278,'Chartwell',51),(1279,'Karori',51),(1280,'Strathmore Park',51),(1281,'Northland',51),(1282,'Churton Park',51),(1283,'Moa Point',51),(1284,'Wellington Central',51),(1285,'Crawford',51),(1286,'Crofton Downs',51),(1287,'Evans Bay',51),(1288,'Te Aro',51),(1289,'Glenside',51),(1290,'Vogeltown',51),(1291,'Grenada',51),(1292,'Grenada North',51),(1293,'Grenada Village',51),(1294,'Hataitai',51),(1295,'Highbury',51),(1296,'Horokiwi',51),(1297,'Houghton Bay',51),(1298,'Island Bay',51),(1299,'Oriental Bay',51),(1300,'Mount Victoria',51),(1301,'Johnsonville',51),(1302,'Kaiwharawhara',51),(1303,'Kelburn',51),(1304,'Khandallah',51),(1305,'Kilbirnie',51),(1306,'Kingston',51),(1307,'Kowhai Park',51),(1308,'Lambton',51),(1309,'Linden',51),(1310,'Makara-Ohariu',51),(1311,'Maupuia',51),(1312,'Lyall Bay',51),(1313,'Melrose',51),(1314,'Miramar',51),(1315,'Mornington',51),(1316,'Mount Cook',51),(1317,'Newlands',51),(1318,'Newtown',51),(1319,'Ngaio',51),(1320,'Ngauranga',51),(1321,'Karaka Bays',51),(1322,'Ohariu',51),(1323,'Owhiro Bay',51),(1324,'Paparangi',51),(1325,'Pipitea',51),(1326,'Raroa',51),(1327,'Redwood',51),(1328,'Rongotai',51),(1329,'Roseneath',51),(1330,'Seatoun',51),(1331,'Seatoun Bays',51),(1332,'Seatoun Heights',51),(1333,'Southgate',51),(1334,'Tawa',51),(1335,'Thorndon',51),(1336,'Wadestown',51),(1337,'Wilton',51),(1338,'Woodridge',51),(1339,'Annesbrook',52),(1340,'Atawhai',52),(1341,'Bishopdale',52),(1342,'Britannia Heights',52),(1343,'Cable Bay ',52),(1344,'City Centre',52),(1345,'Enner Glynn',52),(1346,'Hira',52),(1347,'Maitai',52),(1348,'Maitlands',52),(1349,'Marybank',52),(1350,'Moana',52),(1351,'Monaco',52),(1352,'Nayland',52),(1353,'Nelson',52),(1354,'Nelson East',52),(1355,'Nelson South',52),(1356,'Ngawhatu',52),(1357,'Port Hills',52),(1358,'Stepneyville',52),(1359,'Stoke',52),(1360,'Tahunanui',52),(1361,'Tahunanui Hills',52),(1362,'The Brook',52),(1363,'The Glen',52),(1364,'The Wood',52),(1365,'Todds Valley',52),(1366,'Toi Toi',52),(1367,'Wakapuaka',52),(1368,'Wakatu',52),(1369,'Washington Valley',52),(1370,'Whangamoa',52),(1371,'Aniseed Valley',53),(1372,'Appleby',53),(1373,'Best Island',53),(1374,'Brightwater',53),(1375,'Collingwood',53),(1376,'Dovedale',53),(1377,'Golden Bay',53),(1378,'Hope',53),(1379,'Kaiteriteri',53),(1380,'Kohatu',53),(1381,'Korere',53),(1382,'Lower Moutere',53),(1383,'Mahana',53),(1384,'Mapua',53),(1385,'Marahau',53),(1386,'Motueka',53),(1387,'Motupiko',53),(1388,'Murchison',53),(1389,'Ngatimoti',53),(1390,'Onekaka',53),(1391,'Owen River',53),(1392,'Pakawau',53),(1393,'Parapara',53),(1394,'Patons Rock',53),(1395,'Pohara',53),(1396,'Rabbit Island',53),(1397,'Redwood Valley',53),(1398,'Richmond',53),(1399,'Riwaka',53),(1400,'Ruby Bay',53),(1401,'Saint Arnaud',53),(1402,'Takaka',53),(1403,'Tapawera',53),(1404,'Tasman',53),(1405,'Tata Beach',53),(1406,'Upper Moutere',53),(1407,'Wakefield',53),(1408,'Blenheim Central',54),(1409,'Burleigh',54),(1410,'Mayfield',54),(1411,'Omaka',54),(1412,'Redwoodtown',54),(1413,'Riverlands',54),(1414,'Riversdale',54),(1415,'Springlands',54),(1416,'Witherlea',54),(1417,'Kaikoura',55),(1418,'Kekerengu',55),(1419,'Anakiwa ',56),(1420,'Awatere Valley',56),(1421,'Dashwood',56),(1422,'Fairhall',56),(1423,'Grovetown',56),(1424,'Havelock',56),(1425,'Kenepuru Sounds',56),(1426,'Koromiko',56),(1427,'Linkwater',56),(1428,'Mahau Sound',56),(1429,'Marlborough Sounds',56),(1430,'Ngakuta Bay',56),(1431,'North Bank',56),(1432,'Okaramio',56),(1433,'Okiwi Bay',56),(1434,'Onamalutu Valley',56),(1435,'Pelorus Sounds',56),(1436,'Picton',56),(1437,'Portage',56),(1438,'Queen Charlotte Sounds',56),(1439,'Rai Valley',56),(1440,'Rapaura',56),(1441,'Rarangi',56),(1442,'Renwick',56),(1443,'Seddon',56),(1444,'Spring Creek',56),(1445,'Tennyson Inlet',56),(1446,'Tuamarina',56),(1447,'Waihopai Valley',56),(1448,'Waikawa',56),(1449,'Wairau Valley',56),(1450,'Ward',56),(1451,'Woodbourne',56),(1452,'Carters Beach',57),(1453,'Charleston',57),(1454,'Fairdown',57),(1455,'Granity',57),(1456,'Hector',57),(1457,'Inangahua',57),(1458,'Karamea',57),(1459,'Little Wanganui',57),(1460,'Maruia',57),(1461,'Millerton',57),(1462,'Mokihinui',57),(1463,'Ngakawau',57),(1464,'Punakaiki',57),(1465,'Reefton',57),(1466,'Seddonville',57),(1467,'Springs Junction',57),(1468,'Waimangaroa',57),(1469,'Westport',57),(1470,'Ahaura',58),(1471,'Barrytown',58),(1472,'Blackball',58),(1473,'Camerons',58),(1474,'Dobson',58),(1475,'Dunollie',58),(1476,'Greymouth',58),(1477,'Ikamatua',58),(1478,'Lake Brunner',58),(1479,'Moana',58),(1480,'Nelson Creek',58),(1481,'Ngahere',58),(1482,'Paroa',58),(1483,'Rapahoe',58),(1484,'Runanga',58),(1485,'Rutherglen',58),(1486,'South Beach',58),(1487,'Blaketown',58),(1488,'Taylorville',58),(1489,'Totara Flat',58),(1490,'Cobden',58),(1491,'Karoro',58),(1492,'Haast',59),(1493,'Awatuna',59),(1494,'Bruce Bay',59),(1495,'Fox Glacier',59),(1496,'Franz Josef',59),(1497,'Harihari',59),(1498,'Hokitika',59),(1499,'Kaniere',59),(1500,'Kokatahi',59),(1501,'Kowhitirangi',59),(1502,'Kumara',59),(1503,'Moana',59),(1504,'Okarito',59),(1505,'Okuru',59),(1506,'Pukekura',59),(1507,'Ross',59),(1508,'Whataroa ',59),(1509,'Allenton',60),(1510,'Ashburton',60),(1511,'Chertsey',60),(1512,'Fairton',60),(1513,'Hinds',60),(1514,'Mayfield',60),(1515,'Methven',60),(1516,'Montalto',60),(1517,'Mt Somers',60),(1518,'Rakaia',60),(1519,'Tinwald',60),(1520,'Akaroa',61),(1521,'Birdlings Flat',61),(1522,'Cass Bay',61),(1523,'Charteris Bay',61),(1524,'Diamond Harbour',61),(1525,'Duvauchelle',61),(1526,'Governors Bay',61),(1527,'Little Akaloa',61),(1528,'Little River',61),(1529,'Lyttelton',61),(1530,'Motukarara',61),(1531,'Okains Bay',61),(1532,'Pigeon Bay',61),(1533,'Port Levy',61),(1534,'Purau',61),(1535,'Teddington',61),(1536,'Wainui',61),(1537,'Addington',62),(1538,'Aidanfield',62),(1539,'Aranui',62),(1540,'Avondale',62),(1541,'Avonhead',62),(1542,'Avonside',62),(1543,'Balmoral Hill',62),(1544,'Barrington',62),(1545,'Beckenham',62),(1546,'Belfast',62),(1547,'Bexley',62),(1548,'Bishopdale',62),(1549,'Bromley',62),(1550,'Brookhaven',62),(1551,'Brooklands',62),(1552,'Broomfield',62),(1553,'Bryndwr',62),(1554,'Burnside',62),(1555,'Burwood',62),(1556,'Casebrook',62),(1557,'Cashmere',62),(1558,'Chaneys',62),(1559,'City Centre',62),(1560,'Clifton',62),(1561,'Cracroft',62),(1562,'Dallington',62),(1563,'Edgeware',62),(1564,'Eyreton',62),(1565,'Fendalton',62),(1566,'Ferrymead',62),(1567,'Forest Park',62),(1568,'Halswell',62),(1569,'Harewood',62),(1570,'Heathcote',62),(1571,'Hei Hei',62),(1572,'Hillmorton',62),(1573,'Hillsborough',62),(1574,'Hoon Hay',62),(1575,'Hornby',62),(1576,'Huntsbury',62),(1577,'Hyde Park',62),(1578,'Ilam',62),(1579,'Islington',62),(1580,'Kainga',62),(1581,'Kennedy\'s Bush',62),(1582,'Linwood',62),(1583,'Mairehau',62),(1584,'Marshland',62),(1585,'Merivale',62),(1586,'Middleton',62),(1587,'Moncks Bay',62),(1588,'Mount Pleasant',62),(1589,'Murray Aynsley',62),(1590,'New Brighton',62),(1591,'North Linwood',62),(1592,'North New Brighton',62),(1593,'Northcote',62),(1594,'Northshore',62),(1595,'Northwood ',62),(1596,'Oaklands',62),(1597,'Opawa',62),(1598,'Papanui',62),(1599,'Parklands',62),(1600,'Phillipstown',62),(1601,'Queenspark',62),(1602,'Redcliffs',62),(1603,'Redwood',62),(1604,'Riccarton',62),(1605,'Richmond',62),(1606,'Richmond Hill',62),(1607,'Russley',62),(1608,'Scarborough',62),(1609,'Shirley',62),(1610,'Sockburn',62),(1611,'Somerfield',62),(1612,'South New Brighton',62),(1613,'Southshore',62),(1614,'Spencerville',62),(1615,'Spreydon',62),(1616,'St. Albans',62),(1617,'St. Andrews Hill',62),(1618,'St. Martins',62),(1619,'Stewarts Gully',62),(1620,'Strowan',62),(1621,'Styx',62),(1622,'Sumner',62),(1623,'Sydenham',62),(1624,'Templeton',62),(1625,'Upper Riccarton',62),(1626,'Waimairi Beach',62),(1627,'Wainoni',62),(1628,'Waitikiri',62),(1629,'Waltham',62),(1630,'Westlake',62),(1631,'Westmorland',62),(1632,'Wigram',62),(1633,'Wigram Park',62),(1634,'Woolston',62),(1635,'Yaldhurst ',62),(1636,'Amberley',63),(1637,'Amberley Beach',63),(1638,'Balcairn',63),(1639,'Cheviot',63),(1640,'Culverden',63),(1641,'Domett',63),(1642,'Gore Bay',63),(1643,'Greta Valley',63),(1644,'Hanmer Springs',63),(1645,'Hawarden',63),(1646,'Leithfield',63),(1647,'Leithfield Beach',63),(1648,'Lewis Pass',63),(1649,'Lyford',63),(1650,'Motunau',63),(1651,'Oaro',63),(1652,'Omihi',63),(1653,'Rotherham',63),(1654,'Scargill',63),(1655,'Waiau',63),(1656,'Waikari',63),(1657,'Waipara',63),(1658,'Albury',64),(1659,'Burkes Pass ',64),(1660,'Cave',64),(1661,'Fairlie',64),(1662,'Lake Tekapo',64),(1663,'Mt Cook',64),(1664,'Twizel',64),(1665,'Annat',65),(1666,'Arthurs Pass',65),(1667,'Broadfield',65),(1668,'Burnham',65),(1669,'Castle Hill Village',65),(1670,'Charing Cross',65),(1671,'Coalgate',65),(1672,'Darfield',65),(1673,'Doyleston',65),(1674,'Dunsandel',65),(1675,'Glentunnel',65),(1676,'Greendale',65),(1677,'Hororata',65),(1678,'Irwell',65),(1679,'Kirwee',65),(1680,'Kowai Bush',65),(1681,'Lake Coleridge',65),(1682,'Lake Ellesmere',65),(1683,'Leeston',65),(1684,'Lincoln',65),(1685,'Prebbleton',65),(1686,'Rolleston',65),(1687,'Russells Flat',65),(1688,'Sheffield',65),(1689,'Southbridge',65),(1690,'Springfield',65),(1691,'Springston',65),(1692,'Tai Tapu',65),(1693,'Weedons',65),(1694,'West Melton',65),(1695,'Windwhistle',65),(1696,'Arundel',66),(1697,'Geraldine',66),(1698,'Kerrytown',66),(1699,'Levels',66),(1700,'Orari',66),(1701,'Pareora',66),(1702,'Pleasant Point',66),(1703,'St Andrews',66),(1704,'Temuka',66),(1705,'Timaru',66),(1706,'Washdyke',66),(1707,'Winchester',66),(1708,'Ashley',67),(1709,'Ashley Gorge',67),(1710,'Bennetts',67),(1711,'Bexley',67),(1712,'Burnt Hill',67),(1713,'Carleton',67),(1714,'Clarkville',67),(1715,'Coldstream',67),(1716,'Coopers Creek',67),(1717,'Coutts Island',67),(1718,'Cust',67),(1719,'Eyrewell',67),(1720,'Fernside',67),(1721,'Flaxton',67),(1722,'Gammans Creek',67),(1723,'Glentui',67),(1724,'Horrellville',67),(1725,'Kaiapoi',67),(1726,'Loburn',67),(1727,'Loburn North',67),(1728,'Ohapuku',67),(1729,'Ohoka',67),(1730,'Okuku',67),(1731,'Oxford',67),(1732,'Pegasus',67),(1733,'Pegasus Bay',67),(1734,'Pines Beach',67),(1735,'Rangiora',67),(1736,'Sefton',67),(1737,'Southbrook',67),(1738,'Springbank',67),(1739,'Summerhill',67),(1740,'Swannanoa',67),(1741,'Tuahiwi',67),(1742,'View Hill',67),(1743,'Waikuku',67),(1744,'Waikuku Beach',67),(1745,'West Eyreton',67),(1746,'White Rock',67),(1747,'Woodend',67),(1748,'Glenavy',68),(1749,'Makikihi',68),(1750,'Saint Andrews',68),(1751,'Waimate',68),(1752,'Alexandra',69),(1753,'Clyde',69),(1754,'Cromwell',69),(1755,'Ettrick',69),(1756,'Ida Valley',69),(1757,'Lauder',69),(1758,'Millers Flat',69),(1759,'Naseby',69),(1760,'Omakau',69),(1761,'Ophir',69),(1762,'Oturehua',69),(1763,'Ranfurly',69),(1764,'Roxburgh',69),(1765,'St Bathans',69),(1766,'Tarras',69),(1767,'Wedderburn',69),(1768,'Balclutha',70),(1769,'Clinton',70),(1770,'Kaitangata',70),(1771,'Kaka Point',70),(1772,'Lawrence',70),(1773,'Milton',70),(1774,'Owaka',70),(1775,'Pounawea',70),(1776,'Taieri Mouth',70),(1777,'Tapanui',70),(1778,'Taumata',70),(1779,'Tuapeka Mouth',70),(1780,'Waihola',70),(1781,'Wairuna',70),(1782,'Waiwera South',70),(1783,'Abbotsford',71),(1784,'Allanton',71),(1785,'Andersons Bay',71),(1786,'Aramoana',71),(1787,'Balaclava',71),(1788,'Belleknowes',71),(1789,'Berwick',71),(1790,'Blackhead',71),(1791,'Bradford',71),(1792,'Brighton',71),(1793,'Broad Bay',71),(1794,'Brockville',71),(1795,'Calton Hill',71),(1796,'Careys Bay',71),(1797,'Caversham',71),(1798,'City Centre',71),(1799,'Clyde Hill',71),(1800,'Company Bay',71),(1801,'Concord',71),(1802,'Corstorphine',71),(1803,'Dalmore',71),(1804,'East Taieri',71),(1805,'Doctors Point',71),(1806,'Fairfield',71),(1807,'Forbury',71),(1808,'Glenleith',71),(1809,'Glenross',71),(1810,'Green Island',71),(1811,'Halfway Bush',71),(1812,'Harington Point',71),(1813,'Harwood',71),(1814,'Helensburgh',71),(1815,'Henley',71),(1816,'Heyward Point',71),(1817,'Highcliff',71),(1818,'Hindon',71),(1819,'Kaikorai',71),(1820,'Kaikorai Valley',71),(1821,'Karitane',71),(1822,'Kenmure',71),(1823,'Kensington',71),(1824,'Kew',71),(1825,'Kinmont Park',71),(1826,'Leith Valley',71),(1827,'Liberton',71),(1828,'Littlebourne',71),(1829,'Long Beach',71),(1830,'Lookout Point',71),(1831,'Macandrew Bay',71),(1832,'Maia',71),(1833,'Maori Hill',71),(1834,'Maryhill',71),(1835,'Maungatua',71),(1836,'Middlemarch',71),(1837,'Mornington',71),(1838,'Mosgiel',71),(1839,'Mount Mera',71),(1840,'Mt Cargill',71),(1841,'Musselburgh',71),(1842,'Normanby',71),(1843,'North Dunedin',71),(1844,'North East Valley',71),(1845,'North Taieri',71),(1846,'Ocean Grove',71),(1847,'Ocean View',71),(1848,'Opoho',71),(1849,'Osborne',71),(1850,'Otago Peninsula',71),(1851,'Otakou',71),(1852,'Otokia',71),(1853,'Outram',71),(1854,'Pine Hill',71),(1855,'Port Chalmers',71),(1856,'Portobello',71),(1857,'Purakaunui',71),(1858,'Ravensbourne',71),(1859,'Roseneath',71),(1860,'Roslyn',71),(1861,'Saddle Hill',71),(1862,'Saint Clair',71),(1863,'Saint Clair Park',71),(1864,'Saint Kilda',71),(1865,'Saint Leonards',71),(1866,'Sawyers Bay',71),(1867,'Scroggs Hill',71),(1868,'Shiel Hill',71),(1869,'South Dunedin',71),(1870,'Tainui',71),(1871,'The Cove',71),(1872,'Vauxhall',71),(1873,'Waikouaiti',71),(1874,'Waipori',71),(1875,'Waitati',71),(1876,'Wakari',71),(1877,'Waldronville',71),(1878,'Warrington',71),(1879,'Waverley',71),(1880,'Westgate',71),(1881,'Westwood',71),(1882,'Wingatui',71),(1883,'Woodhaugh',71),(1884,'Arrow Junction',72),(1885,'Arrowtown',72),(1886,'Arthurs Point',72),(1887,'Boydtown',72),(1888,'Closeburn',72),(1889,'Crown Terrace',72),(1890,'Dalefield',72),(1891,'Fernhill',72),(1892,'Frankton',72),(1893,'Gibbston',72),(1894,'Glenorchy',72),(1895,'Goldfield Heights',72),(1896,'Jacks Point',72),(1897,'Kelvin Peninsula',72),(1898,'Lake Hayes',72),(1899,'Kingston',72),(1900,'Lake Wakatipu',72),(1901,'Lower Shotover',72),(1902,'Queenstown East',72),(1903,'Queenstown Hill',72),(1904,'Sunshine Bay',72),(1905,'Town Centre',72),(1906,'Stirling',73),(1907,'Alma',74),(1908,'Lake Ohau',74),(1909,'Awamoko',74),(1910,'Dunback',74),(1911,'Duntroon',74),(1912,'Enfield',74),(1913,'Glenavy',74),(1914,'Hampden',74),(1915,'Herbert',74),(1916,'Kakanui',74),(1917,'Kurow',74),(1918,'Maheno',74),(1919,'Moeraki',74),(1920,'Ngapara',74),(1921,'Oamaru',74),(1922,'Otematata',74),(1923,'Palmerston',74),(1924,'Pukeuri',74),(1925,'Sailors Cutting',74),(1926,'South Oamaru',74),(1927,'Taranui',74),(1928,'Totara',74),(1929,'Twizel',74),(1930,'Waiareka',74),(1931,'Waihemo',74),(1932,'Weston',74),(1933,'Windsor',74),(1934,'Omarama',74),(1935,'Albert Town',75),(1936,'Cardrona',75),(1937,'Cattle flat',75),(1938,'Glendhu Bay',75),(1939,'Hawea Flat',75),(1940,'Hunter Valley',75),(1941,'Lake Hawea',75),(1942,'Queensberry',75),(1943,'Makarora',75),(1944,'Mount Aspiring',75),(1945,'Mount Barker',75),(1946,'The Neck',75),(1947,'Treble cone',75),(1948,'Wanaka',75),(1949,'Luggate',75),(1950,'Waikawa Valley',76),(1951,'Gore',77),(1952,'Mataura',77),(1953,'Ascot',78),(1954,'Bluff',78),(1955,'Hawthorndale',78),(1956,'City Centre',78),(1957,'Clifton',78),(1958,'Gladstone',78),(1959,'Glengarry',78),(1960,'Hargest',78),(1961,'Invercargill',78),(1962,'Grasmere',78),(1963,'Kennington',78),(1964,'Kew',78),(1965,'Lorneville',78),(1966,'Mabel Bush',78),(1967,'Makarewa',78),(1968,'Mataura Island',78),(1969,'Georgetown',78),(1970,'Myross Bush',78),(1971,'Appleby',78),(1972,'Newfield',78),(1973,'Otatara',78),(1974,'Richmond',78),(1975,'Rosedale',78),(1976,'Roslyn Bush',78),(1977,'Strathern',78),(1978,'Tisbury',78),(1979,'Tussock Creek',78),(1980,'Waikiwi',78),(1981,'Wallacetown',78),(1982,'Waverley',78),(1983,'West Plains',78),(1984,'Windsor',78),(1985,'Woodend',78),(1986,'Heidelberg',78),(1987,'Avenal',78),(1988,'Kingswell',78),(1989,'Athol',79),(1990,'Balfour',79),(1991,'Browns',79),(1992,'Centre Bush',79),(1993,'Colac Bay',79),(1994,'Dipton',79),(1995,'Dipton West',79),(1996,'Drummond',79),(1997,'Edendale',79),(1998,'Garston',79),(1999,'Isla Bank',79),(2000,'Limehills',79),(2001,'Lumsden',79),(2002,'Manapouri',79),(2003,'Mossburn',79),(2004,'Nightcaps',79),(2005,'Oban',79),(2006,'Ohai',79),(2007,'Orepuki',79),(2008,'Otautau',79),(2009,'Riversdale',79),(2010,'Riverton',79),(2011,'Ryal Bush',79),(2012,'Stewart Island',79),(2013,'Te Anau',79),(2014,'Thornbury',79),(2015,'Tokanui',79),(2016,'Tuatapere',79),(2017,'Waianiwa',79),(2018,'Waikaia',79),(2019,'Waituna',79),(2020,'Wallacetown',79),(2021,'Waverley',79),(2022,'Winton',79),(2023,'Woodlands',79),(2024,'Wyndham',79);
/*!40000 ALTER TABLE `suburb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant` (
  `entity_id` int(11) NOT NULL,
  `is_looking` enum('false','true') NOT NULL DEFAULT 'false',
  `access_control` int(11) NOT NULL DEFAULT '3',
  `about_me` varchar(1000) DEFAULT '',
  PRIMARY KEY (`entity_id`),
  KEY `access_control_fk_idx` (`access_control`),
  KEY `tenant_entity_id_fk_idx` (`entity_id`),
  CONSTRAINT `access_control_fk` FOREIGN KEY (`access_control`) REFERENCES `tenant_access_control_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tenant_entity_id_fk` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenant`
--

LOCK TABLES `tenant` WRITE;
/*!40000 ALTER TABLE `tenant` DISABLE KEYS */;
INSERT INTO `tenant` VALUES (164,'true',3,'asdweqrasf2342rawerfasr3421543qfqEWDQWr43q2trwvewq315432TTREGWedfe3r2ty564gwdDE12443TWREAVQE53u756hrwDR1sdgfdafhw435yt543ghaergtrehg'),(165,'true',3,'');
/*!40000 ALTER TABLE `tenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenant_access_control_options`
--

DROP TABLE IF EXISTS `tenant_access_control_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant_access_control_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `des` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenant_access_control_options`
--

LOCK TABLES `tenant_access_control_options` WRITE;
/*!40000 ALTER TABLE `tenant_access_control_options` DISABLE KEYS */;
INSERT INTO `tenant_access_control_options` VALUES (1,'Everybody'),(2,'Agents / owners only'),(3,'Agents or owners restricted to my region(s)'),(4,'Using a non-discoverable id emailed to them');
/*!40000 ALTER TABLE `tenant_access_control_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenant_preference`
--

DROP TABLE IF EXISTS `tenant_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant_preference` (
  `preference_id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `suburb_id` int(11) DEFAULT NULL,
  `bedroom_min` int(11) DEFAULT NULL,
  `bedroom_max` int(11) DEFAULT NULL,
  `max_rent_pw` int(11) DEFAULT NULL,
  PRIMARY KEY (`preference_id`),
  KEY `teant_preference_entity_fk_idx` (`entity_id`),
  KEY `teant_preference_region_id_fk_idx` (`region_id`),
  KEY `teant_preference_city_id_fk_idx` (`city_id`),
  KEY `teant_preference_suburb_id_fk_idx` (`suburb_id`),
  CONSTRAINT `teant_preference_entity_fk` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenant_preference`
--

LOCK TABLES `tenant_preference` WRITE;
/*!40000 ALTER TABLE `tenant_preference` DISABLE KEYS */;
INSERT INTO `tenant_preference` VALUES (32,164,3,13,NULL,NULL,NULL,1000),(33,164,2,NULL,NULL,1,5,1000),(34,165,3,13,531,2,7,1231),(39,165,3,14,576,1,5,1111);
/*!40000 ALTER TABLE `tenant_preference` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-24 19:32:14
