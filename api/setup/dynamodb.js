const dynamodb = require('../api/modules/dynamodb').dynamodb;
const dynamodbUti = require('../api/modules/dynamodb').Utilities;

const config = require('../config');

module.exports = {
    test: async function (table) {
        console.log("Test DynamoDB ...         ");
        process.stdout.write("      Creating test table ...         ");
        var params = {
            TableName: table.name,
            KeySchema: [{ AttributeName: table.key, KeyType: "HASH" }],
            AttributeDefinitions: [{ AttributeName: table.key, AttributeType: "S" }],
            ProvisionedThroughput: { ReadCapacityUnits: 5, WriteCapacityUnits: 5 }
        };

        await dynamodb.createTable(params).promise().then(async d => { console.log("Done.") }).catch(async error => { 
            if (error.code === "ResourceInUseException" && error.message === "Cannot create preexisting table") {
                console.log("exists.");
            } else {
                console.error("Unable to create table. Error JSON:", JSON.stringify(error, null, 2));
                dynamodbError(error); 
            }
        });

        process.stdout.write("      Inserting into test table ...   ");
        var values = { [table.key]: table.value };
        var resResult = await dynamodbUti.putNewItem(false, table.name, values, table.key);
        resResult.status ? console.log("Done.") : dynamodbError(retValue.error);

        process.stdout.write("      Removing item ...               ");
        var resResult = await dynamodbUti.removeItem(table.name, values);
        resResult.status ? console.log("Done.") : null;

        process.stdout.write("      Removing test table ...         ");
        var params = { TableName: table.name };
        await dynamodb.deleteTable(params).promise().then(async d => { console.log("Done.") }).catch(async error => { dynamodbError(error); });
    },
    createTable: async function () {
        var tables = [{
            ttl: true,
            TableName: config.aws.dynamodb.table.loginStatus.name,
            key:config.aws.dynamodb.table.loginStatus.key,
        }, {
            ttl: true,
            TableName: config.aws.dynamodb.table.verifyCode.name,
            key:config.aws.dynamodb.table.verifyCode.key,
        }];

        for (var i = 0; i < tables.length; i++) {
            var creParams = {
                TableName: tables[i].TableName,
                KeySchema: [{ AttributeName: tables[i].key, KeyType: "HASH" },],
                AttributeDefinitions: [{ AttributeName: tables[i].key, AttributeType: "S" }],
                ProvisionedThroughput: { ReadCapacityUnits: 5, WriteCapacityUnits: 5 }
            };
            process.stdout.write("      Trying to create " + creParams.TableName + "   ")
            await dynamodb.createTable(creParams).promise().then(async d => {
                console.log("Done.")
                if (tables[i].ttl) {
                    process.stdout.write("          Modifying TTL  ...       ")
                    var ttlParams = {
                        TableName: creParams.TableName,
                        TimeToLiveSpecification: {
                            AttributeName: 'ttl',
                            Enabled: true
                        }
                    };
                    await dynamodb.updateTimeToLive(ttlParams).promise().then(async d => {
                        console.log("Done.")
                        var desParams = {
                            TableName: creParams.TableName
                        };
                        await dynamodb.describeTimeToLive(desParams).promise()
                            .catch(async error => {
                                console.log(error, error.stack);
                            })
                    }).catch(async error => {
                        console.log(error, error.stack);
                    })
                }
            }).catch(async error => {
                if (error.code === "ResourceInUseException" && error.message === "Cannot create preexisting table") {
                    console.log("exists.");
                } else {
                    console.error("Unable to create table. Error JSON:", JSON.stringify(error, null, 2));
                }
            });
        }
    }
}
function dynamodbError(error) {
    console.error("DynamoDB Error: ", error);
    process.exit(1);
}