const connectionPool = require('../api/modules/database');
const config = require('../config');


const locations = require("./locations.json");
const errorHandler = require("../api/modules/errorHandler")

async function insert (){
    try{
        for(var rKey in locations){
            var aRegion = locations[rKey];
            var regionId = (await connectionPool.query("insert into region (region_name) values (?)", [rKey])).insertId;
            for(var cKey in aRegion){
                var cityId = (await connectionPool.query("insert into city (city_name, region_id) values (?, ?)", [splitLocation(cKey), regionId])).insertId;
                var suburbs = aRegion[cKey];
                for(var s = 0; s < suburbs.length; s ++){
                    var aSuburb = suburbs[s];
                    await connectionPool.query("insert into suburb (suburb_name, city_id) values (?, ?)", [splitLocation(aSuburb), cityId]);                    
                }
            }
        }
        process.exit(1);
    }catch(error){
        errorHandler.handle(error);
    }
}

function splitLocation(loca){
    var locaArray = loca.split(" ");
    if(locaArray[locaArray.length - 1].indexOf("(") !== -1){
        locaArray.splice(-1,1);
    }
    return locaArray.join(" ");
}

insert ();