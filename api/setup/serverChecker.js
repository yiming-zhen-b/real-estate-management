const dynamodb = require('./dynamodb');
const mysql = require('./mysql');

var table = {
    name: "test",
    key: "key",
    value: "test"
}

check();

async function check () {
    console.log("");
    await mysql.import();
    console.log("");
    await mysql.check(table);
    console.log("");
    await dynamodb.test(table);
    console.log("");
    await dynamodb.createTable();
    process.exit(1);
}