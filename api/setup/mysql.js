const connectionPool = require('../api/modules/database');
const mysql = require('mysql');
const fs = require('fs');
const util = require('util');

const config = require('../config');

module.exports = {
    check: async function (table) {
        console.log("Test MySql database ...");

        process.stdout.write("      Creating test table ...         ");
        await connectionPool.query("create table if not exists " + table.name + " (id INT)").then(async d => { console.log("Done."); }).catch(async error => { mysqlError(error); });

        process.stdout.write("      Inserting into test table ...   ");
        await connectionPool.query("insert into " + table.name + " (id) VALUES (1)").then(async d => { console.log("Done."); }).catch(async error => { mysqlError(error); });

        process.stdout.write("      Updating item ...               ");
        await connectionPool.query("update " + table.name + " set id = 2 where id = 1").then(async d => { console.log("Done."); }).catch(async error => { mysqlError(error); });

        process.stdout.write("      Fetching item ...               ");
        await connectionPool.query("select * from " + table.name + " where id = 2").then(async d => {
            if (d.length == 0) { mysqlError({ code: "No Item Found" }); } console.log("Done.");
        }).catch(async error => { mysqlError(error); });

        process.stdout.write("      Removing item ...               ");
        await connectionPool.query("delete from " + table.name + " where id = 1").then(async d => { console.log("Done."); }).catch(async error => { mysqlError(error); });

        process.stdout.write("      Removing test table ...         ");
        await connectionPool.query("drop table " + table.name).then(async d => { console.log("Done."); }).catch(async error => { mysqlError(error); });
    },
    import: async function () {
        process.stdout.write("Importing into MySql database ...     ");

        var connection = mysql.createConnection({
            host: config.mysql.host,
            user: config.mysql.user,
            password: config.mysql.password,
            multipleStatements: true
        });
        
        connection.query = util.promisify(connection.query);
        fs.readFile = util.promisify(fs.readFile);


        connection.connect(async function (err) {
            if (err) {
                mysqlError(err);
                return;
            }
        });
    
        var sql = await fs.readFile('./mysql/database.sql', 'utf8').catch(async error => {
            mysqlError(error);
        });
    
        await connection.query(sql).then(async d => {
            console.log("Imported.");
        }).catch(async error => {
            mysqlError(error);
        })
    }
}

function mysqlError(error) {
    console.error("MySql Database Error: ", error.code);
    process.exit(1);
}