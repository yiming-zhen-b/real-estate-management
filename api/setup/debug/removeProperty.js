
const connectionPool = require('../../api/modules/database');
const errorHandler = require('../../api/modules/errorHandler');
const dynamodbUti = require('../../api/modules/dynamodb').Utilities;
const config = require('../../config');
const utilities = require("../../api/modules/utilities");
const awsS3 = require('../../api/modules/awsS3');


async function remove() {
    try {
        var args = process.argv.slice(2);
        var p_id = args[0];

        var imgs = await connectionPool.query('SELECT * FROM property_image WHERE property_id = ?;', [p_id])
            .then(async d => { return d; });

        for(var i = 0; i < imgs.length; i ++){
            var oPath = config.s3_dir.p_img.original + imgs[i].org_img;
            var tPath = config.s3_dir.p_img.thumb + imgs[i].thumb_img
            utilities.removeFile("../../api/" + oPath);
            utilities.removeFile("../../api/" + tPath);
            await awsS3.deleteObjects(config.aws.s3.bucket, [{Key: oPath}, {Key: tPath}]);
        }

        await connectionPool.query('DELETE FROM property_image WHERE property_id = ?;', [p_id])
        await connectionPool.query('DELETE FROM property WHERE property_id = ?;', [p_id])

        console.log("Reomved property ", p_id);
        process.exit();
    }
    catch (error) {
        errorHandler.handle(error);
    }
    
}

remove();