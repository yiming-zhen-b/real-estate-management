// SETUP
// =============================================================================
//required models & packages
const fs = require('fs');
const express = require('express');
const https = require('https');
const cors = require("cors");
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
const timeout = require('connect-timeout'); //express v4
// Import modules
const config = require('./config');
const args = require('./api/modules/args').processArgs(process.argv);
const port = process.env.PORT || 8080;        // set our port

// Using Cros to set headers
// app.set('trust proxy', true);
app.use(cors(config.cros));
// enable data from post support json encoded bodies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// Set and handel timeout
app.use(timeout('30s'));
app.use(function haltOnTimedout(req, res, next) {
    if (!req.timedout) next();
    else res.json("Time out");
});
// define the versions
const APIS = {
    client: {
        path: "./api",
        url: [
            '/searchProperty',
            '/listingProperty',
            '/loginRegister',
            '/getPropertyAttr',
            '/home',
            '/staticImg',
            '/dashboard',
            '/dashDocument',
            '/dashInfo',
            '/dashPreference',
            '/dashSecurity',
            '/proDetail'
        ]
    },
    admin: {
        path: "./api/admin",
        url: ['/main']
    }
}
// route to display versions
app.get('/api', function (req, res) {
    res.json(APIS);
});
// REGISTER THE PREFIXES
for (var k in APIS) {
    APIS[k].url.forEach(aUrl => {
        app.use(aUrl, require(APIS[k].path + aUrl));        
    });
}

// START THE SERVER
// =============================================================================

app.listen(port);

// https.createServer({
//     key: fs.readFileSync('./ssl/selfsigned.key', 'utf8'),
//     cert: fs.readFileSync('./ssl/selfsigned.crt', 'utf8')
// }, app).listen(port);

// console.log('API server on port ' + port);
console.log('API server on port ' + port);