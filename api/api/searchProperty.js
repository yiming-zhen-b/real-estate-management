//required models & packages
const express = require('express');

// Import modules
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const encrypt = require('./modules/encrypt');
const config = require('../config');
const basePara = require('./modules/searchPropertyQuery').basePara;
// const args = require('./modules/args').args

// ROUTES
// =============================================================================
// This module is for the search property
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// The api for the default search when the search.html is loaded
router.post("/getDefault", async function (req, res, next) {
    try {
        var result = { status: true, data: {}, img: {} };
        result.data = await connectionPool.query('SELECT ' + basePara.select + ' from ' + basePara.from + " where " + basePara.where);
        result.icon = config.url.client_side_static.img.icon;
        result.img = encrypt.encryptJSONKeys(await getCoverImg(result.data));
        result.data = encrypt.encryptJOSNArrayValueByKey(result.data, "property_id");
        res.json(result);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// The api for the search
router.post("/search", async function (req, res, next) {
    try {
        var reResult = buildSearchQuery(JSON.parse(req.body.data));
        if(config.debug) console.log(reResult);
        var result = { status: false, data: "", img: {} };
        if (reResult.status) {
            result.status = true;
            result.data = await connectionPool.query(reResult.query, reResult.value);
            result.icon = config.url.client_side_static.img.icon;
            result.img = encrypt.encryptJSONKeys(await getCoverImg(result.data));
            result.data = encrypt.encryptJOSNArrayValueByKey(result.data, "property_id");
        }
        res.json(result);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This is used by /search and /get api, it will get only the cover images by an array of property id.
async function getCoverImg(data) {
    try {
        if (data.length == 0) return;
        var ids = data.map(x => x.property_id);
        var imgs = await connectionPool.query("select thumb_img, property_id from property_image where property_id in (?) && is_cover = 'true'", [ids]);
        var idVsImg = {};
        imgs.forEach(x => idVsImg[x.property_id] ? null : idVsImg[x.property_id] = config.url.uploaded_img.pro.thumb + x.thumb_img);
        return idVsImg;
    } catch (error) {
        errorHandler.handle(error);
    }
}

// Creats the search query for /search api
function buildSearchQuery(data) {
    var reResult = { status: true, query: "SELECT " + basePara.select + " from " + basePara.from, value: [] };
    if(config.debug) console.log(data);
    var availableNow = data.availableNow,
        petsOk = data.petsOk,
        region = data.region,
        city = data.city,
        suburb = data.suburb,
        propertyType = data.propertyType,
        bedroomFrom = data.bedroomFrom,
        bedroomTo = data.bedroomTo,
        bathroomFrom = data.bathroomFrom,
        bathroomTo = data.bathroomTo,
        rentPerWeekFrom = data.rentPerWeekFrom,
        rentPerWeekTo = data.rentPerWeekTo,
        queryArray = [];

    if (!data.hasOwnProperty("region") || !data.hasOwnProperty("city") || !data.hasOwnProperty("suburb") ||
        !data.hasOwnProperty("property_for") || !data.hasOwnProperty("propertyType") || !data.hasOwnProperty("bedroomFrom") ||
        !data.hasOwnProperty("bedroomTo") || !data.hasOwnProperty("bathroomFrom") ||
        !data.hasOwnProperty("bathroomTo") || !data.hasOwnProperty("rentPerWeekFrom") || !data.hasOwnProperty("rentPerWeekTo")) {
        return { status: false };
    }

    if(config.debug) console.log(data);
    if (availableNow === "yes") {

    }
    if (availableNow === "no") {

    }
    if (petsOk === "yes") {
        queryArray.push("pets_allowed in (?)");
        reResult.value.push(["negotiable", "don`t know", "yes"]);
    }
    if (petsOk === "no") {
        queryArray.push("pets_allowed = ?");
        reResult.value.push("no");
    }
    if (region != 0) {
        // queryArray.push("region LIKE ?");
        // reResult.value.push("%" + region + "%");
        queryArray.push("property.region_id = ?");
        reResult.value.push(encrypt.decryptID(region));
    }
    if (city != 0) {
        // queryArray.push("city LIKE ?");
        // reResult.value.push("%" + city + "%");
        queryArray.push("property.city_id = ?");
        reResult.value.push(encrypt.decryptID(city));
    }
    if (suburb.length != 0) {
        // queryArray.push("suburb in (?)");
        // reResult.value.push(suburb);
        queryArray.push("property.suburb_id in (?)");
        reResult.value.push(encrypt.decryptIDArray(suburb));
    }
    if (propertyType.length != 0) {
        queryArray.push("category_id in (?)");
        reResult.value.push(encrypt.decryptIDArray(propertyType));
    }
    if (bedroomFrom != 0) {
        queryArray.push("bedrooms >= ?");
        reResult.value.push(bedroomFrom);
    }
    if (bedroomTo != 0) {
        queryArray.push("bedrooms <= ?");
        reResult.value.push(bedroomTo);
    }
    if (bathroomFrom != 0) {
        queryArray.push("bathrooms >= ?");
        reResult.value.push(bathroomFrom);
    }
    if (bathroomTo != 0) {
        queryArray.push("bathrooms <= ?");
        reResult.value.push(bathroomTo);
    }
    if (rentPerWeekFrom != 0) {
        queryArray.push("rent_pw >= ?");
        reResult.value.push(rentPerWeekFrom);
    }
    if (rentPerWeekTo != 0) {
        queryArray.push("rent_pw <= ?");
        reResult.value.push(rentPerWeekTo);
    }
    queryArray.push("property_for = ?");
    reResult.value.push(data.property_for);

    queryArray.push(basePara.where);
    reResult.query += " where " + queryArray.join(" && ");
    if (reResult.value.length == 1){
        reResult.query += " LIMIT 20";
    }

    return reResult;
}

//Return the router for the server
module.exports = router;