//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const config = require('../config');
const utilities = require("./modules/utilities");

// ROUTES
// =============================================================================
// This module is the index.html
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// Thisd will handle the search function in the home page.
router.post("/fuzzySearch", async function (req, res, next) {
    try {
        var resResult = { status: true, data: { suburb: [], city: [], region: [] } };
        var likeValue = utilities.buildLikeValue(req.body.data);
        var queries = {
            suburb: "SELECT DISTINCT suburb_name, city_name, region_name from suburb, city, region where suburb_name like ? && suburb.city_id = city.city_id && city.region_id = region.region_id",
            city: "SELECT DISTINCT city_name, region_name from city, region where city_name like ? && city.region_id = region.region_id",
            region: "SELECT DISTINCT region_name from region where region_name like ?"
        };
        for (var key in queries) {
            resResult.data[key] = await connectionPool.query(queries[key], [likeValue]);
        }
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;