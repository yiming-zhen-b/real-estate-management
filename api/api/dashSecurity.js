//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const authorization = require('./modules/authorization');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const config = require('../config');
const utilities = require("./modules/utilities");
const encrypt = require('./modules/encrypt');
const removeAccount = require("./modules/removeAccount");
// ROUTES
// =============================================================================
// This module is the dashboard
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will remove the account.
router.post("/removeMyAccount", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var entity_id = auResult.data.sqlEntity_id;
        await removeAccount.remove(entity_id);
        res.json({ status: true });

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will update the password.
router.post("/updatePass", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;

        var resResult = { status: true };
        var data = JSON.parse(req.body.data),
            pass = data.pass;
        if (config.debug) console.log(auResult);
        await connectionPool.query("UPDATE entity SET password = ? where email_address = ?", [pass, auResult.data.user_email])
            .catch(async error => {
                resResult.status = false;
            });
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;