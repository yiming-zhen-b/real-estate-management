//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('../modules/errorHandler');
const connectionPool = require('../modules/database');
const dynamodbUti = require('../modules/dynamodb').Utilities;
const authorization = require('../modules/authorization');
const config = require('../../config');
const utilities = require("../modules/utilities");
const encrypt = require('../modules/encrypt');
const removeProperty = require("../modules/removeProperty");
const removeAccount = require("../modules/removeAccount");
const email = require("../modules/email");
const logout = require("../loginRegister").logout;
// ROUTES
// =============================================================================
// This module is the index.html
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// Thisd will handle the admin to get all the agents.
router.post("/getAllAgents", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true, data: [] };

        if (auResult.data.role.is_admin) {
            resResult.data = await connectionPool.query(
                "select created, agent.entity_id, licence_number, on_hold, email_address,first_name,last_name,is_agent " +
                "from agent left join entity on entity.entity_id = agent.entity_id");
            resResult.data = encrypt.encryptJOSNArrayValueByKey(resResult.data, "entity_id");
        } else {
            resResult = { status: false, redirect: "/" };
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// Thisd will update the agent on hole.
router.post("/updateAgentOnHold", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var data = JSON.parse(req.body.data),
            entity_id = encrypt.decryptID(data.id),
            onHold = data.onHold ? "true" : "false",
            isAgent = onHold == "true" ? "false" : "true"
            user_email = data.email;
        var resResult = { status: true, data: [] };
        if (auResult.data.role.is_admin) {
            var session_token = (await connectionPool.query("select session_token from entity where entity_id = ?", [entity_id]))[0].session_token;
            if(onHold == "true"){
                email.sendRegistrationDeclineEmail(user_email, "register_as_agent");
            }else{
                email.sendRegistrationAcceptEmail(user_email, "register_as_agent");
            }
            if(session_token){
                await logout(entity_id, session_token);
            }
            connectionPool.query("update agent set on_hold = ? where entity_id = ?", [onHold, entity_id]);
            connectionPool.query("update entity set is_agent = ? where entity_id = ?", [isAgent, entity_id]);
        } else {
            resResult = { status: false, redirect: "/" };
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// Thisd will handle the search function in the home page.
router.post("/removeAgent", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true, data: [] };
        var entity_id = encrypt.decryptID(req.body.data);
        if (auResult.data.role.is_admin) {
            await removeAccount.remove(entity_id);
        } else {
            resResult = { status: false, redirect: "/" };
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;