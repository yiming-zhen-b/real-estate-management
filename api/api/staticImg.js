//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const config = require('../config');
const utilities = require("./modules/utilities");

// ROUTES
// =============================================================================
// This module is for the static images, the client side will use these api to retrive the iamge rul

const router = express.Router();
var testImg = new RegExp("(jpg|jpeg|png)", "i");
// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// The api for home silder image
router.post("/getHSI", async function (req, res, next) {
    try {
        res.json(config.url.client_side_static.img.home.slider);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// The api for home body image
router.post("/getHBI", async function (req, res, next) {
    try {
        res.json(config.url.client_side_static.img.home.body);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// The api for home test image
router.post("/getHTI", async function (req, res, next) {
    try {
        res.json(config.url.client_side_static.img.home.test);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// The api for 404 image
router.post("/getFOFI", async function (req, res, next) {
    try {
        res.json(config.url.client_side_static.img.error);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;