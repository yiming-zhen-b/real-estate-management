//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const authorization = require('./modules/authorization');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const config = require('../config');
const utilities = require("./modules/utilities");
const encrypt = require('./modules/encrypt');
// ROUTES
// =============================================================================
// This module is the dashboard
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will get tenant perference.
router.post("/getAll", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true };
        resResult.data = await connectionPool.query("select preference_id, region_id, city_id, suburb_id, bedroom_min, bedroom_max, max_rent_pw from tenant_preference where entity_id = ?", [auResult.data.sqlEntity_id]);
        resResult.data.forEach(aPre => {
            aPre.preference_id = encrypt.encryptID(aPre.preference_id);
            if (aPre.region_id) aPre.region_id = encrypt.encryptID(aPre.region_id);
            if (aPre.city_id) aPre.city_id = encrypt.encryptID(aPre.city_id);
            if (aPre.suburb_id) aPre.suburb_id = encrypt.encryptID(aPre.suburb_id);
        });
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will add new tenant perference.
router.post("/save", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true },
            data = JSON.parse(req.body.data),
            query = { col: [], values: [] };

        checkLocations(data);
        
        for (var nKey in data) {
            if (data[nKey]) {
                query.col.push(nKey);
                query.values.push(data[nKey]);
            }
        }
        if (query.col.length) {
            query.col.push("entity_id");
            query.values.push(auResult.data.sqlEntity_id);
            resResult.preference_id = encrypt.encryptID((await connectionPool.query("insert into tenant_preference (" + query.col.join(", ") + ") values (?)", [query.values])).insertId);
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

function checkLocations(data){
    if (data.hasOwnProperty("region_id") && data.region_id) data.region_id = encrypt.decryptID(data.region_id);
    if (data.hasOwnProperty("city_id") && data.city_id) data.city_id = encrypt.decryptID(data.city_id);
    if (data.hasOwnProperty("suburb_id") && data.suburb_id) data.suburb_id = encrypt.decryptID(data.suburb_id);
}

// This will update tenant perference.
router.post("/update", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true },
            data = JSON.parse(req.body.data),
            preference_id = encrypt.decryptID(data.preference_id),
            query = { set: [], values: [] };

        checkLocations(data);

        delete data.preference_id;
        for (var nKey in data) {
            query.set.push(nKey + " = ?");
            if (!data[nKey]) {
                query.values.push(null);
            } else {
                query.values.push(data[nKey]);
            }
        }
        if (query.set.length) {
            query.values.push(preference_id);
            query.values.push(auResult.data.sqlEntity_id);
            if ((await connectionPool.query("update tenant_preference set " + query.set.join(", ") + " where preference_id = ? && entity_id = ?", query.values)).affectedRows !== 1) {
                resResult.status = false;
            }
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will remove tenant perference.
router.post("/remove", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true },
            preference_id = encrypt.decryptID(req.body.data);
        if ((await connectionPool.query("DELETE FROM tenant_preference WHERE preference_id = ? && entity_id = ?", [preference_id, auResult.data.sqlEntity_id])).affectedRows !== 1) {
            resResult.status = false;
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;