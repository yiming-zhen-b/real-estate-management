const config = require("../../config");
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(config.google_api_client_id);

// This module is for the Google token verification
module.exports.verify = async function (token) {
	const ticket = await client.verifyIdToken({
		idToken: token,
		audience: config.google_api_client_id,  // Specify the CLIENT_ID of the app that accesses the backend
		// Or, if multiple clients access the backend:
		//[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
	}).catch(err => {
		//throw an error if something gos wrong
		console.log("error while authenticating google user: " + JSON.stringify(err));
	})
	//if verification is ok, google returns a jwt
	var payload = ticket.getPayload(),
		expireDate = payload['exp'],
		reResult = { status: true, data: payload };
	/** 
	 * The value of iss in the ID token is equal to accounts.google.com or https://accounts.google.com.
	**/
	if (payload['iss'] !== config.google_api.iss_values[0] && payload['iss'] !== config.google_api_iss_values[1]) {
		console.err("Google Verify different iss value", payload['iss'])
		reResult.status = false;
	}
	/** 
	 * The value of aud in the ID token is equal to one of your app's client IDs. 
	 * This check is necessary to prevent ID tokens issued to a malicious app being used to 
	 * access data about the same user on your app's backend server.
	**/
	if (payload['aud'] !== config.google_api.client_id) {
		console.err("Google Verify different api client id", "wanted [" + config.google_api_client_id + "] but was [" + payload['aud'] + "]")
		reResult.status = false;
	}
	/** 
	 * The expiry time (exp) of the ID token has not passed. 
	**/
	if (expireDate < Math.round(Date.now() / 1000)) {
		console.err("Google Verify different api token expired", expireDate)
		reResult.status = false;
	}
	return reResult;
	// If request specified a G Suite domain:
	//const domain = payload['hd'];
}