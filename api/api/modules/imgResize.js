//required models & packages
const fs = require('fs');
const sharp = require('sharp');
const sizeOf = require('image-size');
const crypto = require("crypto");
const mime = require("mime");

// Import modules
const utilities = require("./utilities");
const connectionPool = require('./database');
const errorHandler = require('./errorHandler');
const config = require('../../config');
const awsS3 = require('./awsS3').s3;

// This module create the image thumbnail
module.exports.resize = async function (image, thumbImgName) {
    // { fieldname: 'images',
    // originalname: '0D6EF04A-6D87-4CF7-9F87-BAFC28D7EF9B.JPG',
    // encoding: '7bit',
    // mimetype: 'image/jpeg',
    // destination: './img/',
    // filename: '4c16d079661b71149513bd56a8e81f8abe09aa91bcb951376cfe0f03109bcc87.jpeg',
    // path: 'img/4c16d079661b71149513bd56a8e81f8abe09aa91bcb951376cfe0f03109bcc87.jpeg',
    // size: 1751915 }
    try {
        var path = image.path,
            dimensions = sizeOf(path),
            width = dimensions.width,
            heigth = dimensions.height;

        if (width > config.img_size.thumb.width) {
            var ratio = config.img_size.thumb.width / width;
            width = Math.round(width * ratio);
            heigth = Math.round(heigth * ratio);
        }

        if (heigth > config.img_size.thumb.height) {
            var ratio = config.img_size.thumb.height / heigth;
            width = Math.round(width * ratio);
            heigth = Math.round(heigth * ratio);
        }

        sharp(path)
            .resize(width, heigth)
            .withMetadata()
            .toFile(utilities.createImgPath(config.server_dir.tmp + thumbImgName), function (err) { });

    } catch (error) {
        errorHandler.handle(error);
    }
}