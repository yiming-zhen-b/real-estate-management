var nodemailer = require('nodemailer');

const utilities = require('./utilities');
const config = require('../../config');

var transporter = nodemailer.createTransport(config.email.property);

function send(to, subject, html) {
    var mailOptions = {
        from: config.email.property.auth.user,
        to: to,
        subject: subject,
        html: html
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + JSON.stringify(info));
        }
    });
}
// This module for sending email
module.exports = {
    send: send,
    sendWithTemplate: function (user_email, templateKey, extraText) {
        // Use the template specified in confid.json to generate the email.
        var template = config.email.template[templateKey];
        var html = JSON.parse(JSON.stringify(template.html));
        html.splice(template.html_insert_index, 0, extraText);
        // Send the email.
        send(user_email, template.subject, config.email.template.head + html.join(" "))
    },
    sendRegistrationAsAgentAndLanlordEmail: function (templateKey, extraText) {
        var template = config.email.template[templateKey].to_admin;
        var html = JSON.parse(JSON.stringify(template.html));
        var newExtraText = "";
        for(var eKey in extraText){
            newExtraText += "<p>" + utilities.capitalizeTxt(eKey.split("_").join(" ")) + ": " + extraText[eKey] + "</p>"
        }
        html.splice(template.html_insert_index, 0, newExtraText);
        send(config.email.property.auth.user, template.subject, config.email.template.head + html.join(" "))
    },
    sendRegistrationAcceptEmail: function (user_email, templateKey) {
        var template = config.email.template[templateKey].accept;
        var html = JSON.parse(JSON.stringify(template.html));
        send(user_email, template.subject, config.email.template.head + html.join(" "))
    },
    sendRegistrationDeclineEmail: function (user_email, templateKey) {
        var template = config.email.template[templateKey].decline;
        var html = JSON.parse(JSON.stringify(template.html));
        send(user_email, template.subject, config.email.template.head + html.join(" "))
    },
    sendNewPropertyNotiToTenant: async function (tenants, encryptdID, proLocations) {
        var template = config.email.template["new_property"];
        var href = config.url.static_base_url + "propertyDetail.html?d=\"" + encryptdID + "\""
        // var href = "http://localhost:8088/propertyDetail.html?d=\"" + encryptdID + "\""
        var html = config.email.template.head + "<p>There is a new property online: <a href='" + href + " '>" + proLocations.join(", ") + "</a></p>";
        console.log(tenants);
        tenants.map(x => send(x.email_address, template.subject, html))
        // send(user_email, template.subject, html)
    }
}