
var base = {
    select: ["suburb_name as suburb", "city_name as city", "region_name as region", "rent_pw", "street_number", "unit_flat_number", "street_name",
    "available", "bedrooms", "bathrooms", "parking"],
    from: ["property", "suburb", "city", "region"],
    where: ["property.suburb_id = suburb.suburb_id", "property.city_id = city.city_id", "property.region_id = region.region_id"]
}

var detail = {
    select: base.select.concat(["first_name", "last_name", "max_tenants", "pets_allowed", "smoking_allowed", "smoke_alarm", "dishwasher",
    "heating", "fibre", "parking", "details", "furnishings", "amenities", "ideal_tenants", "notes", "details", "video_tour_link",
    "contact_time", "mobile_number", "landline_number", "show_landline", "show_mobile"]),
    from: base.from.concat(["entity"]),
    where: base.where.concat(["property_id = ?", "entity.entity_id = property.entity_id"])
}

var basePara = {
    select: base.select.concat(["property_id"]).join(", "),
    from: base.from.join(", "),
    where: base.where.concat(["disabled = 'false'"]).join(" && ")
};

var detailPara = {
    select: detail.select.join(", "),
    from: detail.from.join(", "),
    where: detail.where.concat(["disabled = 'false'"]).join(" && ")
}

var dashEditPara = {
    select: detail.select.concat(["category_id", "property_for"]).join(", "),
    from: detail.from.join(", "),
    where: detail.where.join(" && ")
}

var dashEditAllAppPara = {
    from: base.from.join(", "),
    where: base.where.concat(["entity_id = ?"]).join(" && ")
}

module.exports = {
    basePara: basePara,
    detailPara: detailPara,
    dashEditPara: dashEditPara,
    dashEditAllAppPara: dashEditAllAppPara
}