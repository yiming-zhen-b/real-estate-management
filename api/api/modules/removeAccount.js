
const connectionPool = require('./database');
const removeProperty = require('./removeProperty');
const removeImage = require('./removeImage');
const dynamodbUti = require('./dynamodb').Utilities;

const config = require('../../config');
const dyLSTable = config.aws.dynamodb.table.loginStatus;

module.exports.remove = async function (entity_id) {
    var pro_id = (await connectionPool.query("select property_id from property where entity_id = ?", [entity_id])).map(x => x.property_id);
    if (pro_id.length) await removeProperty.remove(pro_id);

    var session_token = await connectionPool.query("select session_token from entity where entity_id = ?", [entity_id])
    if (session_token.length && session_token[0].session_token) {
        await dynamodbUti.removeItem(dyLSTable.name, { [dyLSTable.key]: session_token[0].session_token });
    }

    await removeImage.removeDoc({ entity_id: entity_id });

    var tables = ["tenant_preference", "documents", "agent", "lanlord", "seller", "tenant", "entity"];
    for(var i = 0; i < tables.length; i ++){
        await connectionPool.query("delete from " + tables[i] + " where entity_id = ?", [entity_id]);
    }
}