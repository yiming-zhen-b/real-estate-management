
const config = require('../../config');
const awsS3 = require('./awsS3');
const connectionPool = require('./database');

module.exports = {
    remove: async function (imgs) {
        var ids = imgs.map(x => x.img_id);
        for (var i = 0; i < imgs.length; i++) {
            var oPath = config.s3_dir.p_img.original + imgs[i].org_img;
            var tPath = config.s3_dir.p_img.thumb + imgs[i].thumb_img;
            await awsS3.deleteObjects(config.aws.s3.bucket, [{ Key: oPath }, { Key: tPath }]);
        }
        if (imgs.length) await connectionPool.query('DELETE FROM property_image WHERE img_id in (?);', [ids]);
    },
    removeDoc: async function (options) {
        var docs = [],
            doc_id = options.doc_id,
            entity_id = options.entity_id;
        if (options.modeSingle) {
            var doc_id = options.doc_id;
            var doc_name = await connectionPool.query("select url, s3_path from documents, documents_types where document_type_id = documents_types.id && documents.id = ?", [doc_id]);
            docs.push({ Key: doc_name[0].s3_path + doc_name[0].url })
            await connectionPool.query("delete from documents where id = ? && entity_id = ?", [doc_id, entity_id]);
        }
        else {
            await connectionPool.query("select s3_path, url, document_type_id from documents, documents_types " +
                "where documents_types.id = documents.document_type_id && entity_id = ?", [entity_id])
                .then(async data => {
                    data.forEach(element => {
                        docs.push({ Key: element.s3_path + element.url })
                    });
                    if (data.length) await connectionPool.query('DELETE FROM documents WHERE entity_id = ?;', [entity_id]);
                });
        }
        if (docs.length) await awsS3.deleteObjects(config.aws.s3.bucket, docs);
    }
}