const AWS = require("./awsConfig")("dynamodb");
const util = require('util');
const dateTime = require('node-datetime');

// Import modules
const errorHandler = require('./errorHandler');
const config = require('../../config');
var docClient = new AWS.DynamoDB.DocumentClient();

// This module is for dynamodb to insert, update, delete
module.exports = {
    dynamodb: new AWS.DynamoDB(),
    documentCilent: new AWS.DynamoDB.DocumentClient(),
    Utilities: {
        putNewItem: async function (allowOverride, table, values, keyName) {
            var retResult = { status: true };
            try {
                var params = {
                    TableName: table,
                    Item: values
                };
                if (!allowOverride) {
                    params.ConditionExpression = '#i <> :val',
                        params.ExpressionAttributeNames = { '#i': keyName },
                        params.ExpressionAttributeValues = { ':val': values[keyName] }
                }
                await docClient.put(params).promise()
                    .then(d => { if (config.debug) console.log(table + " New item added", values); retResult.data = values })
                    .catch(async error => {
                        if(error.code === "ConditionalCheckFailedException"){
                            if(config.debug) console.log(table + " This item exists", values[keyName])
                        }else{
                            errorHandler.handle(error);
                        }
                        retResult.status = false;
                        retResult.error = error;
                    });
            }
            catch (error) {
                retResult = { status: false, error: error };
                errorHandler.handle(error);
            }
            return retResult;
        },
        updateItem: async function (table, paramKey, UE, EAV) {
            try {
                var params = {
                    TableName: table,
                    Key: paramKey,
                    UpdateExpression: UE,
                    ExpressionAttributeValues: EAV,
                    ReturnValues: "UPDATED_NEW"
                };
                return await docClient.update(params).promise()
                    .then(data => {
                        if (config.debug)
                            console.log(table + " UpdateItem succeeded:", paramKey);
                        return true;
                    })
                    .catch(error => { errorHandler.handle(error); return false; });
            }
            catch (error) {
                errorHandler.handle(error);
                return false;
            }
        },
        getItem: async function (params) {
            var retResult = { status: false };
            try {
                await docClient.get(params).promise()
                    .then(async d => {
                        if (d.Item) {
                            retResult.status = true;
                            retResult.data = d.Item;
                        }
                    })
                    .catch(error => { errorHandler.handle(error); });
            }
            catch (error) {
                errorHandler.handle(error);
            }
            return retResult;
        },
        removeItem: async function (table, key) {
            var retResult = { status: true };
            try {
                var params = {
                    TableName: table,
                    Key: key
                };
                await docClient.delete(params).promise()
                    .catch(error => {
                        retResult.status = false;
                        console.error("ERROR: ", params);
                        errorHandler.handle(error);
                    });
            }
            catch (error) {
                retResult.status = false;
                errorHandler.handle(error);
            }
            return retResult;
        }
    }
}