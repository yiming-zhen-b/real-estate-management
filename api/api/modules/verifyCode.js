const dateTime = require('node-datetime');

const config = require('../../config');
const utilities = require("./utilities");
const errorHandler = require('./errorHandler');
const dynamodbUti = require('./dynamodb').Utilities;
const email = require('./email');
const verifyCodeTable = config.aws.dynamodb.table.verifyCode;

module.exports = {
    createAndSendEmail: async function (user_email, aim, templateKey) {
        // Create the varification code
        var code = utilities.createRandomPass();
        var values = {
            [verifyCodeTable.key]: user_email,
            "code": code,
            "aim": aim,
            "ttl": utilities.getTTLValue(config.ttl.verfyCode),
            "created": Math.round(new Date() / 1000)
        };
        var putResult = await dynamodbUti.putNewItem(false, verifyCodeTable.name, values, verifyCodeTable.key);
        if (putResult.status) {
            email.sendWithTemplate(user_email, templateKey, code);
        }
        return putResult;
    },
    getVerifyCode: async function (user_email) {
        return await dynamodbUti.getItem({
            TableName: verifyCodeTable.name,
            Key: { [verifyCodeTable.key]: user_email },
            AttributesToGet: ["code"]
        });
    },
    removeVerifyCode: async function (user_email){
        await dynamodbUti.removeItem(verifyCodeTable.name, { [verifyCodeTable.key]: user_email });
    }
}