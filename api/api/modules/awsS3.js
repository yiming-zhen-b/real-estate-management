const s3 = require('./awsConfig')("s3");
const multer = require("multer");
const multerS3 = require('multer-s3');
const crypto = require("crypto");
const mime = require("mime");
const fs = require('fs');
const path = require('path');


const config = require('../../config');
const imgResize = require('./imgResize');
const utilities = require("./utilities");
const errorHandler = require('./errorHandler');
const authorization = require('./authorization');
const connectionPool = require('./database');

// This module is for AWS S3 to upload, delete
// var s3 = new AWS.S3();
module.exports = {
    upload: multer({
        storage: multerS3({
            s3: s3,
            bucket: config.aws.s3.bucket,
            acl: config.aws.s3.acl,
            contentType: function (req, file, cb) {
                cb(null, file.mimetype);
            },
            metadata: function (req, file, cb) {
                cb(null, { fieldName: file.fieldname });
            },
            key: function (req, file, cb) {
                crypto.pseudoRandomBytes(32, function (err, raw) {
                    cb(null, config.s3_dir.p_img.original + raw.toString('hex') + '.' + mime.extension(file.mimetype));
                });
            }
        })
    }),
    uploadDocs: multer({
        storage: multerS3({
            s3: s3,
            bucket: config.aws.s3.bucket,
            acl: config.aws.s3.acl,
            contentType: function (req, file, cb) {
                cb(null, file.mimetype);
            },
            metadata: function (req, file, cb) {
                cb(null, { fieldName: file.fieldname });
            },
            key: async function (req, file, cb) {
                await connectionPool.query("select s3_path from documents_types where id = ?", [req.body.doc_type])
                    .then(data => {
                        if (data[0].s3_path) {
                            crypto.pseudoRandomBytes(32, function (err, raw) {
                                cb(null, data[0].s3_path + raw.toString('hex') + '.' + mime.extension(file.mimetype));
                            });
                        } else {
                            cb(new Error('Wrong type'));
                        }
                    });
            }
        }),
        fileFilter: async function (req, file, callback) {
            try {
                var auResult = await authorization.checkAuth(req);
                if (!auResult.status) return callback(new Error('authorization wrong'));
                var entity_id = auResult.data.sqlEntity_id;
                var typeData = await connectionPool.query("select max_value, valid_extname from documents_types where id = ?", [req.body.doc_type])
                if (typeData.length) {
                    var currentCount = await connectionPool.query("select count(entity_id) as count from documents where entity_id = ? && document_type_id = ?",
                        [entity_id, req.body.doc_type]);
                    if (typeData[0].max_value && typeData[0].max_value - currentCount[0].count <= 0) {
                        return callback(new Error('File count exceeded'));
                    }
                    if (typeData[0].valid_extname && typeData[0].valid_extname.split(",").indexOf(path.extname(file.originalname).toLowerCase()) < 0) {
                        return callback(new Error('File type wrong'));
                    }
                    callback(null, true);
                }
            } catch (error) {
                return callback(error);
            }
        }
    }),
    startUploadImage: async function (image) {
        var retResult = { status: true };
        try {
            var thumbName = "t-" + image.filename;
            await imgResize.resize(image, thumbName);

            let fileData = [
                { file: config.server_dir.tmp + image.filename, key: config.s3_dir.p_img.original + image.filename },
                { file: config.server_dir.tmp + thumbName, key: config.s3_dir.p_img.thumb + thumbName.substring(2, thumbName.length) }
            ];

            for (var fileKey in fileData) {
                var file = fs.readFileSync(fileData[fileKey].file)
                var params = {
                    Bucket: config.aws.s3.bucket,
                    Key: fileData[fileKey].key,
                    Body: file,
                    ACL: config.aws.s3.acl,
                    ContentType: image.mimetype
                };
                await s3.putObject(params).promise().then(async d => {
                    if (config.debug)
                        console.log("uploading image successfully");
                    utilities.removeFile(fileData[fileKey].file);
                }).catch(async err => {
                    retResult.status = false;
                    errorHandler.handle(err);
                });
            }
        } catch (error) {
            retResult.status = false;
            errorHandler.handle(error);
        }
        return retResult;
    },
    deleteObjects: async function (bucket, objects) {
        var deleteParam = {
            Bucket: bucket,
            Delete: { Objects: objects }
        };
        await s3.deleteObjects(deleteParam).promise().then(async data => {
            if (config.debug) console.log(data, 'deleted from S3');
        }).catch(async err => {
            errorHandler.handle(err);
        });
    }
}

module.exports.s3 = s3;