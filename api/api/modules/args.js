const config = require('../../config');

var args = {};
var validArgs = {
    d: "debug mode, d=true will turn the debug mode on."
};
module.exports = {
    processArgs: function (argv){
        args = argv.slice(2)
        .map(arg => arg.split('='))
        .reduce((args, [value, key]) => {
            args[value] = key;
            return args;
        }, {});
        if(args.d === "true") config.debug = true;         
        for(var aKey in args){
            if(!validArgs[aKey]){
                console.error("Accepted args: ", validArgs);
                break;
            }
        }
        return args;
    },
    args: args
}
