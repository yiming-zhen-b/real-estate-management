//required models & packages
const multer = require("multer");
const path = require('path');
const mime = require("mime");
const crypto = require("crypto");
const connectionPool = require('./database');

// This module is used by the image upload
// But it is no longer be used
const utilities = require("./utilities");
const config = require('../../config');
const authorization = require('./authorization');
const encrypt = require('./encrypt');


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, utilities.createImgPath(config.server_dir.tmp))
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(32, function (err, raw) {
            cb(null, raw.toString('hex') + '.' + mime.extension(file.mimetype));
        });
    }
});

var upload = multer({
    storage: storage,
    fileFilter: async function (req, file, callback) {
        try {
            var auResult = await authorization.checkAuth(req);
            if (!auResult.status) return callback(new Error('authorization wrong'));
            req.body.c = encrypt.decryptID(req.body.c);
            if ((await connectionPool.query("select count(property_id) as count from property where property_id = ?", [req.body.c]))[0].count === 0)
                return callback(new Error('forbidden'));
            if (config.max_pro_img_num - (await connectionPool.query("select count(img_id) as count from property_image where property_id = ?", [req.body.c]))[0].count <= 0)
                return callback(new Error('File count exceeded'));
            if ((await connectionPool.query("SELECT count(id) as count FROM property_image_type where concat(id) = ?", [req.body.img_type]))[0].count === 0) {
                req.body.img_type = 0;
            }
            var validExt = [".png", ".jpg", ".gif", ".bmp", ".jpeg"];
            if (validExt.indexOf(path.extname(file.originalname).toLowerCase()) < 0) {
                return callback(new Error('Only images are allowed'));
            }
            callback(null, true);
        } catch (error) {
            return callback(error);
        }
    }
});

var uploadDocs = multer({
    storage: storage,
    fileFilter: async function (req, file, callback) {
        try {
            var auResult = await authorization.checkAuth(req);
            if (!auResult.status) return callback(new Error('authorization wrong'));
            var entity_id = auResult.data.sqlEntity_id;

            var typeData = await connectionPool.query("select max_value, valid_extname from documents_types where id = ?", [req.body.doc_type])
            if (typeData.length) {
                var currentCount = await connectionPool.query("select count(entity_id) as count from documents where entity_id = ? && document_type_id = ?",
                    [entity_id, req.body.doc_type]);
                if (typeData[0].max_value - currentCount[0].count <= 0) {
                    return callback(new Error('File count exceeded'));
                }
                if (typeData[0].valid_extname.split(",").indexOf(path.extname(file.originalname)) < 0) {
                    return callback(new Error('File type wrong'));
                }
                callback(null, true);
            }
        } catch (error) {
            return callback(error);
        }
    }
});

module.exports.upload = upload;
module.exports.uploadDocs = uploadDocs;