//required models & packages
const fs = require('fs');
const sharp = require('sharp');
const sizeOf = require('image-size');
// Import modules
const connectionPool = require('./database');
const errorHandler = require('./errorHandler');
const config = require('../../config');

// // Check the region, city and suburb are exists in the database, update the corresponding table if not exists.
module.exports.check = async function (data) {
    try {
        var regionName = data.region, cityName = data.city, suburbName = data.suburb;

        var regionId = await connectionPool.query('SELECT region_id from region where region_name = ?', [regionName])
            .then(async d => { return d.length > 0 ? d[0].region_id : null; });
        // if (!regionId) regionId = (await connectionPool.query('INSERT into region (region_name) VALUES (?)', [regionName])).insertId;

        var cityId = await connectionPool.query('SELECT city_id, region_id from city where city_name = ? && region_id = ?', [cityName, regionId])
            .then(async d => { return d.length > 0 ? d[0].city_id : null; });
        // if (!cityId) cityId = (await connectionPool.query('INSERT into city (city_name, region_id) VALUES (?, ?)', [cityName, regionId])).insertId;

        var suburbId = await connectionPool.query('SELECT suburb_id, city_id from suburb where suburb_name = ? && city_id = ?', [suburbName, cityId])
            .then(async d => { return d.length > 0 ? d[0].suburb_id : null; });
        // if (!suburbId) suburbId = (await connectionPool.query('INSERT into suburb (suburb_name, city_id) VALUES (?, ?)', [suburbName, cityId])).insertId;

        delete data.region;
        delete data.city;
        delete data.suburb;

        data["region_id"] = regionId;
        data["city_id"] = cityId;
        data["suburb_id"] = suburbId;
    } catch (error) {
        errorHandler.handle(error);
    }
}