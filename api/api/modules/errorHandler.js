// This module will hadle the errors
module.exports = {
    handle: function (error, res, data) {

        if(data) data.message = "HaHa!"        
        if(res != null && data != null){
            if(data.htStatus)res.status(data.htStatus)
            else res.status(403)
            res.json(data);
        }
        
        var consoleMessage = {type: "", message: error};
        if(error instanceof Error){
            consoleMessage.type = "Error";
        }
        if(error instanceof RangeError){
            consoleMessage.type = "RangeError";
        }
        if(error instanceof ReferenceError){
            consoleMessage.type = "ReferenceError";
        }
        if(error instanceof SyntaxError){
            consoleMessage.type = "SyntaxError";
        }
        if(error instanceof TypeError){
            consoleMessage.type = "TypeError";
        }
        // if(error instanceof SystemError){
        //     consoleMessage.type = "SystemError";
        // }
        // if(error instanceof AssertionError){
        //     consoleMessage.type = "AssertionError";
        // }

        console.error("Error handler: ", consoleMessage);       
        // console.error("Error handler: message   ", error.message);
    }
}