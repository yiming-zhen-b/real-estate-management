const fs = require('fs');

const errorHandler = require('./errorHandler');
const config = require('../../config');

// This module is for the Utilities
module.exports = {
    capitalizeTxt: function(txt) {
        if(typeof txt !== "string" || txt.length == 0) return txt;
        return txt.charAt(0).toUpperCase() + txt.slice(1); //or if you want lowercase the rest txt.slice(1).toLowerCase();
    },
    createPath: function (path) {
        return "." + path;
    },
    createImgPath: function (path) {
        return "./" + path;
    },
    buildLikeValue: function (value) {
        return "%" + value + "%";
    },
    buildRedirect: function (url1, url2) {
        return url2 ? url1 + "?re=" + url2 : url1;
    },
    removeFile: function (path) {
        try {
            fs.unlinkSync(path);
            if (config.debug) console.log(path, " was removed");
        } catch (error) {
            if (error.code === 'ENOENT') {
                console.error(path, " does not exists in the server.");
            } else {
                errorHandler.handle(error);
            }
        }
    },
    getTTLValue: function (options) {
        var d = new Date();

        if (!options.day) options.day = 0;
        if (!options.hour) options.hour = 0;
        if (!options.min) options.min = 0;

        d.setDate(d.getDate() + options.day);
        d.setHours(d.getHours() + options.hour);
        d.setMinutes(d.getMinutes() + options.min);
        return Math.round(d / 1000);
    },
    createRandomPass: function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
}