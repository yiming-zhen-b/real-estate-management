const parse = require('user-agent-parser');

const config = require('../../config');
const encrypt = require('./encrypt');
const dynamodbUti = require('./dynamodb').Utilities;
const errorHandler = require('./errorHandler');
const dyLSTable = config.aws.dynamodb.table.loginStatus;

function parseUserAgent(agent) {   
    var p = parse(agent);                
    return { broswer: p.browser, os: p.os };
}

function checkHeaders(req, res){
    if (!req.hasOwnProperty("headers") || !req.headers.hasOwnProperty("user-agent")) {
        if(res) errorHandler.handle("No header", res, { status: false });
        return false;
    }
    return true;
}

// This module create and verify the token for the website.
module.exports = {
    checkHeaders: checkHeaders,
    encryptUserInfo: function (user_email, role, agent) {
        return encrypt.encrypt(JSON.stringify({ email: user_email, role: JSON.stringify(role), agent: parseUserAgent(agent) }), "128");
    },
    checkAuth: async function (req, res) {
        var returnResult = { status: false, message: "Please login first.", login_alert: true, auth_alert: true };                            
        try {
            if (checkHeaders(req) && req.headers.authorization) {
                // console.log(headers.authorization);
                // console.log(authorization.decryptUserInfo(headers.authorization));
                var headers = req.headers;
                var getResult = await dynamodbUti.getItem({
                    TableName: dyLSTable.name,
                    Key: { [dyLSTable.key]: headers.authorization },
                    AttributesToGet: ["sqlEntity_id", "user_email", "user_name", 'role', "auth_id"]
                });
                
                var agent = parseUserAgent(headers["user-agent"])
                var decrypted = JSON.parse(encrypt.decrypt(headers.authorization, "128"));

                if (getResult.status){
                    if(JSON.stringify(decrypted.agent) === JSON.stringify(agent) || decrypted.email === getResult.data.user_email) {
                        returnResult.status = true;
                        returnResult.data = getResult.data;
                        return returnResult;
                    }else{
                        errorHandler.handle("Authorization Compare Error");
                    }
                }
            }
        }
        catch (error) {
            errorHandler.handle(error, { status: false });
        }
        if(res) res.json(returnResult);
        return returnResult;        
    }
}