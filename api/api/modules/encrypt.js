const crypto = require('crypto');
const hmac = crypto.createHmac('sha256', 'a secret');

// key = Buffer.from(crypto.randomBytes(16)),
// iv = Buffer.from(crypto.randomBytes(16));


// This module is for the encryption
var algorithm = {
    "128": { name: 'aes-128-cbc', key: Buffer.from("16b52cc529b34eeb"), iv: Buffer.from("b9b094dc6fa8386b") },
    "192": { name: 'aes-192-cbc'}
}

function doEncrypt(text, al) {
    var cipher = crypto.createCipheriv(algorithm[al].name, algorithm[al].key, algorithm[al].iv)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function doDecrypt(text, al) {
    var decipher = crypto.createDecipheriv(algorithm[al].name, algorithm[al].key, algorithm[al].iv)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

module.exports = {
    encrypt: doEncrypt,
    decrypt: doDecrypt,
    encryptJSONKeys: function (data) {
        var newData = {};
        for (var key in data) { newData[doEncrypt(key, "128")] = data[key]; }
        return newData;
    },
    encryptJOSNArrayValueByKey: function (data, key) {
        data.forEach(element => { element[key] = doEncrypt(element[key].toString(), "128") });
        return data;
    },
    encryptID: function (data) {
        return doEncrypt(data.toString(), "128");
    },
    decryptID: function (data) {
        return doDecrypt(data.toString(), "128");
    },
    decryptIDArray: function (data) {
        var newData = [];
        data.forEach(element => { newData.push(doDecrypt(element.toString(), "128")); });
        return newData;
    },
    hash: function (text) {
        hmac.update(text);
        return hmac.digest('hex');
    }
}