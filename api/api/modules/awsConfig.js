const AWS = require('aws-sdk');
const config = require('../../config');

AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: 'homely-account' });

// This module is for the aws configuration
module.exports = function (target) {
    var options = {};
    if (target == "dynamodb") {
        options = {
            region: config.aws.dynamodb.region,
            endpoint: config.aws.dynamodb.endpoint
        };
        AWS.config.update(options);
        return AWS;
    }
    if (target == "s3") {
        options = {
            region: config.aws.s3.region,
            // endpoint: null
        };
        AWS.config.update(options);
        return new AWS.S3()
    }
    // AWS.config.update(options);
    // return AWS;
}