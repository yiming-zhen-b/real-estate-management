
const connectionPool = require('./database');
const errorHandler = require('./errorHandler');
const removeImage = require('./removeImage');
const config = require('../../config');

module.exports.remove = async function (propertyIdArray) {
    var remReturn = {status: true};
    try {
        var imgs = await connectionPool.query('SELECT * FROM property_image WHERE property_id in (?)', [propertyIdArray]);
        await removeImage.remove(imgs);
        await connectionPool.query('DELETE FROM property WHERE property_id in (?);', [propertyIdArray]);
        if(config.debug) console.log("Reomved property ", propertyIdArray);
    } catch (error) {
        remReturn.status = false
        errorHandler.handle(error);
    }
    return remReturn;
}