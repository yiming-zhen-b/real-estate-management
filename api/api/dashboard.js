//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const authorization = require('./modules/authorization');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const config = require('../config');
const utilities = require("./modules/utilities");
const encrypt = require('./modules/encrypt');
const awsS3 = require('./modules/awsS3');
const dashEditPara = require('./modules/searchPropertyQuery').dashEditPara;
const dashEditAllAppPara = require('./modules/searchPropertyQuery').dashEditAllAppPara;
const getAllImg = require('./proDetail').getAllImg;
const SCRChecker = require("./modules/SCRChecker");
const removeProperty = require("./modules/removeProperty");
const removeImage = require("./modules/removeImage");

// ROUTES
// =============================================================================
// This module is the dashboard
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will get all the applications.
router.post("/getAllApp", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;

        var entity_id = auResult.data.sqlEntity_id;
        var resResult = { status: true };

        resResult.data = await connectionPool.query("select available, listed_date, property_id as id, property_for, suburb_name as suburb, city_name as city, region_name as region, " +
            "rent_pw, street_number, unit_flat_number, street_name, disabled from " +
            dashEditAllAppPara.from + ' where ' + dashEditAllAppPara.where, [entity_id]);

        resResult.data = encrypt.encryptJOSNArrayValueByKey(resResult.data, "id");
        // console.log(resResult);
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the applications.
router.post("/changeProStatus", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;

        var entity_id = auResult.data.sqlEntity_id;
        var data = JSON.parse(req.body.data),
            propertyId = encrypt.decryptID(data.id),
            status = data.status,
            newStatus = data.newStatus;
        var resResult = { status: true };

        await connectionPool.query("update property set disabled = ? where property_id = ? && entity_id = ? && disabled = ?", [newStatus, propertyId, entity_id, status]);

        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the details of a property.
router.post("/getDetails", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var propertyId = encrypt.decryptID(req.body.data);
        var result = { status: true, data: {} };
        result.data = await connectionPool.query('SELECT ' + dashEditPara.select + " from " + dashEditPara.from + ' where ' + dashEditPara.where, [propertyId]);
        result.data = encrypt.encryptJOSNArrayValueByKey(result.data, "category_id");
        await getAllImg(result.data, propertyId);
        res.json(result);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the image of a property.
router.post("/getImages", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var propertyId = encrypt.decryptID(req.body.data);
        var result = { status: true, data: [{ img: [] }] };
        await getAllImg(result.data, propertyId);
        res.json(result);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will update the property data.
router.post("/updateProperty", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var entity_id = auResult.data.sqlEntity_id;

        var result = { status: true, data: {} };
        var data = JSON.parse(req.body.data);
        var pro_id = encrypt.decryptID(data.pro_id);
        delete data.pro_id;
        if (data.category_id) data.category_id = encrypt.decryptID(data.category_id);
        // check region city and suburb
        if (data.region && data.city && data.suburb) await SCRChecker.check(data);

        var set = [];
        for (var nKey in data) {
            set.push(nKey + " = '" + data[nKey] + "'");
        }
        if (config.debug) console.log(data);
        if (config.debug) console.log(set);
        await connectionPool.query('UPDATE property SET ' + set.join(", ") + ' where property_id = ' + pro_id + ' && entity_id = ' + entity_id)
            .catch(error => {
                result.status = false;
                if (error.code === "ER_DUP_ENTRY") {
                    result.message = "This property already exists."
                    result.is_alert = true;
                }
            });
        res.json(result);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will remove the property.
router.post("/remove", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true };
        var p_id = encrypt.decryptID(req.body.data);
        await removeProperty.remove([p_id]);
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will update the image data.
router.post("/modifiyImage", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var result = { status: true };
        var data = JSON.parse(req.body.data);
        if (data.removed.length) {
            var img_id = encrypt.decryptIDArray(data.removed);
            var imgs = await connectionPool.query('SELECT * FROM property_image WHERE img_id in (?);', [img_id]);
            await removeImage.remove(imgs);
            if (config.debug) console.log(img_id);
        }
        if (Object.keys(data.modified).length !== 0) {
            for (var mKey in data.modified) {
                if (config.debug) console.log(data.modified);
                await connectionPool.query("UPDATE property_image SET caption = ?,img_type = ?,is_cover = ? WHERE img_id = ?",
                    [data.modified[mKey].caption, data.modified[mKey].img_type, data.modified[mKey].is_cover, encrypt.decryptID(mKey)]);
            }
        }
        res.json(result);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will update the image data.
router.post("/matchTenants", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var result = { status: true, data: [] };

        var pro_id = encrypt.decryptID(req.body.data);

        if (auResult.data.role.is_agent || auResult.data.role.is_landlord) {
            result.data = await matchTenants(pro_id);
            result.data = encrypt.encryptJOSNArrayValueByKey(result.data, "tenant_id");
        }
        res.json(result);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

async function matchTenants(pro_id){
    var query = {
        select: ["distinct(preference.entity_id) as tenant_id", "about_me", "first_name", "last_name", "email_address", "mobile_number", "landline_number"].join(", "),
        from: ["tenant_preference as preference left join entity on preference.entity_id = entity.entity_id", "tenant", "property"].join(", "),
        where: ["preference.entity_id = tenant.entity_id", "tenant.is_looking = 'true'", "tenant.access_control in (1, 2, 3)", 
            "preference.region_id = property.region_id", 
            "IFNULL(preference.city_id = property.city_id, TRUE)", "IFNULL(preference.suburb_id = property.suburb_id, TRUE)", 
            "IFNULL(preference.bedroom_min <= property.bedrooms, TRUE)", "IFNULL(preference.bedroom_max >= property.bedrooms, TRUE)", 
            "IFNULL(preference.max_rent_pw >= property.rent_pw, TRUE)", "property_id = ?"].join(" && ")
    };
    // console.log("select " + query.select + " from " + query.from + " where " + query.where, [pro_id])
    return await connectionPool.query("select " + query.select + " from " + query.from + " where " + query.where, [pro_id]);
}

//Return the router for the server
module.exports = router;
module.exports.matchTenants = matchTenants;