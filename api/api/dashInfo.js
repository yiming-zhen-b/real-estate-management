//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const authorization = require('./modules/authorization');
const connectionPool = require('./modules/database');
const config = require('../config');
const utilities = require("./modules/utilities");
const encrypt = require('./modules/encrypt');
// ROUTES
// =============================================================================
// This module is the dashboard
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will get the user info
router.post("/get", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var entity_id = auResult.data.sqlEntity_id;

        var resResult = { status: true };
        if (auResult.data.role.is_agent) {
            resResult.data = await connectionPool.query(
                "select licence_number, on_hold, email_address,first_name,last_name,is_agent,img_url, mobile_number, landline_number " +
                "from entity left join agent on entity.entity_id = agent.entity_id where entity.entity_id = ?", [entity_id]);
        }
        if (auResult.data.role.is_landlord) {
            resResult.data = await connectionPool.query(
                "select on_hold, email_address,first_name,last_name,is_landlord,img_url, mobile_number, landline_number " +
                "from entity left join landlord on entity.entity_id = landlord.entity_id where entity.entity_id = ?", [entity_id]);
        }
        if (auResult.data.role.is_tenant) {
            resResult.data = await connectionPool.query(
                "select access_control, about_me, email_address,first_name,last_name,is_tenant,img_url, mobile_number, landline_number " +
                "from entity left join tenant on entity.entity_id = tenant.entity_id where entity.entity_id = ?", [entity_id]);
        }
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});
// This will update the user info
router.post("/update", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;

        var entity_id = auResult.data.sqlEntity_id,
            resResult = { status: true },
            data = JSON.parse(req.body.data);
        if (auResult.data.role.is_agent) {
            await updateTable("agent", ["licence_number", "is_agent"], data, entity_id)
        }
        if (auResult.data.role.is_landlord) {

        }
        if (auResult.data.role.is_tenant) {
            await updateTable("tenant", ["access_control", "is_looking", "about_me"], data, entity_id)
        }
        res.json(resResult);
        async function updateTable(tableName, cols) {
            var query = { col: [], value: [] };
            for (var i = 0; i < cols.length; i++) {
                if (data.hasOwnProperty(cols[i])) {
                    query.col.push(cols[i] + " = ?");
                    query.value.push(data[cols[i]]);
                }
                delete data[cols[i]];
            }
            query.value.push(entity_id);
            if (query.col.length) {
                await connectionPool.query('UPDATE ' + tableName + ' SET ' + query.col.join(",") + ' where entity_id = ?', query.value);
            }
            var queryAll = { col: [], value: [] };
            for (var nKey in data) {
                queryAll.col.push(nKey + " = ?");
                queryAll.value.push(data[nKey]);
            }
            if (queryAll.col.length) {
                queryAll.value.push(entity_id);
                await connectionPool.query('UPDATE entity SET ' + queryAll.col.join(",") + ' where entity_id = ?', queryAll.value);
            }
        }
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get is a tenant is looking.
router.post("/isLooking", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true };
        if (auResult.data.role.is_tenant) {
            resResult.is_looking = (await connectionPool.query("select is_looking from tenant where entity_id = ?", [auResult.data.sqlEntity_id]))[0].is_looking;
        }
        res.json(resResult);
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the access controls.
router.post("/getAllAccessCon", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        res.json(await connectionPool.query("select * from tenant_access_control_options"));
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get user landline and mobile for listing property.
router.post("/getUserDetail", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var entity_id = auResult.data.sqlEntity_id;
        var data = await connectionPool.query("select mobile_number, landline_number from entity where entity_id = ?", [entity_id]);

        res.json(data);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;