//required models & packages
const express = require('express');
const dateTime = require('node-datetime');

// Import modules
const config = require('../config');
const utilities = require("./modules/utilities");
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const googleVerify = require('./modules/verifyGoogleToken');
const encrypt = require('./modules/encrypt');
const authorization = require('./modules/authorization');
const email = require('./modules/email');
const verifyCode = require("./modules/verifyCode");
const dyUserTable = config.aws.dynamodb.table.user;
const dyLSTable = config.aws.dynamodb.table.loginStatus;
const verifyCodeTable = config.aws.dynamodb.table.verifyCode;


// ROUTES
// =============================================================================
// This module is for the login, logout, register, Google, and facebook login.
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will create the verification code, put the code into dynamodb with ttl specified in the config.json
router.post("/getVeriCode", async function (req, res, next) {
    try {
        if (!authorization.checkHeaders(req, res)) return;
        const data = JSON.parse(req.body.data),
            mode = data.mode,
            user_email = data.user_email;
        // Check if this is a valid email
        var resResult = checkEmail(user_email);
        var countResult = await connectionPool.query("select count(entity_id) as count from entity where email_address = ?", [user_email]);
        if (resResult.status) {
            if (mode !== "register" && countResult[0].count === 0) {
                resResult.status = false;
                resResult.error_field.push({ id: ".user_email", message: "This email does not exists." });
            } else {
                // Create the varification code and send it to user email
                var putResult = await verifyCode.createAndSendEmail(user_email, "forgot password", "forgot_pass");
                resResult.seconds = putResult.data.ttl - putResult.data.created;
            }
        }
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will verify the verification code and change the user password
router.post("/setNewPass", async function (req, res, next) {
    try {
        if (!authorization.checkHeaders(req, res)) return;
        const data = JSON.parse(req.body.data),
            user_email = data.user_email,
            code = data.code,
            user_pass = data.user_pass;
        // Check if this is a valid email and password
        var resResult = await checkUserInput({ signUp: false, verifyCode: true }, { code: code, user_email: user_email, user_pass: user_pass });
        if (resResult.status) {
            var updateResult = await connectionPool.query("UPDATE entity SET password = ? where email_address = ?", [user_pass, user_email])
                .catch(async error => { resResult.status = false; });
            if (updateResult.affectedRows == 0) {
                resResult.status = false;
                resResult.error_field.push({ id: ".user_email", message: "This email does not exists." });
            }
        }
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will handle the login action, check the user email and password
router.post("/login", async function (req, res, next) {
    try {
        if (!authorization.checkHeaders(req, res)) return;
        const data = JSON.parse(req.body.data),
            user_email = data.user_email,
            user_pass = data.user_pass;
        // Check if this is a valid email and password

        var resResult = await checkUserInput({ signUp: false }, { user_email: user_email, user_pass: user_pass });

        if (resResult.status) {
            const result = await connectionPool.query("SELECT is_admin, is_landlord, is_buyer, is_tenant, is_agent, entity_id, first_name, last_name from entity where email_address = ? && password = ?", [user_email, user_pass]);
            if (result.length === 1) {
                // Create the token and store the token in the dynamodb
                var role = {
                    is_admin: result[0].is_admin == "true" ? true : false, is_landlord: result[0].is_landlord == "true" ? true : false,
                    is_buyer: result[0].is_buyer == "true" ? true : false, is_tenant: result[0].is_tenant == "true" ? true : false,
                    is_agent: result[0].is_agent == "true" ? true : false
                };
                resResult.u = authorization.encryptUserInfo(user_email, role, req.headers["user-agent"]);
                await storeLoginStatus(resResult.u, result[0].entity_id, user_email, result[0].first_name, result[0].last_name, config.auth_id.home, role);
            }
            else {
                resResult.status = false;
                resResult.error_field.push({ id: ".user_email", message: "Email is wrong" });
                resResult.error_field.push({ id: ".user_pass", message: "Password is wrong" });
            }
        }
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// Handle signUp request, it will check if the data is valid before insert into mysql database.
router.post("/register", async function (req, res, next) {
    try {
        if (!authorization.checkHeaders(req, res)) return;
        var data = JSON.parse(req.body.data),
            sourceCheck = false,
            code = data.code,
            source = data.source,
            user_pass = data.user_pass,
            user_pass_again = data.user_pass_again,
            first_name = data.first_name,
            user_email = data.user_email,
            last_name = data.last_name,
            register_as = data.register_as,
            licence_number = data.licence_number;

        console.log(data);

        // Check if the data is valid
        var resResult = await checkUserInput({ signUp: true, verifyCode: true },
            {
                code: code, user_email: user_email, user_pass: user_pass, last_name: last_name,
                first_name: first_name, user_pass_again: user_pass_again, register_as: register_as, licence_number: licence_number
            });
        if (source === config.auth_id.home && resResult.status) {
            // Insert into mysql database
            resResult = await insertNewUser(first_name, last_name, user_email, user_pass, register_as, licence_number, config.auth_id.home);
            if (resResult.status) {
                var role = resResult.is_tenant ? {is_tenant: true} : {};
                resResult.u = authorization.encryptUserInfo(user_email, role, req.headers["user-agent"]);
                await storeLoginStatus(resResult.u, resResult.insertId, user_email, first_name, last_name, config.auth_id.home, role);
            }
            delete resResult.insertId, delete resResult.is_tenant;
        } else {
            resResult.status = false;
        }
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This is used by /register and /loginGoogle api to insert a new user into mysql database.
async function insertNewUser(first_name, last_name, user_email, user_pass, register_as, licence_number, auth_id) {
    var resResult = { status: true, error_field: [] }
    if (register_as === "is_agent" || register_as === "is_lanlord") register_as_value = "false";
    await connectionPool.query("INSERT INTO entity (first_name, last_name, email_address, password, auth_id, " + register_as + ") VALUES (?, ?, ?, ?, ?, ?)",
        [first_name, last_name, user_email, user_pass, auth_id, register_as == "is_tenant" ? "true" : "false"])
        .then(async d => {
            resResult.insertId = d.insertId;
            if (register_as === "is_tenant") {
                resResult.is_tenant = true;
                connectionPool.query("INSERT INTO tenant (entity_id) VALUES (?)", [d.insertId]);
            }
            if (register_as === "is_agent") {
                connectionPool.query("INSERT INTO agent (entity_id, licence_number) VALUES (?,?)", [d.insertId, licence_number]);
                email.sendRegistrationAsAgentAndLanlordEmail("register_as_agent", { user_email: user_email, first_name: first_name, last_name: last_name, licence_number: licence_number });
            }
            if (register_as === "is_landlord") {
                connectionPool.query("INSERT INTO lanlord (entity_id) VALUES (?)", [d.insertId]);
                email.sendRegistrationAsAgentAndLanlordEmail("register_as_landlord", { user_email: user_email, first_name: first_name, last_name: last_name });
            }
        })
        .catch(async error => {
            if (error.code === "ER_DUP_ENTRY") resResult.error_field.push({ id: ".user_email", message: "This email is already exists" });
            else errorHandler.handle(error);
            resResult.status = false;
        });
    return resResult;
}

// The api for the Google login, it will verify the token, then check if the use is already exists in the mysql databse, insert a new one if not
router.post("/thirdParty", async function (req, res, next) {
    try {
        if (!authorization.checkHeaders(req, res)) return;
        const data = JSON.parse(req.body.data),
            user_id = data.user_id,
            code = data.code,
            source = data.source,
            user_pass = data.user_pass,
            user_pass_again = data.user_pass_again,
            register_as = data.register_as,
            licence_number = data.licence_number;
        var user_email = undefined,
            first_name = undefined,
            last_name = undefined,
            aim = undefined;

        var resResult = { status: false };
        // Verify the google token
        if (source === config.auth_id.google) {
            var verifyResult = await googleVerify.verify(user_id);
            resResult.status = verifyResult.status;
            if (verifyResult.status) {
                aim = "new google user";
                user_email = verifyResult.data['email'];
                first_name = verifyResult.data['given_name'];
                last_name = verifyResult.data['family_name'];
            }
        }
        // Verify the facebook
        if (source === config.auth_id.facebook) {
            aim = "new facebook user";
            user_email = data.user_id;
            var name = data.name.split(" ");
            first_name = name[0];
            if (name.length == 1) {
                last_name = name[0];
            } else {
                last_name = name[1];
            }
        }
        const result = await connectionPool.query("SELECT is_admin, is_landlord, is_buyer, is_tenant, is_agent, entity_id from entity where email_address = ?", [user_email]);
        var entity_id, role;
        if (result.length === 1) {
            resResult.status = true;
            entity_id = result[0].entity_id;
            role = {
                is_admin: result[0].is_admin == "true" ? true : false, is_landlord: result[0].is_landlord == "true" ? true : false,
                is_buyer: result[0].is_buyer == "true" ? true : false, is_tenant: result[0].is_tenant == "true" ? true : false,
                is_agent: result[0].is_agent == "true" ? true : false
            };
        }
        else {
            resResult = await checkUserInput({ signUp: true, verifyCode: true },
                {
                    code: code, user_email: user_email, user_pass: user_pass, last_name: last_name,
                    first_name: first_name, user_pass_again: user_pass_again, register_as: register_as, licence_number: licence_number
                });
            if (resResult.status) {
                // Insert into mysql database
                var insertResult = await insertNewUser(first_name, last_name, user_email, user_pass, register_as, licence_number, source);
                resResult.status = insertResult.status;
                entity_id = insertResult.insertId;
                role = insertResult.is_tenant ? {is_tenant: true} : {};
            } else {
                resResult.newUser = true;
                var putResult = await verifyCode.createAndSendEmail(user_email, aim, "register");
                if (putResult.status) {
                    resResult.seconds = putResult.data.ttl - putResult.data.created;
                    resResult.email = user_email;
                } else {
                    if (putResult.error.code === "ConditionalCheckFailedException") {
                        resResult.showModal = true;
                    }
                }
            }
        }
        if (resResult.status) {
            resResult.u = authorization.encryptUserInfo(user_email, role, req.headers["user-agent"]);
            await storeLoginStatus(resResult.u, entity_id, user_email, first_name, last_name, config.auth_id.google, role);
        }
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// The api will verify the token to see if the user is logged in or not.
router.post("/checkLoginStatus", async function (req, res, next) {
    try {
        var retResult = { status: true };

        var checkResult = await authorization.checkAuth(req, res);
        if (!checkResult.status) return;

        retResult.user_name = checkResult.data.user_name;
        retResult.auth_id = checkResult.data.auth_id;
        retResult.user_email = checkResult.data.user_email;
        retResult.role = checkResult.data.role;
        for (var rKey in retResult.role) {
            if (!retResult.role[rKey]) {
                delete retResult.role[rKey];
            }
        }
        res.json(retResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
})

// This api will log out the user, and remove the token from dynamodb.
router.post("/logout", async function (req, res, next) {
    try {
        var resResult = { status: false };
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = await logout(auResult.data.sqlEntity_id, req.headers.authorization);
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

async function logout(entity_id, authorization){
    await connectionPool.query("update entity set session_token = '' where entity_id = ?", [entity_id]);
    return await dynamodbUti.removeItem(dyLSTable.name, { [dyLSTable.key]: authorization });
}

// This is used by /login, /register and /loginGoogle api, it will store the session data (token) into dynamodb.
async function storeLoginStatus(encrypKey, sqlEntity_id, user_email, first_name, last_name, auth_id, role) {
    var values = {
        [dyLSTable.key]: encrypKey,
        "sqlEntity_id": sqlEntity_id,
        "user_email": user_email,
        "user_name": first_name + (last_name ? " " + last_name : ""),
        "auth_id": auth_id,
        "role": role,
        "ttl": utilities.getTTLValue(config.ttl.session),
        "created": Math.round(new Date() / 1000)
    };
    await connectionPool.query("update entity set session_token = ? where entity_id = ?", [encrypKey, sqlEntity_id]);
    return await dynamodbUti.putNewItem(true, dyLSTable.name, values);
}

// THie function checks the user input
async function checkUserInput(mode, values) {
    var retResult = checkEmail(values.user_email);
    if (!values.user_pass) {
        retResult.status = false;
        retResult.error_field.push({ id: ".user_pass", message: "Password is empty" });
    }
    if (mode.signUp) {
        if (!values.user_pass_again || values.user_pass !== values.user_pass_again) {
            retResult.status = false;
            retResult.error_field.push({ id: ".user_pass", message: "Passwords are different" });
        }
        if (!values.last_name) {
            retResult.status = false;
            retResult.error_field.push({ id: ".last_name", message: "Last name is empty" });
        }
        if (!values.first_name) {
            retResult.status = false;
            retResult.error_field.push({ id: ".first_name", message: "First name is empty" });
        }
        if (!values.register_as || values.register_as === "is_admin" || !(values.register_as === "is_tenant" || values.register_as === "is_landlord" || values.register_as === "is_agent")) {
            retResult.status = false;
            retResult.error_field.push({ id: ".sign_up_radio", message: "Please select a character" });
        }
        if (values.register_as === "is_agent" && !values.licence_number) {
            retResult.status = false;
            retResult.error_field.push({ id: ".licence_number", message: "Please enter a licence number" });
        }
    }
    if (retResult.status && mode.verifyCode) {
        var getResult = await verifyCode.getVerifyCode(values.user_email);
        if (getResult.data && getResult.data.code === values.code) {
            await verifyCode.removeVerifyCode(values.user_email);
        } else {
            retResult.status = false;
            retResult.error_field.push({ id: ".veri_code", message: "Verification code is wrong." });
        }
    }
    return retResult;
}

function checkEmail(user_email) {
    var retResult = { status: true, error_field: [], redirect: "/" };
    if (!user_email || user_email.match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
        retResult.status = false;
        retResult.error_field.push({ id: ".user_email", message: "Valid email is: a@b.c" });
    }
    return retResult;
}


//Return the router for the server
module.exports = router;
module.exports.logout = logout;