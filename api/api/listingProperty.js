//required models & packages
const express = require('express');
const crypto = require("crypto");

// Import modules
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const imgResize = require('./modules/imgResize');
const encrypt = require('./modules/encrypt');
const authorization = require('./modules/authorization');
const config = require('../config');
const utilities = require("./modules/utilities");
const multerUpload = require("./modules/multerUpload").upload;
const dynamodbUti = require('./modules/dynamodb').Utilities;
const awsS3 = require('./modules/awsS3');
const SCRChecker = require("./modules/SCRChecker");
const email = require("./modules/email");
const matchTenants = require('./dashboard').matchTenants;

// ROUTES
// =============================================================================
// This module is for uploading the property
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This api will upload the imformation to the mysql database.
router.post("/upload", async function (req, res, next) {
    try {
        var returnResult = { status: true, data: "", redirect: "/", is_alert: false, message: "The property has been uploaded." };

        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var entity_id = auResult.data.sqlEntity_id;

        if(!auResult.data.role.is_agent && !auResult.data.role.is_landlord) {
            res.json({status: false, redirect: "/", message: "Please wait for registration approval"});
            return;
        }

        var data = JSON.parse(req.body.data);
        // check uploaded data.
        var cheResult = checkValues(data);
        if (!cheResult.status) {
            res.json(cheResult);
            return;
        }
        var proLocations = [];
        ["street_number", "unit_flat_number", "street_name", "suburb", "city", "region"].forEach(key => {
            proLocations.push(data[key]);
        })
        
        if (config.debug) console.log(data);
        // if (data.is_agent == "true") {
        //     await connectionPool.query("select count(entity_id) as count from agent where entity_id = ?", [entity_id])
        //         .then(async d => {
        //             if (d[0].count === 0) {
        //                 await connectionPool.query("update entity set is_agent = 'true' where entity_id = ?", [entity_id]);
        //                 await connectionPool.query("update entity set licence_number = ?, on_hold = 'true' where entity_id = ?", [data.licence_number, entity_id]);                       
        //             }
        //         });
        // }
        if (data.show_landline == "true") {
            await connectionPool.query("select landline_number from entity where entity_id = ?", [entity_id])
                .then(async d => {
                    if (d.length === 1 && !d[0].landline_number) {
                        await connectionPool.query("update entity set landline_number = ? where entity_id = ?", [data.landline_number, entity_id])
                    }
                });
        }
        if (data.show_mobile == "true") {
            await connectionPool.query("select mobile_number from entity where entity_id = ?", [entity_id])
                .then(async d => {
                    if (d.length === 1 && !d[0].mobile_number) {
                        await connectionPool.query("update entity set mobile_number = ? where entity_id = ?", [data.mobile_number, entity_id])
                    }
                });
        }
        delete data.is_agent, delete data.licence_number, delete data.landline_number, delete data.mobile_number;
        // check region city and suburb
        await SCRChecker.check(data);

        var qurResult = { query: "", col: [], values: [], questionMark: [] };
        data["entity_id"] = entity_id;
        data["category_id"] = encrypt.decryptID(data["category_id"]);

        for (var k in data) {
            if (data[k]) {
                qurResult.col.push(k);
                qurResult.values.push(data[k]);
                qurResult.questionMark.push("?");
            }
        }
        if (qurResult.col.length > 0) {
            qurResult.query = "INSERT INTO property (" + qurResult.col.join(", ") + ") VALUES (" + qurResult.questionMark.join(", ") + ")";
            await connectionPool.query(qurResult.query, qurResult.values)
                .then(async d => { 
                    returnResult.data = encrypt.encryptID(d.insertId);
                    var matchedTenants = await matchTenants(d.insertId);
                    console.log(matchedTenants);
                    if(matchedTenants.length){
                        returnResult.show_matched_tenants = true;
                        returnResult.matchedTenants = matchedTenants.length;
                        email.sendNewPropertyNotiToTenant(matchedTenants, returnResult.data, proLocations);                        
                    }
                })
                .catch(error => {
                    returnResult.status = false;
                    if (error.code === "ER_DUP_ENTRY") {
                        returnResult.message = "This property already exists."
                        returnResult.is_alert = true;
                    }else{
                        errorHandler.handle(error);
                    }
                });
        }
        res.json(returnResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// router.post('/uploadImages', awsS3.array('images', 10), async function (req, res, next) {
//     try {
//         var resResult = { status: true }
//         console.log(req.body.caption, req.body.img_type, req.files);
//         // var getResult = await dynamodbUti.getItem({
//         //     TableName: dyProEncyptTable.name,
//         //     Key: { [dyProEncyptTable.key]: req.body.c },
//         //     AttributesToGet: ['sql_id']
//         // });

//         // resResult.status = getResult.status;
//         // if (getResult.status && getResult.data) {
//         //     if (getResult.data.sql_id) {
//         //         imgResize.resize(req.files, getResult.data.sql_id, req.body.caption, req.body.img_type);
//         //     }
//         // }

//         console.log(req.body.c, req.files);
//         imgResize.resize(req.files, 1, req.body.caption, req.body.img_type);

//         res.json(resResult);
//     }
//     catch (error) {
//         errorHandler.handle(error, res, { status: false });
//     }
// });
// This api will upload the image to S3 and insert the image name to mysql database 
var singleUplodad = multerUpload.single('images');
router.post('/uploadImages', async function (req, res, next) {
    try {
        var resResult = { status: true };        
        singleUplodad(req, res, async function (error, some) {
            if (error) {
                console.log(error);
                errorHandler.handle(error, res, { status: false, message: error, htStatus: 403 });
                return;
            }
            else {
                if (config.debug) console.log(req.body, req.file);   
                const query = "INSERT INTO property_image (property_id, org_img, thumb_img, caption, img_type, is_cover, originalname) VALUES (?, ?, ?, ?, ?, ?, ?)";
                await connectionPool.query(query, [req.body.c, req.file.filename, req.file.filename, req.body.caption, req.body.img_type, req.body.is_cover, req.file.originalname]);
                if (!(await awsS3.startUploadImage(req.file)).status) {
                    resResult.status = false;
                    resResult.message = "Image upload failed."
                }
            }
        });
        res.json(resResult);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will check the property information to see if some information is missing.
function checkValues(data) {
    var retResult = { status: true, error_field: [], type: 0, message: "More data needed." };
    var valueCheck = {
        property_for: ".listing-property-for-span", category_id: ".listing-property-type-span",
        rent_pw: ".rent-per-week-span", region: ".region-span", city: ".city-span", suburb: ".suburb-span",
        available: ".available-from-span", bedrooms: ".bedroom-span", bathrooms: ".bathroom-span",
    };
    for (var vKey in valueCheck) {
        if (data[vKey] == null) {
            retResult.status = false;
            retResult.error_field.push(vKey === "property_for" || vKey === "property_id" ? "#category_tab a" : "#details_tab a");
            retResult.error_field.push(valueCheck[vKey]);
        }
    }
    return retResult;
}

//Return the router for the server
module.exports = router;