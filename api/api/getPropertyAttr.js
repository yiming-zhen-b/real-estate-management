//required models & packages
const express = require('express');

// Import modules
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const dynamodbUti = require('./modules/dynamodb').Utilities;
const encrypt = require('./modules/encrypt');

const config = require('../config');

// ROUTES
// =============================================================================
// This module is for the property attributes
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will get all the regions
router.get("/getRegion", async function (req, res, next) {
    try {
        var regions = await connectionPool.query('SELECT region_name, region_id from region order by region_id');
        res.json(encrypt.encryptJOSNArrayValueByKey(regions, "region_id"));
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the city according the region id
router.get("/getCity", async function (req, res, next) {
    try {
        var result = { status: true, error_field: [] };
        const region_id = encrypt.decryptID(req.query.data);
        var citys = await connectionPool.query('SELECT city_name, city_id from city where region_id = ? order by city_id', [region_id])
        res.json(encrypt.encryptJOSNArrayValueByKey(citys, "city_id"));
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });        
    }
});

// This will get all the suburbs according the city id
router.get("/getSuburb", async function (req, res, next) {
    try {
        var result = { status: true, error_field: [] };
        const city_id = encrypt.decryptID(req.query.data);
        var suburbs = await connectionPool.query('SELECT suburb_name, suburb_id from suburb where city_id = ? order by suburb_id', [city_id])
        res.json(encrypt.encryptJOSNArrayValueByKey(suburbs, "suburb_id"));
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the property types
router.get("/getPropertyType", async function (req, res, next) {
    try {
        var typeIds = await connectionPool.query('SELECT * from category order by category_name');
        res.json(encrypt.encryptJOSNArrayValueByKey(typeIds, "category_id"))
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the values of the bathroom, bedroom and garage.
router.get("/getBBR", async function (req, res, next) {
    try {
        res.json(await connectionPool.query("SELECT * FROM config WHERE attr_name IN ('max_bath_num', 'max_bed_num', 'max_rent')"));
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all the image types of the property.
router.get("/getProImageTypes", async function (req, res, next) {
    try {
        res.json(await connectionPool.query("SELECT name FROM property_image_type"));
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

//Return the router for the server
module.exports = router;