//required models & packages
const express = require('express');
const fs = require('fs');

// Import modules
const errorHandler = require('./modules/errorHandler');
const authorization = require('./modules/authorization');
const connectionPool = require('./modules/database');
const config = require('../config');
const utilities = require("./modules/utilities");
const encrypt = require('./modules/encrypt');
const awsS3 = require('./modules/awsS3');
const removeImage = require("./modules/removeImage");
const uploadDocs = require("./modules/multerUpload").uploadDocs;
// ROUTES
// =============================================================================
// This module is the dashboard
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// This will get max file size.
router.post("/getMaxFileSize", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true };
        resResult.data = await connectionPool.query("select * from documents_types");
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will upload a doc.
var singleUplodad = awsS3.uploadDocs.single('doc');
router.post("/upload", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true };
        var entity_id = auResult.data.sqlEntity_id;

        singleUplodad(req, res, async function (error, some) {
            if (error) {
                console.log(error);
                errorHandler.handle(error, res, { status: false, message: error, htStatus: 403 });
                return;
            }
            else {
                var array = req.file.key.split("/"),
                    fileName = array[array.length - 1];
                resResult.id = encrypt.encryptID((await connectionPool.query("insert into documents (entity_id, document_type_id, url, originalname) values (?, ?, ?, ?)", [entity_id, req.body.doc_type, fileName, req.file.originalname])).insertId);
                resResult.url = config.url.docs + (await connectionPool.query("select s3_path from documents_types where id = ?", [req.body.doc_type]))[0].s3_path + fileName;
                console.log(req.body.doc_type, req.file);
                res.json(resResult);
            }
        });
    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

// This will get all docs.
router.post("/getAll", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true },
            entity_id = auResult.data.role.is_tenant ? auResult.data.sqlEntity_id : encrypt.decryptID(req.body.data);
        resResult.data = await getAllDoc(entity_id);
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});

async function getAllDoc(entity_id) {
    var data = await connectionPool.query("select s3_path, documents_types.name, documents.url, documents.id, document_type_id as doc_type, originalname " +
        "from documents, documents_types where documents.document_type_id = documents_types.id && entity_id = ? order by doc_type",
        [entity_id]);
    data.forEach(element => {
        element.url = config.url.docs + element.s3_path + element.url;
        element.id = encrypt.encryptID(element.id);
        delete element.s3_path;
    });
    return data;
}

// This will remove a doc.
router.post("/remove", async function (req, res, next) {
    try {
        var auResult = await authorization.checkAuth(req, res);
        if (!auResult.status) return;
        var resResult = { status: true };
        await removeImage.removeDoc({ modeSingle: true, doc_id: encrypt.decryptID(req.body.data), entity_id: auResult.data.sqlEntity_id });
        res.json(resResult);

    } catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});
//Return the router for the server
module.exports = router;