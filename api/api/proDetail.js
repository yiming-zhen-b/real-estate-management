//required models & packages
const express = require('express');

// Import modules
const errorHandler = require('./modules/errorHandler');
const connectionPool = require('./modules/database');
const encrypt = require('./modules/encrypt');
const config = require('../config');
const detailPara = require('./modules/searchPropertyQuery').detailPara;
// const sqlPara = require('./searchProperty').sqlPara;

// ROUTES
// =============================================================================
// This module is for the property detail
const router = express.Router();

// logs all the api requests
router.use(function (req, res, next) {
    // TODO log the request here
    next(); // make sure we go to the next routes and don't stop here
});

// The api for retrieving a property detail, when the user click a search result.
router.post("/getDetails", async function (req, res, next) {
    try {
        var propertyId = encrypt.decryptID(req.body.data);
        var result = { status: true, data: {} };
        result.icon = config.url.client_side_static.img.icon;        
        result.data = await connectionPool.query('SELECT ' + detailPara.select + ' from ' + detailPara.from + ' where ' + detailPara.where, [propertyId]);
        await getAllImg(result.data, propertyId);
        res.json(result);
    }
    catch (error) {
        errorHandler.handle(error, res, { status: false });
    }
});
// This is used by /getDetails api, it will get all the images by a property id.
async function getAllImg(data, propertyId) {
    try {
        var imgs = await connectionPool.query("select * from property_image where property_id = ? order by img_id", [propertyId]);
        encrypt.encryptJOSNArrayValueByKey(imgs, "img_id");
        data[0].img = [];
        imgs.forEach(e => data[0].img.push({
            id: e.img_id,
            t_img: config.url.uploaded_img.pro.thumb + e.thumb_img,
            o_img: config.url.uploaded_img.pro.original + e.org_img,
            caption: e.caption,
            img_type: e.img_type,
            is_cover: e.is_cover,
            originalname: e.originalname
        }));
    } catch (error) {
        errorHandler.handle(error);
    }
}

//Return the router for the server
module.exports = router;
module.exports.getAllImg = getAllImg;