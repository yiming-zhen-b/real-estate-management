@echo off

set DynamoDBpath=C:\ProgramData\DynamoDB
echo Your dynamodb path is :'%DynamoDBpath%'

set "hasDependencies=true"

echo ----- Checking if JAVA is installed
Set "JV="
For /F "Tokens=3" %%A In ('java -version 2^>^&1') Do If Not Defined JV Set "JV=%%~A"
If /I "%JV%"=="" (
	CALL:ECHORED "Please install JAVA from this website:"
	CALL:ECHORED "https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html"
	Echo.
	set "hasDependencies=false"
) Else (
	Echo ----- Java Version "%JV%"
	Echo.
)

echo ----- Checking if node is installed
Set "nodeV=" 
For /F "delims=" %%B In ('node -v 2^>nul') Do If Not Defined nodeV Set "nodeV=%%~B"
If /I "%nodeV%"=="" ( 
	CALL:ECHORED "Please install node from this website:"
	CALL:ECHORED "https://nodejs.org/en/"
	Echo.
	set "hasDependencies=false"
) Else (
	Echo ----- node version "%nodeV%"
	Echo.
)
cd api
call npm install


echo ----- Checking if DynamoDB is installed
IF NOT EXIST %DynamoDBpath%"\DynamoDBLocal.jar" (
	CALL:ECHORED "Please install DynamoDB from this website:"
	CALL:ECHORED "https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.DownloadingAndRunning.html"
	Echo.
	set "hasDependencies=false"
        pause
        exit
) Else (
	echo ----- DynamoDB is installed in %DynamoDBpath%\DynamoDBLocal.jar
	Echo.
)

if "%hasDependencies%" == "true" (
    IF EXIST %DynamoDBpath%"\DynamoDBLocal.jar" (
    	echo ----- Starting DynamoDB in new window
    	START "DynamoDB" /d %DynamoDBpath% CMD /c java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb 
	) ELSE (
	    goto usage
	)

	echo ----- Testing database
	cd setup
	call node serverChecker.js 

	
    echo if no error messages - all good. shut down dynamo and start 
	pause
	exit /b

) else ( 
	pause
)


:ECHORED
%Windir%\System32\WindowsPowerShell\v1.0\Powershell.exe write-host -foregroundcolor Red %1
goto:eof


:ERROR
@echo: Error! See ./check.log for more details

pause