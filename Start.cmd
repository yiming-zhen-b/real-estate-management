echo starting dynamo
set DynamoDBpath=F:\dynamodb
echo Your dynamodb path is :'%DynamoDBpath%'
IF EXIST %DynamoDBpath%"\DynamoDBLocal.jar" (
    echo ----- Starting DynamoDB in new window
    START "DynamoDB" /d %DynamoDBpath% CMD /c java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb 
) ELSE (
   	CALL:ECHORED "Please install DynamoDB from this website:"
	CALL:ECHORED "https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.DownloadingAndRunning.html"
	Echo.
    pause
    exit
)
echo dynamo running

echo starting server
TITLE "API Server" 
node api\server-api
echo running
pause