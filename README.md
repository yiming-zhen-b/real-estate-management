**Real Estate Management Project**

*This is the project for the Real Estate Management.*

---

## Requirements

1. Node.js and Npm, download and install here **https://nodejs.org/en/**.

---

## Test Node.js and npm

Run command in terminal.

1. node -v.
2. npm -v.

---

## Run Node server

1. cd path/to/website.
2. import mysql database ./setup/mysql/database.sql
3. build DynamoDB.
4. run npm install && npm start.