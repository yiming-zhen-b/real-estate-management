var http = require('http');
var fs = require('fs');
var url = require('url');

var extensionsVSHeader = {".css": 'text/css', ".svg": "image/svg+xml"};

http.createServer(function (request, response) {
    var pathName =url.parse(request.url).pathname;
    console.log("Request for "+pathName + " recieved")
    var filename;
    if (pathName=="/") {
        filename="index.html";
    } else {
        filename = pathName.substr(1);
    }
    console.log("fetching <" + filename + ">")
    fs.readFile(filename, function (err, data) {
        if (err) {
            response.writeHead(404, { 'Content-type': 'text/html' });
            response.write('<script>location.href = "/404.html";</script>');
            response.end();
        } else {
            var extension = filename.lastIndexOf('.');
            extension = filename.substr(extension);
            var contenttype = extensionsVSHeader[extension];
    
            console.log("File extension = " + extension);
            if(!contenttype){
                contenttype = 'text/html';
                console.log("File extension again = " + extension);
            }

            response.writeHead(200, { 'Content-type': contenttype});            
            response.write(data);
            response.end();
        }
    });
}).listen(8088);

console.log("Server running on 8088");
