var Main = /** @class */ (function () {
    function Main() {
        this.manageAgentTableId = "#dc_manage_agent_table";
        this.manageAgentTable = dc.dataTable(this.manageAgentTableId);
        this.agentAlert = $("#manage_agent_content").find("#alert_span");
        this.dateFormatSpecifier = '%d/%m/%Y';
        this.isoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S.%LZ");
        this.dateFormat = d3.timeFormat(this.dateFormatSpecifier);
        this.dateFormatParser = d3.timeParse(this.dateFormatSpecifier);
    }
    Main.prototype.agentTabClick = function () {
        var _this = this;
        $("#manage_agent_tab a").click(function () {
            Query.sendRequest("POST", "/main/getAllAgents", "", function (data) {
                _this.agents = data;
                if (_this.agents.status) {
                    if (!_this.agents.data.length) {
                        _this.agentAlert.removeClass("d-none");
                        $(_this.manageAgentTableId).addClass("d-none");
                    }
                    else {
                        _this.agentAlert.addClass("d-none");
                        $(_this.manageAgentTableId).removeClass("d-none");
                    }
                    var data = _this.agents.data;
                    data.forEach(function (d) {
                        d.email = d.email_address;
                        d.name = d.first_name + " " + d.last_name;
                        d["licence number"] = d.licence_number;
                        d["is agent"] = Utilities.capitalizeTxt(d.is_agent);
                        d.status = '<div class="btn-group app_edit_dropdown" role="group">' +
                            '<button data-email="' + d.email_address + '" data-id="' + d.entity_id + '" data-current-value="' + d.on_hold + '" id="btnGroupDrop1" type="button" class="btn btn-secondary btn-cus dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (d.on_hold === "true" ? "On hold" : "Accept") + '</button>' +
                            '<div class="dropdown-menu dropdown_admin dropdown-menu-cus" aria-labelledby="btnGroupDrop1">' +
                            '<a class="dropdown-item" data-new-value="false" onclick="return main.changAgentOnHold(this);" href="#">Accpet</a>' +
                            '<div class="dropdown-divider"></div>' +
                            '<a class="dropdown-item" data-new-value="true" onclick="return main.changAgentOnHold(this);" href="#">On hold</a>' +
                            '</div>' +
                            '</div>';
                        d.created = _this.dateFormat(_this.isoParse(d.created));
                        d.remove = "<button data-id='" + d.entity_id + "' onclick='main.removeAgent(this);' type='button' class='remove_btn btn btn-primary btn-cus'>Remove</button>";
                    });
                    var ndx = crossfilter(data), all = ndx.groupAll(), tableDimension = ndx.dimension(function (d) { return d["for"]; });
                    _this.manageAgentTable
                        .width($(_this.manageAgentTableId).width())
                        .dimension(tableDimension)
                        .group(function (d) {
                        return "";
                    })
                        .showGroups(false)
                        .columns(["name", "email", "licence number", "created", "is agent", "status", "remove"])
                        .size(all.value())
                        .order(d3.descending)
                        .sortBy(function (d) { return d.listFormatedDate; });
                    dc.renderAll();
                }
                else {
                    if (data.redirect)
                        Query.redirect(data.redirect);
                }
            });
        });
    };
    Main.prototype.changAgentOnHold = function (ele) {
        var _this = this, button = $(ele).parent().parent().find("button"), currentValue = button.data("current-value"), newValue = $(ele).data("new-value"), id = button.data("id"), name = button.parent().parent().parent().find("td").first().text(), email = button.data("email");
        if (currentValue !== newValue) {
            new Dialog().show("Are you sure you want to " + "<span class='text-primary'>" + $(ele).text() + " " + name + "</span> ?")
                .addCloseBtn().setBtnClick(function () {
                Query.sendRequest("POST", "/main/updateAgentOnHold", JSON.stringify({ id: id, onHold: newValue, email: email }), function (data) {
                    if (!data.status) {
                    }
                    else {
                    }
                    _this.clickAgentTab();
                });
            });
        }
    };
    Main.prototype.removeAgent = function (ele) {
        var _this = this;
        var id = $(ele).data("id");
        var name = $(ele).parent().parent().find("td").first().text();
        new Dialog().show("Are you sure you want to delete " + "<span class='text-primary'>" + name + "</span> ?").addCloseBtn().setBtnClick(function () {
            var dialog = new Dialog({ disabledBtn: true, processBar: true }).show('Deleting ' + name);
            Query.sendRequest("POST", "/main/removeAgent", id, function (data) {
                if (data.status) {
                    dialog.changeText(name + " deleted.");
                }
                else {
                    dialog.changeText("Error.");
                }
                dialog.disabledBtn(false).setBtnClick(function () {
                    _this.clickAgentTab();
                }).removeAnimation();
            });
        });
    };
    Main.prototype.clickAgentTab = function () {
        $('#manage_agent_tab a').click();
    };
    return Main;
}());
nav.checkLoginStatus(function (data) {
    if (!data.status) {
        new Dialog().show(data.message).setBtnClick(function () { Query.redirect("/login.html"); });
    }
});
var main = new Main();
main.agentTabClick();
main.clickAgentTab();
