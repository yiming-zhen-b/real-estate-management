// SETUP
// =============================================================================
//required models & packages
const fs = require('fs');
const express = require('express');
const http = require('http');
const https = require('https');
const cors = require("cors");
const path = require('path');
const app = express();
const timeout = require('connect-timeout'); //express v4

const port = process.env.PORT || 8088;        // set our port


app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "allowedHeaders": ["X-Requested-With", "content-type", "authorization"],
    "preflightContinue": false,
    "optionsSuccessStatus": 200,
    // "credentials": true,
}));
// Set and handel timeout
app.use(timeout('10s'));
app.use(function haltOnTimedout(req, res, next) {
    if (!req.timedout) next();
    else res.json("Time out");
});

app.use(express.static(__dirname));

// show 404 page
app.get('*', function (req, res) {
    if (req.accepts('html')) {
        res.status(404).send('<script>location.href = "/error.html";</script>');
        return;
    }
});


// START THE SERVER
// =============================================================================
app.listen(port);

// const httpsServer = https.createServer({
//     key: fs.readFileSync('./ssl/selfsigned.key', 'utf8'),
//     cert: fs.readFileSync('./ssl/selfsigned.crt', 'utf8')
// }, app);
// httpsServer.listen(port);


console.log('Static server on port ' + port);