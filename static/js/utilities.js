var Utilities;
(function (Utilities) {
    function showConfirm(message, redirect, afterRedirect) {
        if (confirm(message)) {
            Query.redirectBackAfterRedirectTo(redirect);
            // Query.redirectWithRedirect(redirect, afterRedirect);
        }
    }
    Utilities.showConfirm = showConfirm;
    function getCurrentWindowUrl(message, redirect, afterRedirect) {
        var cur_url = window.location.href;
        if (cur_url.indexOf("#") === cur_url.length - 1) {
            cur_url = cur_url.substring(0, cur_url.length - 1);
        }
        return cur_url;
    }
    Utilities.getCurrentWindowUrl = getCurrentWindowUrl;
    function getCurrentPathname() {
        return window.location.pathname;
    }
    Utilities.getCurrentPathname = getCurrentPathname;
    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }
    Utilities.isInArray = isInArray;
    function isNumber(element, value) {
        setInputFilter($("#myTextBox"), function (value) {
            return /^\d*$/.test(value);
        });
    }
    Utilities.isNumber = isNumber;
    function convertToInt(value) {
        if (typeof value === "string") {
            return parseInt(value);
        }
        if (typeof value === "object") {
            return value.map(Number);
            // var newValue = [];
            // value.forEach(element => {
            //     newValue.push(parseInt(element));
            // });
            // return newValue;
        }
    }
    Utilities.convertToInt = convertToInt;
    function capitalizeTxt(txt) {
        if (typeof txt !== "string" || txt.length == 0)
            return txt;
        return txt.charAt(0).toUpperCase() + txt.slice(1); //or if you want lowercase the rest txt.slice(1).toLowerCase();
    }
    Utilities.capitalizeTxt = capitalizeTxt;
    function sortJSONKey(json) {
        var keys = [];
        for (var k in json) {
            if (json.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        keys.sort();
        return keys;
    }
    Utilities.sortJSONKey = sortJSONKey;
    function getJSONKey(json) {
        var keys = [];
        for (var k in json) {
            if (json.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        return keys;
    }
    Utilities.getJSONKey = getJSONKey;
    function getJSONValues(json) {
        var keys = [];
        for (var k in json) {
            if (json.hasOwnProperty(k)) {
                keys.push(json[k]);
            }
        }
        return keys;
    }
    Utilities.getJSONValues = getJSONValues;
    function copyToClipboard(value) {
        var el = document.createElement('textarea');
        el.value = value;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        new DialogSmall({ size: "lg" }).show("<span class='text-primary'>" + el.value + "</span> is copied the clipboard");
    }
    Utilities.copyToClipboard = copyToClipboard;
})(Utilities || (Utilities = {}));
function arrayToString(array, join) {
    return array.join(join);
}
function setInputFilter(obj, inputFilter) {
    obj.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        }
        else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
    });
}
