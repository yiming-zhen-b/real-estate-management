var Footer = /** @class */ (function () {
    function Footer() {
    }
    Footer.prototype.createFooter = function () {
        var li = [
            { text: "About", href: "#" },
            { text: "Contact", href: "#" },
            { text: "Terms of Use", href: "#" },
            { text: "Privacy Policy", href: "#" },
        ];
        var ul = $("<ul>", { "class": "list-inline mb-2" });
        for (var i = 0; i < li.length; i++) {
            ul.append($("<li>", { "class": "list-inline-item" }).append($("<a>", { href: li[i].href, text: li[i].text })));
            if (i != li.length - 1) {
                ul.append($("<li>", { "class": "list-inline-item", html: "&sdot;" }));
            }
        }
        $("body").append($("<footer>", { "class": "bg-dark" })
            .append($("<div>", { "class": "container" })
            .append($("<div>", { "class": "row" })
            .append($("<div>", { "class": "col-12 h-100 text-center text-lg-left my-auto" })
            .append(ul)
            .append($("<p>", { "class": "small mb-4 mb-lg-0", html: "&copy; <a href='" + window.location.origin + "'>" + window.location.hostname + "</a> 2018. All Rights Reserved." }))))));
    };
    return Footer;
}());
var footer = new Footer();
footer.createFooter();
