var Query;
(function (Query) {
    function sendRequest(method, api, praValue, callback, async) {
        var para = "";
        if (SessionStorage)
            para = SessionStorage.getItem("u");
        $.ajax({
            type: method,
            async: async === false ? false : true,
            url: Config.api_base_url + api,
            data: { data: praValue },
            // xhrFields: {
            //     withCredentials: true
            // },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', para ? para : "");
                // xhr.setRequestHeader('Authorization', 'Basic ' + btoa(unescape(encodeURIComponent(YOUR_USERNAME + ':' + YOUR_PASSWORD))))
            },
            success: callback,
            error: function (e) {
                console.log(e.statusText);
            }
        });
    }
    Query.sendRequest = sendRequest;
    function sendImage(method, api, praValue, callback) {
        $.ajax({
            type: method,
            url: window.location.origin + api,
            data: praValue,
            contentType: false,
            processData: false,
            success: callback,
            error: function (jqXHR, textStatus, errorMessage) {
                console.log('Error uploading: ' + errorMessage);
                console.log(jqXHR.statusText);
            }
        });
    }
    Query.sendImage = sendImage;
    function redirect(redirect) {
        var _a;
        var pre_url = Utilities.getCurrentWindowUrl();
        if (pre_url.indexOf("login.html") == -1) {
            SessionStorage.setItem((_a = {}, _a[Config.sessionKey.pre_url] = Utilities.getCurrentWindowUrl(), _a));
        }
        window.location.href = redirect;
    }
    Query.redirect = redirect;
    function loginRedirect(u) {
        var _a;
        if (u)
            SessionStorage.setItem({ u: u });
        if (SessionStorage.getItem(Config.sessionKey.donotGoBack) || !SessionStorage.getItem(Config.sessionKey.pre_url)) {
            SessionStorage.setItem((_a = {}, _a[Config.sessionKey.pre_url] = "/", _a));
            SessionStorage.removeItem(Config.sessionKey.donotGoBack);
        }
        window.location.href = SessionStorage.getItem(Config.sessionKey.pre_url);
    }
    Query.loginRedirect = loginRedirect;
    function redirectNewTab(redirect, data) {
        window.open(redirect + createUrlParams(data), "").focus();
    }
    Query.redirectNewTab = redirectNewTab;
    function getUrlParams(url) {
        if (!url)
            url = window.location.href;
        var name = "d";
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
        if (!results)
            return {};
        if (!results[2])
            return {};
        return JSON.parse(decodeURIComponent(results[2].replace(/\+/g, " ")));
    }
    Query.getUrlParams = getUrlParams;
    function createUrlParams(data) {
        return "?d=" + encodeURIComponent(JSON.stringify(data));
    }
})(Query || (Query = {}));
