var MyDropZone = /** @class */ (function () {
    function MyDropZone(id, options) {
        this.imageCount = 0;
        this.options = {
            autoProcessQueue: false,
            addFileCallback: null,
            removeFileCallback: null,
            uploadedFileRemoveCallback: null,
            onUploadedFileRemove: null
        };
        var _this = this;
        this.containerId = id;
        if (options) {
            for (var k in options) {
                if (options[k] != null)
                    this.options[k] = options[k];
            }
        }
        Dropzone.autoDiscover = false;
        Query.sendRequest("GET", "/getPropertyAttr/getProImageTypes", "", function (data) {
            _this.propertyPhotoTypes = data;
        }, false);
        _this.dropzone = new Dropzone(id, {
            url: Config.api_base_url + "/listingProperty/uploadImages",
            method: "POST",
            paramName: "images",
            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
            parallelUploads: 10,
            autoProcessQueue: _this.options.autoProcessQueue,
            addRemoveLinks: true,
            maxFiles: Config.dropzoneMaxFiles,
            maxThumbnailFilesize: 50,
            headers: {
                'Authorization': SessionStorage.getItem("u") ? SessionStorage.getItem("u") : "",
                'Cache-Control': null,
                'X-Requested-With': null
            }
            // maxFilesize: 2, // MB
            // accept: function (file, done) {
            //     if (file.name == "justinbieber.jpg") {
            //         done("Naha, you don't.");
            //     }
            //     else { done(); }
            // }
        });
        _this.dropzone.on("addedfile", function (file) {
            _this.imageCount++;
            var caption = file.caption == undefined ? "" : file.caption;
            var img_type = file.img_type == undefined ? "" : file.img_type;
            var radioId = new Date().getTime();
            var options = "";
            for (var o = 0; o < _this.propertyPhotoTypes.length; o++) {
                var value = _this.propertyPhotoTypes[o].name;
                options += "<option value='" + o + "' " + (o == (img_type ? img_type : 0) ? "selected" : "") + ">" + value + "</option>";
            }
            file._captionLabel = Dropzone.createElement("<div class='span12 text-center'><span><small>Caption:</small></span></div>");
            file._captionBox = Dropzone.createElement("<input class='form-control form-control-sm' type='text' name='caption' value=" + caption + " >");
            file._typeLabel = Dropzone.createElement("<div class='span12 text-center'><span><small>File type:</small></span></div>");
            file._typeOptions = Dropzone.createElement("<select class='selectpicker form-control form-control-sm'>" + options + "</select>");
            file._coverImgCheck = Dropzone.createElement("<div class='span12 col-12 no-gutters'><input name='cover_image_radio' id='" + radioId + "' class='col-2 form-check-input' type='radio'/><label class='small' for='" + radioId + "'>Cover image</label></div>");
            file.previewElement.appendChild(file._captionLabel);
            file.previewElement.appendChild(file._captionBox);
            file.previewElement.appendChild(file._typeLabel);
            file.previewElement.appendChild(file._typeOptions);
            file.previewElement.appendChild(file._coverImgCheck);
            $(file._typeOptions).selectpicker("refresh");
            $(file._coverImgCheck).change(function () {
                $(_this.containerId).find(".dropdown.bootstrap-select button").attr("disabled", false);
                $(file._typeOptions).val("0");
                $(file._typeOptions).selectpicker("refresh");
                $(file._typeOptions).parent().find("button").attr("disabled", true);
            });
            // Check radio for image cover for default
            if ($(_this.containerId + " .dz-preview").length == 1) {
                $(file._coverImgCheck).find("input[name='cover_image_radio']").click();
            }
            // Add more data if mocking the uploaded file
            if (file.uploaded) {
                $(file.previewElement).attr("data-img-id", file.id);
                if (file.is_cover === "true")
                    $(file._coverImgCheck).find("input[name='cover_image_radio']").click();
                if (_this.options.onUploadedFileRemove) {
                    $(file._removeLink).on("click", function () {
                        _this.options.onUploadedFileRemove(file.id);
                    });
                }
            }
            // excute the callback after a file added
            if (_this.options.addFileCallback) {
                _this.options.addFileCallback();
            }
        });
        _this.dropzone.on("removedfile", function (file) {
            var coverImageRadio = $(_this.containerId + " .dz-preview").find("input[name='cover_image_radio']");
            if (!coverImageRadio.is(":checked"))
                coverImageRadio.first().click();
            // excute the callback after a file removed
            if (_this.options.removeFileCallback) {
                _this.options.removeFileCallback(file);
            }
            if (file.uploaded && _this.options.uploadedFileRemoveCallback) {
                _this.options.uploadedFileRemoveCallback(file);
            }
        });
        return this;
    }
    MyDropZone.prototype.uploadImage = function (data, options) {
        var _this = this;
        if (_this.validFileLength() > 0) {
            _this.dropzone.on('sending', function (file, xhr, formData) {
                var caption = $(file.previewElement.querySelector("input[name='caption']")).val();
                var img_type = $(file.previewElement.querySelector("select.selectpicker.form-control")).val();
                var is_cover = $(file.previewElement.querySelector("input[name='cover_image_radio']")).is(':checked');
                formData.append("img_type", img_type);
                formData.append("caption", caption);
                formData.append('is_cover', is_cover);
                formData.append('c', data);
            });
            _this.dropzone.options.autoProcessQueue = true;
            _this.dropzone.processQueue();
            _this.dropzone.on("queuecomplete", function (file) {
                if (options.success)
                    options.success.call(_this);
            });
        }
        else {
            if (options.success)
                options.success.call(_this);
        }
    };
    MyDropZone.prototype.validFileLength = function () {
        var count = 0;
        var _this = this;
        _this.dropzone.files.forEach(function (element) {
            if (element.accepted && element.status === "queued")
                count++;
        });
        return count;
    };
    ;
    MyDropZone.prototype.mockFile = function (files) {
        var _this = this;
        files.forEach(function (aImg) {
            // Create the mock file:
            var mockFile = {
                dataURL: aImg.t_img, name: aImg.originalname, caption: aImg.caption, img_type: aImg.img_type, id: aImg.id,
                is_cover: aImg.is_cover, status: "success", accepted: true, uploaded: true, size: 12345, kind: 'image'
            };
            _this.dropzone.emit("addedfile", mockFile);
            _this.dropzone.files.push(mockFile);
            _this.dropzone.createThumbnailFromUrl(mockFile, 120, 120, "crop", true, function (thumbnail) {
                _this.dropzone.emit('thumbnail', mockFile, thumbnail);
            }, "anonymous");
            // Make sure that there is no progress bar, etc...
            _this.dropzone.emit("complete", mockFile);
        });
    };
    return MyDropZone;
}());
