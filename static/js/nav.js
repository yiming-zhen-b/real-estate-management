var Nav = /** @class */ (function () {
    function Nav() {
        this.redirectPathname = ["/listingProperty.html", "/dashboard.html"];
    }
    Nav.prototype.createNav = function () {
        var _this = this;
        var vanContainer = $("nav.navbar.navbar-expand-lg");
        vanContainer.empty();
        var navValues = {
            "Home": { l: "/", id: "nav_home" },
            "Search": { l: "/search.html", id: "" },
            "Listing": { l: "/listingProperty.html", id: "nav_listing", "class": "d-none" },
            "Services": { l: "/#", id: "" },
            "Dashboard": { l: "/dashboard.html", id: "nav_user_profile", "class": "d-none" },
            "": { l: "#", id: "login_status", "class": "nav-pills" }
        };
        var navValuesUL = $("<ul>", { id: "nav_ul", "class": "navbar-nav ml-auto" });
        for (var key in navValues) {
            var li = $("<li>", {
                "class": (navValues[key].l == Utilities.getCurrentPathname() ? "nav-item active " : "nav-item ") + (navValues[key]["class"] ? navValues[key]["class"] : ""),
                id: navValues[key].id ? navValues[key].id : ""
            }).append($("<a>", { "class": "nav-link", href: navValues[key].l, text: key }));
            navValuesUL.append(li);
        }
        vanContainer
            .append($("<div>", { "class": "container" })
            .append($("<a>", { id: "nav_logo", "class": "navbar-brand", href: "/", text: "Homely" }))
            .append($("<button>", {
            "class": "navbar-toggler", type: "button", "data-toggle": "collapse", "data-target": "#navbarResponsive",
            "aria-controls": "navbarResponsive", "aria-expanded": false, "aria-label": "Toggle navigation"
        }).append($("<span>", { "class": "navbar-toggler-icon" })))
            .append($("<div>", { "class": "collapse navbar-collapse col-11", id: "navbarResponsive" })
            .append($("<div>", { "class": "row col-lg-4 col-md-12 font-weight-bold", id: "nav_wel_message" }))
            .append(navValuesUL)));
        _this.checkLoginStatus();
    };
    Nav.prototype.checkLoginStatus = function (callback) {
        var _this = this;
        Query.sendRequest("POST", "/loginRegister/checkLoginStatus", "", function (data) {
            if (callback)
                callback(data);
            var userProfile = $("#nav_user_profile");
            var listingLink = $("#nav_listing");
            var loginStatusDiv = $("#login_status"), navWelMessage = $("#nav_wel_message");
            navWelMessage.empty();
            loginStatusDiv.empty();
            if (data.status) {
                console.log(data);
                if (window.location.pathname === "/login.html")
                    Query.redirect("/");
                listingLink.removeClass("d-none");
                userProfile.removeClass("d-none");
                navWelMessage.append($("<span>", { text: "Welcome " + data.user_name, "class": "nav-link" }));
                loginStatusDiv.append($("<a>", { href: "#", "data-auth-id": data.auth_id, text: "Logout", id: "logout_link", "class": "nav-link" }));
                loginStatusDiv.css({ width: "70px" });
                handleLogoutClick(_this);
            }
            else {
                listingLink.addClass("d-none");
                userProfile.addClass("d-none");
                loginStatusDiv.append($("<a>", { href: "#", text: "Login / Register", id: "login_link", "class": "nav-link" }));
                loginStatusDiv.css({ width: "135px" });
                handleLoginClick();
            }
            function handleLoginClick() {
                $("#login_link").on("click", function () {
                    Query.redirect("/login.html");
                });
            }
            function handleLogoutClick(_this) {
                $("#logout_link").on("click", function () {
                    var authId = $(this).data("auth-id");
                    logout();
                });
                function logout() {
                    Query.sendRequest("POST", "/loginRegister/logout", "", function (data) {
                        if (data.status) {
                            _this.checkLoginStatus();
                            console.log('User signed out.');
                            SessionStorage.removeItem("u");
                            if ($.inArray(Utilities.getCurrentPathname(), _this.redirectPathname) > -1 || Utilities.getCurrentPathname().indexOf("admin") > -1)
                                Query.redirect("/");
                        }
                    });
                }
            }
        });
    };
    Nav.prototype.detectTabClose = function () {
        window.onbeforeunload = function (event) {
            var message = 'Important: Please click on \'Save\' button to leave this page.';
            if (typeof event == 'undefined') {
                event = window.event;
            }
            if (event) {
                event.returnValue = message;
            }
            return message;
        };
    };
    return Nav;
}());
var nav = new Nav();
nav.createNav();
// nav.detectTabClose();
