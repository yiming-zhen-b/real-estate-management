var DialogSmall = /** @class */ (function () {
    function DialogSmall(options) {
        this.textClass = "modal-text";
        this.options = {
            size: "sm"
        };
        var _this = this;
        if (options) {
            for (var k in options) {
                if (options[k] != null)
                    this.options[k] = options[k];
            }
        }
        this.dialogHtml = $('<div class="modal fade" tabindex="-1" role="dialog" style="overflow-y:visible;padding-top:50px;">' +
            '<div class="modal-dialog modal-' + this.options.size + '">' +
            '<div class="modal-content" style="">' +
            '<div class="modal-body">' +
            '<div class="' + this.textClass + ' word-wrap font-weight-bold text-center"></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');
        this.dialogHtml.on('shown.bs.modal', function (e) {
            //    $("body").removeClass("modal-open");
        });
        this.dialogHtml.on('hidden.bs.modal', function (e) {
            e.currentTarget.remove();
            if ($(".modal-backdrop").length > 0 && $(".modal").length > 0) {
                $("body").addClass("modal-open");
                $("body").attr("style", "padding-right: 15px;");
            }
        });
    }
    DialogSmall.prototype.show = function (text) {
        var _this = this;
        this.changeText(text);
        setTimeout(function () {
            _this.dialogHtml.modal('hide');
        }, 500);
        this.dialogHtml.modal({
            backdrop: false
        });
        // this.dialogHtml.modal("show");
        return this;
    };
    DialogSmall.prototype.changeText = function (message) {
        this.dialogHtml.find('.' + this.textClass).html(message);
        return this;
    };
    return DialogSmall;
}());
