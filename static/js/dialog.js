var Dialog = /** @class */ (function () {
    function Dialog(options) {
        this.headerClass = "header";
        this.textClass = "modal-text";
        this.btnClass = "modal-btn";
        this.closeBtnClass = "close_btn";
        this.processbarConClass = "modal-processbar";
        this.processbarClass = "progress-bar";
        this.footerClass = "modal-footer";
        this.extraHeaderClass = "extra_modal_header";
        this.options = {
            onShow: null,
            onShown: null,
            disabledBtn: false,
            processBar: false,
            dialogSize: 'm',
            progressType: '',
            onHide: null,
            btnClick: null
        };
        var _this = this;
        if (options) {
            for (var k in options) {
                if (options[k] != null)
                    this.options[k] = options[k];
            }
        }
        this.dialogHtml = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y:visible;">' +
            '<div class="modal-dialog modal-m modal-dialog-centered">' +
            '<div class="modal-content" style="padding-bottom:15px">' +
            '<div class="modal-header">' +
            '<h3 class= "' + this.headerClass + '" style="margin:0;">Homely</h3>' +
            '</div>' +
            '<div class="' + this.extraHeaderClass + ' modal-header d-none"></div>' +
            '<div class="modal-body">' +
            '<span class="' + this.textClass + ' font-weight-bold"></span>' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="' + this.btnClass + ' col-4 btn btn-primary align-self-center">Yes</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');
        if (this.options.disabledBtn == true) {
            this.disabledBtn(true);
        }
        if (this.options.processBar == true) {
            this.addAnimationBar();
        }
        if (this.options.onShow) {
            this.dialogHtml.on('show.bs.modal', function (e) {
                _this.options.onShow(e);
            });
        }
        if (this.options.onShown) {
            this.dialogHtml.on('shown.bs.modal', function (e) {
                _this.options.onShown(e);
            });
        }
    }
    Dialog.prototype.show = function (message) {
        if (typeof message === 'undefined') {
            message = 'Loading';
        }
        // Configuring dialog
        this.dialogHtml.find('.modal-dialog').addClass('modal-' + this.options.dialogSize);
        if (this.options.progressType) {
            this.dialogHtml.find('.' + this.processbarClass).addClass(this.options.progressType);
        }
        this.changeText(message);
        // Adding callbacks
        // if (typeof this.options.onHide === 'function') {
        //     this.dialogHtml.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
        //         this.options.onHide.call(this);
        //     });
        // }
        this.setBtnClick(this.options.btnClick);
        // Opening dialog
        this.dialogHtml.modal();
        // this.handleStayBtnClick();
        return this;
    };
    Dialog.prototype.setOnShow = function (callback) {
        var _this = this;
        if (callback) {
            this.dialogHtml.on('show.bs.modal', function (e) {
                callback(e);
            });
        }
        return this;
    };
    Dialog.prototype.setOnShown = function (callback) {
        var _this = this;
        if (callback) {
            this.dialogHtml.on('shown.bs.modal', function (e) {
                callback(e);
            });
        }
        return this;
    };
    Dialog.prototype.setBody = function (body) {
        this.dialogHtml.find('.modal-body').html(body);
        return this;
    };
    Dialog.prototype.setHeader = function (header) {
        this.dialogHtml.find('.' + this.headerClass).html(header);
        return this;
    };
    Dialog.prototype.setExtraHeader = function (header) {
        this.dialogHtml.find('.' + this.extraHeaderClass).removeClass("d-none");
        this.dialogHtml.find('.' + this.extraHeaderClass).html(header);
        return this;
    };
    Dialog.prototype.changeText = function (message) {
        this.dialogHtml.find('.' + this.textClass).html(message);
        return this;
    };
    Dialog.prototype.removeAnimation = function () {
        this.dialogHtml.find('.' + this.processbarClass).removeClass("progress-bar-animated");
        return this;
    };
    Dialog.prototype.startAnimation = function () {
        if (this.dialogHtml.find('.' + this.processbarClass).length == 0) {
            this.addAnimationBar();
        }
        this.dialogHtml.find('.' + this.processbarClass).addClass("progress-bar-animated");
        return this;
    };
    Dialog.prototype.addAnimationBar = function () {
        this.dialogHtml.find('.modal-body').append('<div class="' + this.processbarConClass + ' progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar progress-bar-striped progress-bar-animated" style="width: 100%"></div>' +
            '</div>');
    };
    Dialog.prototype.addCss = function (key, value) {
        this.dialogHtml.attr(key, this.dialogHtml.attr(key) + value);
        return this;
    };
    Dialog.prototype.addCloseBtn = function (callback, btnText) {
        var _this = this;
        var footer = this.dialogHtml.find('.' + this.footerClass);
        var footerHtml = footer.html();
        footer.empty();
        footer.append($("<button>", { text: "Close", type: "button", "class": "btn btn-secondary " + this.closeBtnClass }))
            .append(footerHtml);
        if (btnText)
            this.setCloseBtnText(btnText);
        this.dialogHtml.find('.' + this.closeBtnClass).on("click", function () {
            _this.hide();
            _this.remove();
            if (callback)
                callback.call(_this);
        });
        return this;
    };
    Dialog.prototype.setCloseBtnText = function (text) {
        this.dialogHtml.find('.' + this.closeBtnClass).text(text);
        return this;
    };
    Dialog.prototype.disabledBtn = function (disabled) {
        this.dialogHtml.find('.' + this.btnClass).attr("disabled", disabled);
        return this;
    };
    Dialog.prototype.setBtnText = function (text) {
        this.dialogHtml.find('.' + this.btnClass).text(text);
        return this;
    };
    Dialog.prototype.setBtnClick = function (callback) {
        var _this = this;
        this.dialogHtml.find('.' + this.btnClass).on("click", function () {
            _this.hide();
            _this.remove();
            if (callback)
                callback.call(_this);
        });
        return _this;
    };
    Dialog.prototype.setBtnClickWithoutClose = function (callback) {
        var _this = this;
        this.dialogHtml.find('.' + this.btnClass).on("click", function () {
            if (callback)
                callback.call(_this);
        });
        return _this;
    };
    // public handleStayBtnClick(){
    //     var _this = this;
    //     this.dialogHtml.find('.modal-stay-btn').on("click", function (){
    //         _this.hide();
    //     })
    // }
    /**
     * Closes dialog
     */
    Dialog.prototype.hide = function () {
        this.dialogHtml.on('hidden.bs.modal', function (e) {
            e.currentTarget.remove();
            if ($(".modal-backdrop").length > 0 && $(".modal").length > 0) {
                $("body").addClass("modal-open");
                $("body").attr("style", "padding-right: 15px;");
            }
        });
        this.dialogHtml.modal('hide');
    };
    Dialog.prototype.remove = function () {
    };
    return Dialog;
}());
