var UserLogin = /** @class */ (function () {
    function UserLogin() {
        this.FBData = { source: 2 };
        this.googleData = { source: 1 };
        this.verificationCodeModal = "#verification_code_modal";
        this.verificationCodeSetBtn = "#verification_code_set_btn";
        var _this = this;
        // register the application Modal phone checkbox
        $("input[name='register_as']:radio, input[name='third_register_as']:radio").change(function () {
            var element = $(this);
            var checkFieldId = element.data("check-field");
            if (element.is(':checked') && element.val() === "is_agent") {
                element.parent().parent().parent().find(".licence_number_div").removeClass("d-none");
            }
            else {
                element.parent().parent().parent().find(".licence_number_div").addClass("d-none");
            }
        });
        $("#login_register_tabs a").click(function () {
            _this.currentTab = $(this).data("target");
            $(this).find(".user_input").val("");
        });
        $('#register_btn').on('click', function () {
            var data = _this.cellectData();
            data["source"] = 0;
            data["register_as"] = $("input[name='register_as']:checked").val();
            Query.sendRequest("POST", "/loginRegister/register", JSON.stringify(data), function (data) {
                _this.handelFallback(data, _this);
                console.log(data);
            });
        });
        var notiSpan;
        $(".verification_btn").on("click", function () {
            Query.sendRequest("POST", "/loginRegister/getVeriCode", JSON.stringify({ user_email: $(_this.currentTab + " .user_email").val(), mode: (_this.currentTab === "#register_content" ? "register" : "") }), function (data) {
                if (data.status) {
                    var secondsLeft = data.seconds - 5;
                    var notiSpan = $("<span>", { "class": "verify_noti_text rounded border bg-white" });
                    $(_this.currentTab + " .verification_btn").parent().append(notiSpan);
                    _this.interval = setInterval(function () {
                        notiSpan.text("The verification code has been send to your email address, you can send again after " + (--secondsLeft) + " seconds.");
                        $(_this.currentTab + " .verification_btn").attr("disabled", true);
                        if (secondsLeft <= 0) {
                            _this.unsetVeriBtn();
                        }
                    }, 1000);
                }
                else {
                    _this.showError(data, _this);
                }
                console.log(data);
            });
        });
        $('#forgot_pass_btn').on('click', function () {
            Query.sendRequest("POST", "/loginRegister/setNewPass", JSON.stringify(_this.cellectData()), function (data) {
                if (data.status) {
                    _this.unsetVeriBtn();
                    $(_this.currentTab + ' .user_input').val("");
                    _this.loginTabClick();
                    new Dialog().show("Your password has been changed.").setBtnClick(null);
                }
                else {
                    _this.showError(data, _this);
                }
                console.log(data);
            });
        });
    }
    UserLogin.prototype.unsetVeriBtn = function () {
        var _this = this;
        $(_this.currentTab + " .verification_btn").attr("disabled", false);
        if (_this.interval)
            clearInterval(_this.interval);
        $(_this.currentTab + " .verification_btn").parent().find("span.verify_noti_text").remove();
    };
    UserLogin.prototype.handelLoginBtnClick = function () {
        var _this = this;
        $('#login_btn').on('click', function () {
            Query.sendRequest("POST", "/loginRegister/login", JSON.stringify(_this.cellectData()), function (data) {
                _this.handelFallback(data, _this);
                console.log(data);
            });
        });
    };
    UserLogin.prototype.cellectData = function () {
        var input = $(this.currentTab + ' .user_input');
        var data = {};
        for (var i = 0; i < input.length; i++) {
            data[$(input[i]).data("type")] = $(input[i]).val();
        }
        console.log(data);
        return data;
    };
    UserLogin.prototype.handelFallback = function (data, _this) {
        if (data.status) {
            // console.log(data);
            _this.loginTabClick();
            Query.loginRedirect(data.u);
        }
        else {
            $("#logout_link").click();
            _this.showError(data, _this);
        }
    };
    UserLogin.prototype.showError = function (data, _this) {
        var error_field = data.error_field;
        for (var key in error_field) {
            if (error_field.hasOwnProperty(key)) {
                var parent = $(_this.currentTab + " " + error_field[key].id).parent();
                parent.attr("data-validate", error_field[key].message);
                parent.addClass('alert-validate');
            }
        }
    };
    UserLogin.prototype.removeError = function (input) {
        $(input).focus(function () {
            $($(this).parent()).removeClass('alert-validate');
        });
    };
    UserLogin.prototype.handelShowPassClick = function () {
        $('.btn-show-pass').on('click', function () {
            var element = $(this);
            var showPass = element.data("show-pass");
            if (showPass == 0) {
                element.next('input').attr('type', 'text');
                element.find('i').removeClass('zmdi-eye');
                element.find('i').addClass('zmdi-eye-off');
                element.data("show-pass", 1);
            }
            else {
                element.next('input').attr('type', 'password');
                element.find('i').addClass('zmdi-eye');
                element.find('i').removeClass('zmdi-eye-off');
                element.data("show-pass", 0);
            }
        });
    };
    UserLogin.prototype.handelInputPlaceHolder = function (input) {
        $(input).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        });
    };
    UserLogin.prototype.onGoogleSignIn = function (googleUser, _this) {
        // The ID token you need to pass to your backend:
        _this.googleData["user_id"] = googleUser.getAuthResponse().id_token;
        gapi.load('auth2', function () {
            gapi.auth2.init({ client_id: '89323615729-crjucj591qknvtb6uic7kkmi0p82o03s.apps.googleusercontent.com' })
                .then(function (authInstance) {
                authInstance.signOut().then(function () {
                    _this.getVerifyCode(_this.googleData);
                });
            });
        });
        // _this.getVerifyCode(userLogin.googleData);
    };
    UserLogin.prototype.getVerifyCode = function (thirdPartyData) {
        var _this = this;
        _this.currentTab = _this.verificationCodeModal;
        Query.sendRequest("POST", "/loginRegister/thirdParty", JSON.stringify(thirdPartyData), function (result) {
            if (!result.status && result.newUser) {
                if (result.showModal) {
                    $(_this.verificationCodeModal).modal("show");
                }
                else {
                    _this.showVerificationModal(result, thirdPartyData.source);
                    $(_this.verificationCodeSetBtn).click(function () {
                        var collectedData = _this.cellectData();
                        for (var sKey in thirdPartyData) {
                            collectedData[sKey] = thirdPartyData[sKey];
                        }
                        collectedData["register_as"] = $("input[name='third_register_as']:checked").val();
                        console.log(collectedData);
                        Query.sendRequest("POST", "/loginRegister/thirdParty", JSON.stringify(collectedData), function (data) {
                            _this.handelFallback(data, _this);
                            console.log(data);
                        });
                    });
                    console.log(result);
                }
            }
            if (result.status && !result.newUser) {
                _this.handelFallback(result, _this);
            }
        });
    };
    UserLogin.prototype.showVerificationModal = function (data, source) {
        var _this = this;
        var secondsLeft = data.seconds - 5;
        $(_this.verificationCodeModal).find("#modal_title_message").html("<span>The verification code is send to <span class='text-primary'>" + data.email +
            "</span>, you can resend it after <span class='text-primary message_second'></span> seconds</span>");
        if (_this.interval)
            clearInterval(_this.interval);
        _this.interval = setInterval(function () {
            $(_this.verificationCodeModal).find("#modal_title_message .message_second").html(--secondsLeft);
            if (secondsLeft <= 0) {
                if (_this.interval)
                    clearInterval(_this.interval);
                $(_this.verificationCodeModal).find("#modal_title_message").html("<span>You can <a href='javascript:userLogin.resendVerifyCode(" + source + ")'>resent</a> the verification code</span>");
            }
        }, 1000);
        $(_this.verificationCodeModal).modal("show");
    };
    UserLogin.prototype.resendVerifyCode = function (source) {
        this.getVerifyCode(this.FBData.source == source ? this.FBData : this.googleData);
    };
    UserLogin.prototype.closeVerificationModal = function () {
        var _this = this;
        _this.loginTabClick();
        _this.googleToken = null;
        $(_this.verificationCodeModal).modal("hide");
    };
    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    UserLogin.prototype.checkLoginState = function () {
        var _this = this;
        FB.getLoginStatus(function (response) {
            _this.FBStatusChangeCallback(response);
        });
    };
    // This is called with the results from from FB.getLoginStatus().
    UserLogin.prototype.FBStatusChangeCallback = function (response) {
        var _this = this;
        console.log('FBStatusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            FB.api('/me', { fields: 'name, email, birthday, hometown, education, gender, website, work' }, function (loginResponse) {
                _this.FBData["user_id"] = loginResponse.email;
                _this.FBData["name"] = loginResponse.name;
                FB.logout(function (logoutResponse) {
                    // user is now logged out
                    console.log("logout");
                    _this.getVerifyCode(_this.FBData);
                });
            });
        }
        else {
            console.log('FB login error');
        }
    };
    UserLogin.prototype.loginTabClick = function () {
        $("#login_tab a").click();
    };
    return UserLogin;
}());
var userLogin = new UserLogin();
userLogin.handelLoginBtnClick();
// Show pass click
userLogin.handelShowPassClick();
userLogin.loginTabClick();
//remove error and handel the placeholder
$('.validate-input .user_input').each(function () {
    userLogin.removeError(this);
    userLogin.handelInputPlaceHolder(this);
});
function renderGoogleSigninBtn() {
    gapi.signin2.render('googleSignIn', {
        'scope': 'profile email',
        'width': 250,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': function (googleUser) {
            userLogin.onGoogleSignIn(googleUser, userLogin);
        }
    });
}
function googleLogout(callback) {
    gapi.load('auth2', function () {
        gapi.auth2.init({ client_id: '89323615729-crjucj591qknvtb6uic7kkmi0p82o03s.apps.googleusercontent.com' })
            .then(function (authInstance) {
            authInstance.signOut().then(function () {
                if (callback)
                    callback;
            });
        });
    });
}
// Facebook login
window.fbAsyncInit = function () {
    FB.init({
        appId: '1083470191825267',
        cookie: true,
        // the session
        xfbml: true,
        version: 'v2.10'
    });
    FB.AppEvents.logPageView();
    userLogin.checkLoginState();
};
// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// $(document).ready(function () {
//     $('.user_email').val("ditto.zhen@gmail.com");
//     $('.user_pass').val("111");
//     $('.user_pass_again').val("111");
//     $('.first_name').val("Yiming");
//     $('.last_name').val("Zhen");
// });
