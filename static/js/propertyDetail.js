var PropertyDetail = /** @class */ (function () {
    function PropertyDetail() {
        this.currentImgTabClass = $("#pro_img_tabs a.active").data("tar");
        this.origCurrImgIndex = 0;
    }
    PropertyDetail.prototype.getDetail = function () {
        var data = Query.getUrlParams();
        var _this = this;
        if (data)
            Query.sendRequest("POST", "/proDetail/getDetails", data, handleResult);
        function handleResult(result) {
            if (result.status == true) {
                var data = result.data[0];
                var propertyDom = $("#property_details");
                createHouseTile(result, data);
                createImgSliderContent(data);
                createEssentialInfor(data);
                createBookingDetail(data);
                console.log(result);
            }
            function createHouseTile(result, data) {
                var houseTitle = $("#house_title");
                var location = [(data.street_number ? data.street_number + " " : "") + (data.unit_flat_number ? data.unit_flat_number + " " : "") +
                        (data.street_name ? data.street_name : ""), data.suburb].join(", ");
                houseTitle.append($("<h4>", { "class": "col-12", text: location }));
                houseTitle.append($("<div>", { "class": "row col-12" })
                    .append($("<span>", { "class": "col-md-4 col-sm-12 font-weight-bold", text: "$" + data.rent_pw + " per week" }))
                    .append($("<div>", { "class": "row col-md-8 col-sm-12 no-gutters" })
                    .append($("<div>", { "class": "row col-3 no-gutters" })
                    .append($("<img>", { src: result.icon.bed, "class": "house_attr_icon col-6 align-self-center" }))
                    .append($("<span>", { text: data.bedrooms, "class": "col-6 align-self-center" })))
                    .append($("<div>", { "class": "row col-3 no-gutters" })
                    .append($("<img>", { src: result.icon.bath, "class": "house_attr_icon col-6 align-self-center" }))
                    .append($("<span>", { text: data.bathrooms, "class": "col-6 align-self-center" })))
                    .append($("<div>", { "class": "row col-6 no-gutters" })
                    .append($("<img>", { src: result.icon.gar, "class": "house_attr_icon col-2 align-self-center" }))
                    .append($("<span>", { text: data.parking, "class": "col-8 align-self-center" })))));
            }
            function createImgSliderContent(data) {
                var originalUL = $(".slides.original_ul"), thumbUL = $(".slides.thumb_ul");
                for (var i = 0; i < data.img.length; i++) {
                    var img = data.img[i];
                    originalUL.append($("<li>").append($("<img>", { src: img.o_img, "data-type": img.img_type, "class": (img.img_type == 1 ? "img_panoramas all-scroll" : "") })).append($("<p>", { text: img.caption ? img.caption : "", "class": "flex-caption" })));
                    thumbUL.append($("<li>").append($("<img>", { src: img.t_img, "data-type": img.img_type, "class": (img.img_type == 1 ? "img_panoramas_thumb" : "") + " rounded" })));
                }
                $(document).ready(function () {
                    // The slider being synced must be initialized first
                    $('#thu_img').flexslider({
                        animation: "slide",
                        controlNav: false,
                        animationLoop: false,
                        slideshow: false,
                        itemWidth: 140,
                        itemMargin: 5,
                        touch: true,
                        before: function (slider) {
                            changePanoramasSize();
                        },
                        asNavFor: '#orig_img'
                    });
                    $('#orig_img').flexslider({
                        animation: "slide",
                        controlNav: false,
                        directionNav: true,
                        animationLoop: false,
                        slideshow: false,
                        touch: true,
                        start: function (slider) {
                            handleThumbnailClick(data.img[0].img_type);
                            changePanoramasSize();
                            $(".thumb_ul li").bind("click touchstart", function () {
                                _this.lastImg = $(".original_ul").find("li").eq(_this.origCurrImgIndex).find("img");
                                _this.origCurrImgIndex = $(this).index();
                                $(".normal_img_content").removeClass("d-none");
                                $("#photosphere").empty();
                                $("#photosphere").addClass("d-none");
                                $('#orig_img').flexslider(_this.origCurrImgIndex);
                                var type = $(this).find("img").data("type");
                                handleThumbnailClick($(this).find("img").data("type"));
                            });
                        },
                        before: function (slider) {
                            _this.lastImg = $(".original_ul").find("li").eq(slider.currentSlide).find("img");
                            changePanoramasSize();
                        },
                        after: function (slider) {
                            _this.origCurrImgIndex = slider.currentSlide;
                            var type = $(slider[0]).find("li.flex-active-slide").find("img").data("type");
                            handleThumbnailClick(type);
                        },
                        sync: "#thu_img"
                    });
                    function changePanoramasSize() {
                        $(".img_panoramas_thumb").each(function () {
                            var img = $(this);
                            if (img.css("margin-top") === "0px") {
                                var height = img[0].naturalHeight * (img[0].width / img[0].naturalWidth);
                                if (height > img.parent().parent().height()) {
                                    height = img.parent().parent().height();
                                }
                                var marginTop = (1 - (height / img.parent().parent().height())) * 100;
                                marginTop = ((marginTop > 25) ? 25 : marginTop);
                                img.css({ "margin-top": marginTop + "%" });
                                // img.css({ "width": "100%", "height": height + "px", "margin-top": marginTop + "%"});
                                img.parent().attr("style", img.parent().attr("style") + "height:" + img.parent().parent().height() + "px");
                            }
                        });
                    }
                });
                function handleThumbnailClick(type) {
                    if (_this.lastImg && _this.lastImg.hasClass("img_panoramas")) {
                        _this.lastImg.attr("style", "");
                    }
                    if (type == 0) {
                        // Bring the #orig_img back
                    }
                    if (type == 1) {
                        var img = $(".original_ul").find("li").eq(_this.origCurrImgIndex).find("img");
                        var width = img[0].naturalWidth * (img[0].height / img[0].naturalHeight);
                        img.css({ "height": "90%", "width": width + "px" });
                        img.parent().attr("style", img.parent().attr("style") + "height:" + img.parent().parent().height() + "px");
                        PanZoom(".img_panoramas");
                        var notification = $("<span>", { "class": "img_panoramas_noti rounded", text: "Use the scroll to zoom around the room" });
                        img.parent().append(notification);
                        setTimeout(function () {
                            notification.fadeOut(1000);
                            notification.remove();
                        }, 5000);
                    }
                    if (type == 2) {
                        $(".normal_img_content").addClass("d-none");
                        $("#photosphere").removeClass("d-none");
                        $("#photosphere").empty();
                        var pan = data.img[_this.origCurrImgIndex].o_img;
                        _this.viewer = new PhotoSphereViewer({
                            container: 'photosphere',
                            panorama: pan,
                            caption: data.img[_this.origCurrImgIndex].caption
                        });
                        _this.viewer.showNotification({ content: "Use the mouse to drag the view 360 degrees around the room.", timeout: 5000 });
                    }
                }
            }
            function createEssentialInfor(data) {
                // amenities: null
                // available: "11/11/2018"
                // bathrooms: 2
                // bedrooms: 5
                // city: "Hamilton"
                // details: null
                // dishwasher: "don`t know"
                // fibre: null
                // furnishings: null
                // heating: null
                // ideal_tenants: null
                // img: (5) [{…}, {…}, {…}, {…}, {…}]
                // max_tenants: null
                // notes: null
                // details: null
                // parking: null
                // pets_allowed: "Don`t know"
                // region: "Waikato"
                // rent_pw: 120
                // smoke_alarm: "Don`t know"
                // smoking_allowed: "Don`t know"
                // street_name: "Hogan St"
                // street_number: "66"
                // suburb: "Hamilton East"
                // unit_flat_number: "D"
                // video_tour_link: null
                // vimeo_link: "https://vimeo.com/202264561"
                // youtube_link: "https://www.youtube.com/watch?v=6QCgKYTAjm0"
                _this.loadGoogleApiJS();
                var essential_infor = $("#essential_infor"), key_col = "col-4", value_col = "col-8";
                if (data.youtube_link || data.vimeo_link) {
                    var tourContainer = $("<div>", { "class": "row col-12 no-gutters" }).append($("<span>", { "class": key_col, text: "Tour" }));
                    if (data.youtube_link) {
                        tourContainer.append($("<a>", { text: "Youtube", href: data.youtube_link, "class": "btn btn-warning col-3", role: "button", target: "_blank" }));
                        tourContainer.append($("<div>", { "class": "col-1" }));
                    }
                    if (data.vimeo_link) {
                        tourContainer.append($("<a>", { text: "Vimeo", href: data.vimeo_link, "class": "btn btn-warning col-3", role: "button", target: "_blank" }));
                        tourContainer.append($("<div>", { "class": "col-1" }));
                    }
                    essential_infor.append(tourContainer).append($("<hr>", { "class": "col-11 essential_info_hr" }));
                }
                var keysVSText = {
                    "property_id": "Property ID#:", "available": "Avaiable:",
                    "bedrooms": "Bedrooms:", "bathrooms": "Bathrooms:",
                    "parking": "Parking:", "smoke_alarm": "Smoking Alarm:",
                    "smoking_allowed": "Smoking Allowed:", "pets_allowed": "Pets Allowed:",
                    "dishwasher": "Dishwasher:", "fibre": "Fibre:",
                    "heating": "Heating:", "amenities": "Amenities:"
                };
                _this.location = [(data.street_number ? data.street_number + " " : "") + (data.unit_flat_number ? data.unit_flat_number + " " : "") +
                        (data.street_name ? data.street_name : ""), data.suburb, data.city, data.region].join(", ");
                var values = {
                    "Location:": _this.location
                };
                for (var key in keysVSText) {
                    data[key] ? values[keysVSText[key]] = typeof data[key] === "number" ? data[key] : Utilities.capitalizeTxt(data[key]) : null;
                }
                for (var key in values) {
                    essential_infor.append($("<div>", { "class": "row col-12 no-gutters" }).append($("<span>", { "class": key_col, text: key }))
                        .append($("<span>", { "class": value_col, text: values[key] })))
                        .append($("<hr>", { "class": "col-11 essential_info_hr" }));
                }
            }
            function createBookingDetail(data) {
                $("#price_per_week").append($("<span>", { "class": "col-6 font-weight-bold", text: "$" + data.rent_pw + " per week" }));
            }
        }
    };
    PropertyDetail.prototype.loadGoogleApiJS = function () {
        $("body").append("<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDNhqIrCd5zTRVyDPfs7M-hxRJpkNB8DfI&callback=propertyDetail.createGoogleMap'></script>");
    };
    PropertyDetail.prototype.createGoogleMap = function () {
        var _this = this;
        var uluru = { lat: -41.289247, lng: 174.774611 };
        var geocoder = new google.maps.Geocoder();
        var map = new google.maps.Map(document.getElementById('google_map'), { zoom: 5, center: uluru });
        if (_this.location) {
            var address = _this.location;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status === 'OK') {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                    map.setZoom(18);
                }
                else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
    };
    return PropertyDetail;
}());
var propertyDetail = new PropertyDetail();
propertyDetail.getDetail();
