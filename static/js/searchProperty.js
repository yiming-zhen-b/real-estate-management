var SearchProperty = /** @class */ (function () {
    function SearchProperty(itemsPP) {
        this.itemsPP = itemsPP;
        this.itemsPerPage = itemsPP;
        this.stratPosition = 0;
        this.currentPageNum = 1;
        this.fillUrlDataCounter = 0;
        this.searchResultCon = $("#search_result");
        this.paginationCon = $("#search_result_pagination ul.pagination");
    }
    SearchProperty.prototype.checkUrlPara = function () {
        this.urlData = Query.getUrlParams();
        if (this.urlData)
            this.urlDataNum = Utilities.getJSONKey(this.urlData).length;
    };
    SearchProperty.prototype.fillUrlData = function (element, key, _this) {
        if (_this.urlData && _this.urlData[key]) {
            _this.fillUrlDataCounter++;
            element.each(function (index) {
                if ($(this).text().trim().toLowerCase() === _this.urlData[key].trim().toLowerCase()) {
                    $(this).click();
                }
            });
            if (_this.fillUrlDataCounter == _this.urlDataNum) {
                $('#searchHouse').click();
            }
        }
    };
    SearchProperty.prototype.createMultiselect = function (element) {
        element.multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            enableFiltering: true,
            maxHeight: 450,
            templates: {
                li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
            }
        });
    };
    SearchProperty.prototype.getPropertyTypes = function () {
        Query.sendRequest("GET", "/getPropertyAttr/getPropertyType", "", function (data) {
            var options = [];
            data.forEach(function (v) {
                options.push({ value: v.category_id, label: v.category_name });
            });
            $("#search-property-types").multiselect('dataprovider', options);
        });
    };
    SearchProperty.prototype.getBathBedRentNum = function () {
        Query.sendRequest("GET", "/getPropertyAttr/getBBR", "", function (data) {
            data.forEach(function (attr) {
                if (attr.attr_name === "max_bath_num")
                    createOption(["#search-bedroom-from", "#search-bedroom-to"], 1, attr.int_value, "");
                if (attr.attr_name === "max_bed_num")
                    createOption(["#search-bathroom-from", "#search-bathroom-to"], 1, attr.int_value, "");
                if (attr.attr_name === "max_rent")
                    createOption(["#search-rent-per-week-from", "#search-rent-per-week-to"], 50, attr.int_value, "$");
            });
            function createOption(ids, start, maxValue, extraText) {
                ids.forEach(function (element) {
                    var ele = $(element);
                    ele.append($("<option>", { value: 0, text: "Any", selected: true }));
                    for (var i = start; i <= maxValue; i = i + start) {
                        ele.append($("<option>", { value: i, text: extraText + i }));
                    }
                    ele.selectpicker("refresh");
                });
            }
        });
    };
    SearchProperty.prototype.getRegion = function () {
        var _this = this;
        Query.sendRequest("GET", "/getPropertyAttr/getRegion", "", function (data) {
            var region = $("#search-region");
            data.forEach(function (v) {
                region.append($("<option>", { value: v.region_id, text: v.region_name }));
            });
            region.selectpicker("refresh");
            _this.fillUrlData(region.parent().find(".dropdown-menu div.inner.show li a"), "region_name", _this);
        });
    };
    SearchProperty.prototype.handleRegionChangeAndGetCity = function () {
        var _this = this;
        $("#search-region").change(function () {
            var city = disableCity(true);
            _this.disableRegion(true);
            var reValue = $(this).val();
            if (reValue == 0)
                return;
            Query.sendRequest("GET", "/getPropertyAttr/getCity", reValue, function (data) {
                data.forEach(function (v) {
                    city.append($("<option>", { value: v.city_id, text: v.city_name }));
                });
                disableCity(false);
                _this.fillUrlData(city.parent().find(".dropdown-menu div.inner.show li a"), "city_name", _this);
            });
        });
        function disableCity(disable) {
            var _this = this;
            var city = $("#search-city");
            if (disable == true) {
                city.html("");
                city.append($("<option>", { value: "0", text: "All city", selected: true }));
            }
            city.prop("disabled", disable);
            city.selectpicker("refresh");
            return city;
        }
    };
    SearchProperty.prototype.handleCityChangeAndGetSuburb = function () {
        var _this = this;
        $("#search-city").change(function () {
            var suburb = _this.disableRegion(true);
            var ciValue = $(this).val();
            if (ciValue == 0)
                return;
            Query.sendRequest("GET", "/getPropertyAttr/getSuburb", ciValue, function (data) {
                _this.disableRegion(false);
                var options = [];
                data.forEach(function (v) {
                    options.push({ value: v.suburb_id, label: v.suburb_name });
                });
                suburb.multiselect('dataprovider', options);
                _this.fillUrlData(suburb.parent().find("div.btn-group ul.multiselect-container li a label"), "suburb_name", _this);
            });
        });
    };
    SearchProperty.prototype.disableRegion = function (disable) {
        var _this = this;
        var suburb = $("#search-suburb");
        if (disable == true)
            suburb.multiselect('dataprovider', []);
        suburb.multiselect('destroy');
        suburb.prop("disabled", disable);
        _this.createMultiselect(suburb);
        return suburb;
    };
    SearchProperty.prototype.searchObject = function (property_for, availableNow, petsOk, region, city, suburb, propertyType, bedroomFrom, bedroomTo, bathroomFrom, bathroomTo, rentPerWeekFrom, rentPerWeekTo) {
        return JSON.stringify({
            property_for: property_for,
            availableNow: availableNow,
            petsOk: petsOk,
            region: region,
            city: city,
            suburb: suburb,
            propertyType: propertyType,
            bedroomFrom: bedroomFrom,
            bedroomTo: bedroomTo,
            bathroomFrom: bathroomFrom,
            bathroomTo: bathroomTo,
            rentPerWeekFrom: rentPerWeekFrom,
            rentPerWeekTo: rentPerWeekTo
        });
    };
    SearchProperty.prototype.handleSearch = function () {
        var _this = this;
        $('#searchHouse').click(function () {
            var region = $("#search-region").val();
            var city = $("#search-city").val();
            var suburb = $("#search-suburb").val();
            var propertyType = $("#search-property-types").val();
            var availableNow = $("#rent_content #available-now").val();
            var petsOk = $("#rent_content #pets-ok").val();
            var bedroomFrom = Utilities.convertToInt($("#rent_content #search-bedroom-from").val());
            var bedroomTo = Utilities.convertToInt($("#rent_content #search-bedroom-to").val());
            var bathroomFrom = Utilities.convertToInt($("#rent_content #search-bathroom-from").val());
            var bathroomTo = Utilities.convertToInt($("#rent_content #search-bathroom-to").val());
            var rentPerWeekFrom = Utilities.convertToInt($("#rent_content #search-rent-per-week-from").val());
            var rentPerWeekTo = Utilities.convertToInt($("#rent_content #search-rent-per-week-to").val());
            if ($("#rent_tab a").hasClass("active show")) {
                Query.sendRequest("POST", "/searchProperty/search", _this.searchObject("rent", availableNow, petsOk, region, city, suburb, propertyType, bedroomFrom, bedroomTo, bathroomFrom, bathroomTo, rentPerWeekFrom, rentPerWeekTo), function (result) {
                    _this.sortAndCreateResult(result, _this);
                });
            }
            if ($("#sale_tab a").hasClass("active show")) {
                Query.sendRequest("POST", "/searchProperty/search", _this.searchObject("sale", availableNow, petsOk, region, city, suburb, propertyType, bedroomFrom, bedroomTo, bathroomFrom, bathroomTo, rentPerWeekFrom, rentPerWeekTo), function (result) {
                    _this.sortAndCreateResult(result, _this);
                });
            }
        });
    };
    SearchProperty.prototype.sortAndCreateResult = function (result, _this) {
        _this.searchResult = result;
        if (_this.searchResult.status && _this.searchResult.data.length > 0) {
            _this.currentPageNum = 1;
            if (!_this.searchResult.data[0].formatedDate) {
                _this.searchResult.data.forEach(function (e) {
                    var dates = e.available.split("/");
                    e.formatedDate = new Date([dates[2], dates[1], dates[0]].join("-"));
                });
            }
            var sort = $("#sort_by").val();
            if (sort == 1)
                _this.searchResult.data.sort(function (a, b) { return a.rent_pw - b.rent_pw; });
            if (sort == 2)
                _this.searchResult.data.sort(function (a, b) { return b.rent_pw - a.rent_pw; });
            if (sort == 3)
                _this.searchResult.data.sort(function (a, b) { return a.formatedDate - b.formatedDate; });
            if (sort == 4)
                _this.searchResult.data.sort(function (a, b) { return b.formatedDate - a.formatedDate; });
            console.log(_this.searchResult.data);
            _this.createResult(_this);
            _this.createPagination(_this);
        }
        else {
            _this.paginationCon.empty();
            _this.searchResultCon.html($("<div>", { "class": "text-center col-12" }).append($("<span>", {
                "class": "align-self-center font-weight-bold",
                text: "There is no result in " +
                    [$("#search-suburb").parent().find("button span.multiselect-selected-text").text(),
                        $("#search-city").parent().find("button div.filter-option-inner-inner").text(),
                        $("#search-region").parent().find("button div.filter-option-inner-inner").text()
                    ].join(", ")
            })));
        }
    };
    SearchProperty.prototype.createResult = function (_this) {
        // console.log( _this.searchResult.data);        
        // address_no: "317A"
        // address_st: "Cobham Drive"
        // address_unit: null
        // available: "26/01/2018"
        // bath_icon: "http://localhost:8088/icon/bathroom.svg"
        // bathrooms: 1
        // bed_icon: "http://localhost:8088/icon/bedroom.svg"
        // bedrooms: 2
        // c_img: "http://localhost:8088/thumb/8A.jpg"
        // city: "Hamilton"
        // gar_icon: "http://localhost:8088/icon/garage.svg"
        // parking: "Single garage"
        // property_id: 1
        // region: "Waikato"
        // rent_pw: 380
        // suburb: "Hillcrest"
        _this.searchResultCon.empty();
        var stratPosition = (_this.currentPageNum - 1) * _this.itemsPerPage;
        var itemCount = 0;
        for (var i = stratPosition; i < _this.searchResult.data.length; i++) {
            var e = _this.searchResult.data[i];
            var row = $("<div>", { "class": "row search_result_property cursor_pointer border rounded", "data-value": e.property_id }), imgContainer = $("<img>", { src: _this.searchResult.img[e.property_id], "class": "col-md-4 col-sm-12 thumb_img img-thumbnail" }), 
            // imgContainer = $("<div>", { id: "thumb_img", class: "flexslider thumb_img col-4" }),
            detailContainer = $("<div>", { "class": "col-md-8 col-sm-12" })
                .append($("<div>", { "class": "row col-12" })
                .append($("<span>", { text: [e.street_number, e.unit_flat_number, Utilities.capitalizeTxt(e.street_name)].join(" "), "class": "col-md-6 col-sm-12 font-weight-bold" })))
                .append($("<div>", { "class": "col-12" }).append($("<span>", { text: [Utilities.capitalizeTxt(e.suburb), Utilities.capitalizeTxt(e.city)].join(", ") })))
                .append($("<div>", { "class": "row col-12" }).append($("<span>", { "class": "col-md-6 col-sm-12", text: "Available from " + e.available }))
                .append($("<span>", { text: "$" + e.rent_pw + " per week", "class": "col-md-6 col-sm-12 text-right font-weight-bold" })))
                .append($("<hr>", { "class": "col-11" }))
                .append($("<div>", { "class": "row col-12" })
                .append($("<div>", { "class": "row col-md-2 col-sm-4 no-gutters" })
                .append($("<img>", { src: _this.searchResult.icon.bed, "class": "house_attr_icon col-6 align-self-center" }))
                .append($("<span>", { text: e.bedrooms, "class": "col-6  align-self-center" })))
                .append($("<div>", { "class": "row col-md-2 col-sm-4 no-gutters" })
                .append($("<img>", { src: _this.searchResult.icon.bath, "class": "house_attr_icon col-6 align-self-center" }))
                .append($("<span>", { text: e.bathrooms, "class": "col-6 align-self-center" })))
                .append($("<div>", { "class": "row col-md-8 col-sm-4 no-gutters" })
                .append($("<img>", { src: _this.searchResult.icon.gar, "class": "house_attr_icon col-2 align-self-center" }))
                .append($("<span>", { text: e.parking, "class": "col-10 align-self-center" }))));
            // createImgSlider(imgContainer, e.c_t_img);
            _this.searchResultCon.append(row.append(imgContainer, detailContainer)).append($("<br>"));
            // initialiseImgSlider();
            itemCount++;
            _this.stratPosition = i + 1;
            if (itemCount >= _this.itemsPerPage)
                break;
        }
        $(".search_result_property").on("click", function () { Query.redirectNewTab("/propertyDetail.html", $(this).data("value")); });
        function createImgSlider(container, data) {
            var thumblUL = $("<ul>", { "class": "slides thumb_ul" });
            for (var i = 0; i < 5; i++) {
                thumblUL.append($("<li>").append($("<img>", { src: data })).append($("<p>", { text: "Bedroom", "class": "flex-caption" })));
            }
            // data.img.forEach(img => {
            //     thumblUL.append($("<li>").append($("<img>", { src: img.o_img })).append($("<p>", { text: "Bedroom", class: "flex-caption" })));
            // });
            container.append(thumblUL);
        }
        function initialiseImgSlider() {
            $('.thumb_img').flexslider({
                animation: "slide",
                slideshow: false
            });
            $(".flex-nav-next").on("click", function (e) { e.stopPropagation(); });
        }
    };
    SearchProperty.prototype.createPagination = function (_this) {
        _this.paginationCon.empty();
        _this.pageNumber = Math.ceil(_this.searchResult.data.length / _this.itemsPerPage);
        if (_this.pageNumber != 0) {
            _this.paginationCon.append($("<li>", { "class": "page-item page-item-pre disabled", "data-value": "p" })
                .append($("<a>", { text: "Previous", "class": "page-link", href: "#", tabindex: "-1" })));
            for (var i = 1; i <= _this.pageNumber; i++) {
                _this.paginationCon.append($("<li>", { id: "page-item-" + i, "class": i == 1 ? "page-item active disabled" : "page-item", "data-value": i })
                    .append($("<a>", { text: i, "class": "page-link", href: "#" })));
            }
            _this.paginationCon.append($("<li>", { "class": "page-item page-item-next", "data-value": "n" })
                .append($("<a>", { text: "Next", "class": "page-link", href: "#" })));
            _this.handlePagniationClick(_this);
        }
        _this.changePaginationStatus(_this);
    };
    SearchProperty.prototype.changePaginationStatus = function (_this) {
        $(".page-item").removeClass("active disabled pointer-none");
        $("#page-item-" + _this.currentPageNum).addClass("active disabled pointer-none");
        if (_this.pageNumber == "1" || _this.currentPageNum == 1)
            $(".page-item-pre").addClass("disabled pointer-none");
        if (_this.currentPageNum == _this.pageNumber)
            $(".page-item-next").addClass("disabled pointer-none");
        var total = _this.searchResult.data.length;
        var from = total == 0 ? 0 : (_this.currentPageNum - 1) * _this.itemsPerPage + 1;
        var to = _this.currentPageNum * _this.itemsPerPage > total ? total : _this.currentPageNum * _this.itemsPerPage;
        $("#result_summary").empty();
        $("#result_summary").append($("<span>", { text: total + " results, showing " + from + " to " + to }));
    };
    SearchProperty.prototype.handlePagniationClick = function (_this) {
        $(".page-item").on("click", function () {
            var val = $(this).data("value");
            if (typeof val === "number") {
                if (_this.currentPageNum == val)
                    return;
                _this.currentPageNum = val;
            }
            else {
                if (val === "p" && _this.currentPageNum != 1)
                    _this.currentPageNum--;
                if (val === "n" && _this.currentPageNum != _this.pageNumber)
                    _this.currentPageNum++;
            }
            _this.changePaginationStatus(_this);
            _this.createResult(_this);
        });
    };
    SearchProperty.prototype.handleSortOptionClick = function () {
        var _this = this;
        $("#sort_by").change(function () {
            if (_this.searchResult)
                _this.sortAndCreateResult(_this.searchResult, _this);
        });
    };
    SearchProperty.prototype.getDefault = function () {
        var _this = this;
        if ($.isEmptyObject(_this.urlData)) {
            Query.sendRequest("POST", "/searchProperty/getDefault", "", function (result) {
                _this.sortAndCreateResult(result, _this);
            });
        }
    };
    return SearchProperty;
}());
var searchProperty = new SearchProperty(5);
searchProperty.checkUrlPara();
searchProperty.createMultiselect($("#search-suburb"));
searchProperty.createMultiselect($("#search-property-types"));
searchProperty.getPropertyTypes();
searchProperty.getBathBedRentNum();
searchProperty.getRegion();
searchProperty.handleRegionChangeAndGetCity();
searchProperty.handleCityChangeAndGetSuburb();
searchProperty.handleSearch();
searchProperty.handleSortOptionClick();
searchProperty.getDefault();
