var DashboardSecurity = /** @class */ (function () {
    function DashboardSecurity() {
        this.securityContentSaveBtn = $("#security_content_save_btn");
        this.securityContent = $("#security_content");
        this.oldSecurityData = { user_pass: "" };
        this.newSecurityData = {};
        var _this = this;
        // _this.securityContent.find(".security_input_data").focusout(function() {
        //     _this.checkValue($(this).attr("name"), $(this).val());
        // });
        _this.securityContent.find(".security_input_data").on("input", function () {
            _this.checkValue($(this).attr("name"), $(this).val());
        });
        $('#security_tab a').click(function () {
            $($(this).data("target")).find(".info_input_data").attr("disabled", true);
        });
        $("#security_content_save_btn").click(function () {
            var pass = $("#user_pass");
            var passVal = pass.val();
            if (passVal == "" || !passVal)
                return;
            Query.sendRequest("POST", "/dashSecurity/updatePass", JSON.stringify({ pass: passVal }), function (data) {
                if (data.status) {
                    pass.val("");
                    new Dialog().show("Your information has been updated!").setBtnClick(null);
                    $("#security_tab a").click();
                }
            });
        });
        $("#remove_my_account").click(function () {
            new Dialog().show('Are you sure you want to delete your account? All the property you listed will also be removed.')
                .addCloseBtn().setBtnClick(function () {
                var dialog = new Dialog({ disabledBtn: true, processBar: true }).show('Deleting property, please do not close this window.');
                Query.sendRequest("POST", "/dashSecurity/removeMyAccount", "", function (result) {
                    if (result.status) {
                        dialog.changeText('Your account is deleted!').disabledBtn(false).setBtnClick(function () {
                            Query.redirect("/");
                        }).removeAnimation();
                    }
                    else {
                        dialog.changeText('Error').disabledBtn(false).setBtnClick(null).removeAnimation();
                    }
                });
            });
        });
    }
    DashboardSecurity.prototype.checkValue = function (key, value) {
        if (this.oldSecurityData[key] != value) {
            this.newSecurityData[key] = value;
        }
        else {
            delete this.newSecurityData[key];
        }
        if ($.isEmptyObject(this.newSecurityData)) {
            this.securityContentSaveBtn.attr("disabled", true);
        }
        else {
            this.securityContentSaveBtn.attr("disabled", false);
        }
    };
    return DashboardSecurity;
}());
