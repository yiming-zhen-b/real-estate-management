var DashboardDocument = /** @class */ (function () {
    function DashboardDocument() {
        this.personalPhoto = { dropzone: null, con: "#personal_photo" };
        this.licencePhoto = { dropzone: null, con: "#licence_photo" };
        this.cvPhoto = { dropzone: null, con: "#cv_photo", key: "CV" };
        this.otherPhoto = { dropzone: null, con: "#other_photo" };
        this.keyArray = { "Personal photo": this.personalPhoto, "Licence photo": this.licencePhoto, "Other": this.otherPhoto, "CV": this.cvPhoto };
        this.infoContentSaveBtn = $("#info_content_save_btn");
        var _this = this;
        Query.sendRequest("POST", "/dashDocument/getMaxFileSize", "", function (data) {
            if (data.status) {
                var val = data.data;
                for (var i = 0; i < val.length; i++) {
                    $(_this.keyArray[val[i].name].con).addClass("dropzone");
                    _this.keyArray[val[i].name].dropzone = new MyDropZoneDoc(_this.keyArray[val[i].name].con, {
                        maxFiles: val[i].max_value,
                        doc_type: val[i].id, acceptedFiles: val[i].valid_extname
                    });
                }
                Query.sendRequest("POST", "/dashDocument/getAll", "", function (data) {
                    if (data.status) {
                        var docs = data.data;
                        for (var i = 0; i < docs.length; i++) {
                            _this.keyArray[docs[i].name].dropzone.mockFile(docs[i]);
                        }
                    }
                });
            }
        });
    }
    return DashboardDocument;
}());
var MyDropZoneDoc = /** @class */ (function () {
    function MyDropZoneDoc(id, options) {
        this.options = {
            maxFiles: null,
            doc_type: null,
            acceptedFiles: null
        };
        var _this = this;
        if (options) {
            for (var k in options) {
                if (options[k] != null)
                    this.options[k] = options[k];
            }
        }
        Dropzone.autoDiscover = false;
        _this.dropzone = new Dropzone(id, {
            url: Config.api_base_url + "/dashDocument/upload",
            method: "POST",
            paramName: "doc",
            acceptedFiles: _this.options.acceptedFiles,
            parallelUploads: _this.options.maxFiles,
            addRemoveLinks: true,
            maxFiles: _this.options.maxFiles,
            maxThumbnailFilesize: 10,
            headers: {
                'Authorization': SessionStorage.getItem("u") ? SessionStorage.getItem("u") : "",
                'Cache-Control': null,
                'X-Requested-With': null
            }
        });
        _this.dropzone.on("addedfile", function (file) {
            if (file.uploaded) {
                addViewDoc(file);
                addCopyToClipboard(file);
            }
            else {
                file.doc_type = _this.options.doc_type;
            }
        });
        _this.dropzone.on("success", function (file, response) {
            file.id = response.id;
            file.dataURL = response.url;
            addViewDoc(file);
            addCopyToClipboard(file);
        });
        function addCopyToClipboard(file) {
            file._copyLink = Dropzone.createElement("<div class='col-12 text-center'><button type='button' class='btn btn-link btn-sm'>Copy Link</button></div>");
            file.previewElement.appendChild(file._copyLink);
            $(file._copyLink.querySelector("button")).click(function () {
                Utilities.copyToClipboard(file.dataURL);
            });
        }
        function addViewDoc(file) {
            file._viewDoc = Dropzone.createElement("<div class='col-12 text-center'><a class='btn btn-link btn-sm' href='" + file.dataURL + "' target='_blank' >View</a></div>");
            file.previewElement.appendChild(file._viewDoc);
        }
        _this.dropzone.on('sending', function (file, xhr, formData) {
            formData.append('doc_type', file.doc_type);
        });
        _this.dropzone.on("error", function (file, message, xhr) {
            if (xhr) {
                var header = xhr.status + ": " + xhr.statusText;
                $(file.previewElement).find('.dz-error-message').text(header);
            }
        });
        _this.dropzone.on("removedfile", function (file) {
            if (file.id) {
                Query.sendRequest("POST", "/dashDocument/remove", file.id, function (data) {
                    if (!data.status) {
                        _this.dropzone.mockFile(file);
                    }
                });
            }
        });
        return this;
    }
    MyDropZoneDoc.prototype.mockFile = function (aFile) {
        var _this = this;
        var mockFile = {
            dataURL: aFile.url, name: aFile.originalname, status: "success", id: aFile.id,
            accepted: true, uploaded: true, size: 12345, doc_type: aFile.doc_type
        };
        _this.dropzone.emit("addedfile", mockFile);
        _this.dropzone.files.push(mockFile);
        var docKind = ["png", "jpg", "gif", "bmp", "jpeg"];
        var kind = aFile.originalname.split(".");
        if (docKind.indexOf(kind[kind.length - 1].toLowerCase()) > -1) {
            _this.dropzone.createThumbnailFromUrl(mockFile, 120, 120, "crop", true, function (thumbnail) {
                _this.dropzone.emit('thumbnail', mockFile, thumbnail);
            }, "anonymous");
        }
        // Make sure that there is no progress bar, etc...
        _this.dropzone.emit("complete", mockFile);
    };
    return MyDropZoneDoc;
}());
