var DashboardApp = /** @class */ (function () {
    function DashboardApp() {
        this.__this = this;
        this.appOldImg = {};
        this.imageUploadId = "div#property_images";
        this.dateFormatSpecifier = '%d/%m/%Y';
        this.isoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S.%LZ");
        this.dateFormat = d3.timeFormat(this.dateFormatSpecifier);
        this.dateFormatParser = d3.timeParse(this.dateFormatSpecifier);
        this.applicationEditModalAttr = $("#application_edit_modal_attr");
        this.applicationEditModalImg = $("#application_edit_modal_iamge");
        this.applicationPropertyForChartId = "#dc_application_property_for_row_chart";
        this.applicationRegionChartId = "#dc_application_region_row_chart";
        this.applicationCityChartId = "#dc_application_city_row_chart";
        this.applicationSuburbChartId = "#dc_application_suburb_chart";
        this.applicationDataCountId = "#dc_application_data_count";
        this.applicationTableId = "#dc_application_table";
        this.propertyForRowChart = dc.rowChart(this.applicationPropertyForChartId);
        this.regionRowChart = dc.rowChart(this.applicationRegionChartId);
        this.cityForRowChart = dc.rowChart(this.applicationCityChartId);
        this.suburbForRowChart = dc.rowChart(this.applicationSuburbChartId);
        this.nasdaqCount = dc.dataCount(this.applicationDataCountId);
        this.table = dc.dataTable(this.applicationTableId);
        var _this = this;
        Query.sendRequest("GET", "/getPropertyAttr/getProImageTypes", "", function (data) {
            _this.propertyPhotoTypes = data;
        });
        // initialise the application Modal
        Query.sendRequest("GET", "/getPropertyAttr/getPropertyType", "", function (data) {
            var propertyType = $("#listing-property-type");
            data.forEach(function (v) {
                propertyType.append($("<a>", {
                    "class": "list-group-item list-group-item-action",
                    "data-toggle": "list",
                    "data-value": v.category_id,
                    href: "",
                    text: v.category_name
                }));
            });
        });
        // this.applicationEditModalAttr.on('hidden.bs.modal', function (e) {
        //     $("body").attr("style", "");
        // })
        $('#datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        // register the application Modal phone checkbox
        $("input[name='phone_display_landline']:checkbox, input[name='phone_display_mobile']:checkbox").change(function () {
            var element = $(this);
            var checkFieldId = element.data("check-field");
            if (element.is(':checked')) {
                $(checkFieldId).data("required", true);
                $($(checkFieldId)).attr("disabled", false);
            }
            else {
                $(checkFieldId).data("required", false);
                $($(checkFieldId)).attr("disabled", true);
                $($(checkFieldId)).val("");
                $($(checkFieldId).data("error-field")).removeClass("bg-warning");
            }
        });
    }
    // Application tab click
    DashboardApp.prototype.handleApplicationTabClick = function () {
        var _this = this;
        $('#application_tab a').click(function () {
            Query.sendRequest("POST", "/dashboard/getAllApp", "", function (result) {
                _this.applications = result;
                console.log(_this.applications);
                if (_this.applications.status && _this.applications.data.length) {
                    $("#application_charts").removeClass("d-none");
                    $("#application_warring").addClass("d-none");
                    var data = _this.applications.data;
                    data.forEach(function (d) {
                        d.address = "<a href='/propertyDetail.html?d=\"" + d.id + "\"' target='_blank'>" + [d.street_number + " " + d.street_name + " " + d.suburb, d.city, d.region].join(", ") + "</a>";
                        d["for"] = Utilities.capitalizeTxt(d.property_for);
                        d["match tenants"] = "<button data-id='" + d.id + "' onclick='matchTenants.show(this);' type='button' class='match_tenants btn btn-secondary btn-cus'>Match Tenants</button>";
                        d.edit = '<div class="btn-group app_edit_dropdown" role="group">' +
                            '<button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-cus dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Edit</button>' +
                            '<div class="dropdown-menu dropdown_application_edit dropdown-menu-cus" aria-labelledby="btnGroupDrop1">' +
                            '<a class="dropdown-item" data-id="' + d.id + '" onclick="return dashboardApp.editPropertyAttr(this);" href="#">Attributes</a>' +
                            '<div class="dropdown-divider"></div>' +
                            '<a class="dropdown-item" data-id="' + d.id + '" onclick="return dashboardApp.editPropertyImage(this);" href="#">Images</a>' +
                            '</div>' +
                            '</div>';
                        d.status = '<div class="btn-group app_edit_dropdown" role="group">' +
                            '<button data-id="' + d.id + '" data-current-value="' + d.disabled + '" id="btnGroupDrop1" type="button" class="btn btn-secondary btn-cus dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (d.disabled == "true" ? "Disabled" : "Enabled") + '</button>' +
                            '<div class="dropdown-menu dropdown_application_edit dropdown-menu-cus" aria-labelledby="btnGroupDrop1">' +
                            '<a class="dropdown-item" data-new-value="false" onclick="return dashboardApp.changePrppertyStatus(this);" href="#">Enable</a>' +
                            '<div class="dropdown-divider"></div>' +
                            '<a class="dropdown-item" data-new-value="true" onclick="return dashboardApp.changePrppertyStatus(this);" href="#">Disable</a>' +
                            '</div>' +
                            '</div>';
                        d.remove = "<button data-id='" + d.id + "' onclick='dashboardApp.removeProperty(this);' type='button' class='remove_btn btn btn-primary btn-cus'>Remove</button>";
                        d.avaFormatedDate = _this.dateFormatParser(d.available);
                        d["listed date"] = _this.dateFormat(_this.isoParse(d.listed_date));
                        d.listFormatedDate = _this.dateFormatParser(d["listed date"]);
                    });
                    var ndx = crossfilter(data), all = ndx.groupAll(), tableDimension = ndx.dimension(function (d) { return d["for"]; });
                    var charts = [
                        { chart: _this.propertyForRowChart, dimension: ndx.dimension(function (d) { return d["for"]; }) },
                        { chart: _this.regionRowChart, dimension: ndx.dimension(function (d) { return d.region; }) },
                        { chart: _this.cityForRowChart, dimension: ndx.dimension(function (d) { return d.city; }) },
                        { chart: _this.suburbForRowChart, dimension: ndx.dimension(function (d) { return d.suburb; }) }
                    ];
                    charts.forEach(function (aRowChart) {
                        aRowChart.chart
                            .width($(_this.applicationPropertyForChartId).parent().parent().width() * 0.96 / 4)
                            .height(150)
                            .margins({ top: 0, left: 10, right: 10, bottom: 20 })
                            .group(aRowChart.dimension.group())
                            .dimension(aRowChart.dimension)
                            // .ordinalColors(['#3182bd', '#6baed6', '#9ecae1', '#c6dbef', '#dadaeb'])
                            .label(function (d) { return d.key; })
                            .title(function (d) { return d.value; })
                            .elasticX(true)
                            .xAxis().ticks(3);
                    });
                    _this.nasdaqCount
                        .dimension(ndx)
                        .group(all)
                        .html({
                        some: '<strong>%filter-count</strong> selected out of <strong>%total-count</strong> records' +
                            ' | <a href=\'javascript:dc.filterAll(); dc.renderAll();\'>Reset All</a>',
                        all: 'All <strong>' + all.value() + '</strong> records have been selected. Please click on the graph to apply filters.'
                    });
                    _this.table
                        .width($(_this.applicationTableId).width())
                        .dimension(tableDimension)
                        .group(function (d) {
                        return "";
                        // var format = d3.format('02d');
                        // return "Group: " + d.listFormatedDate.getFullYear() + '/' + format((d.listFormatedDate.getMonth() + 1));
                    })
                        .showGroups(false)
                        .columns(["address", "listed date", "available", "for", "match tenants", "status", "edit", "remove"])
                        .size(all.value())
                        .order(d3.descending)
                        .sortBy(function (d) { return d.listFormatedDate; })
                        .on('renderlet', function (table) {
                        var sortValue = $("#sort_by").val();
                        var columnClass = "._1";
                        if (sortValue == 3 || sortValue == 4)
                            columnClass = "._2";
                        table.selectAll('.dc-table-column' + columnClass).classed('font-weight-bold', true);
                        table.selectAll("thead tr th").each(function () {
                            if ($(this).text() == "Edit" || $(this).text() == "Remove") {
                                $(this).addClass("d-none");
                            }
                        });
                        table.selectAll("select.selectpicker").each(function () {
                            $(this).selectpicker("refresh");
                        });
                        if (SessionStorage.getItem("show_matched_tenants")) {
                            table.selectAll("button.match_tenants").each(function () {
                                if ($(this).data("id") == SessionStorage.getItem("show_matched_tenants")) {
                                    $(this).click();
                                    SessionStorage.removeItem("show_matched_tenants");
                                    return;
                                }
                            });
                        }
                    });
                    dc.renderAll();
                    _this.sortTable(_this);
                }
                else {
                    $("#application_warring").removeClass("d-none");
                    $("#application_charts").addClass("d-none");
                }
            });
        });
    };
    // Edit attribute click, fill the information from database
    DashboardApp.prototype.editPropertyAttr = function (element) {
        var _this = this;
        _this.appNewValue = {};
        var pro_id = $(element).data("id");
        var eleAddress = $(element).parent().parent().parent().parent().find("td").first().text();
        _this.applicationEditModalAttr.find("h5.modal-title").html("Editing " + "<span class='text-primary'>" + eleAddress + "</span>");
        if (!pro_id)
            return;
        Query.sendRequest("POST", "/dashboard/getDetails", pro_id, function (result) {
            console.log(result);
            if (result.status) {
                _this.applicationEditModalAttr.data("pro_id", pro_id);
                _this.appOldValue = result.data[0];
                // trim the value
                for (var oKey in _this.appOldValue) {
                    if (!_this.appOldValue[oKey]) {
                        _this.appOldValue[oKey] = "";
                    }
                    _this.appOldValue[oKey] = Utilities.capitalizeTxt(_this.appOldValue[oKey]);
                }
                // Fill property category
                $("#listing-property-type a").removeClass("active");
                $("#listing-property-type a").each(function () {
                    if ($(this).data("value") == _this.appOldValue.category_id) {
                        $(this).addClass("active");
                    }
                });
                // Fill text input
                [".application_edit_input_data", ".application_edit_textarea_data"].forEach(function (ele) {
                    $(ele).each(function () {
                        $(this).val(_this.appOldValue[$(this).attr("name")]);
                    });
                });
                // Fill radio and checkbox
                $(".application_edit_radio_data").each(function () {
                    $("#details_content input[name='" + $(this).attr("name") + "']").prop('checked', false);
                    if (_this.appOldValue[$(this).attr("name")]) {
                        $("#details_content input[name='" + $(this).attr("name") + "'][value='" + _this.appOldValue[$(this).attr("name")] + "']").prop('checked', true);
                    }
                });
                _this.applicationEditModalAttr.modal('show');
            }
            else {
                new Dialog().show("Error!").setBtnClick(null);
            }
        });
    };
    // Collect the application data, after edit
    DashboardApp.prototype.collectApplicationEditAttrData = function () {
        var _this = this;
        var errorID = undefined;
        // collect property category and property for
        ["#listing-property-for a.active", "#listing-property-type a.active"].forEach(function (ele) {
            var aEle = $(ele);
            if (aEle.length === 1) {
                checkValue(aEle.parent().attr("name"), aEle.data("value"));
                aEle.removeClass("bg-warning");
            }
            else {
                aEle.addClass("bg-warning");
            }
        });
        // collect radio and checkbox
        $(".application_edit_radio_data").each(function () {
            var val = $("#details_content input[name='" + $(this).attr("name") + "']:checked").val();
            if ($(this).attr("type") === "checkbox" && !val)
                val = "False";
            checkValue($(this).attr("name"), val);
        });
        // collect text input
        [".application_edit_input_data", ".application_edit_textarea_data"].forEach(function (ele) {
            $(ele).each(function () {
                if ($(this).data("required") == true && !$(this).val()) {
                    $($(this).data("error-field")).addClass("bg-warning");
                    if (errorID === undefined)
                        errorID = $(this).attr("id");
                }
                else {
                    $($(this).data("error-field")).removeClass("bg-warning");
                }
                checkValue($(this).attr("name"), $(this).val());
            });
        });
        // Check if any value changed
        function checkValue(key, value) {
            if (_this.appOldValue[key] != value) {
                _this.appNewValue[key] = value;
            }
            else {
                delete _this.appNewValue[key];
            }
        }
        console.log(_this.appNewValue);
        return errorID === undefined ? true : errorID;
    };
    // appication Modal Attribute save click
    DashboardApp.prototype.saveApplicationEditAttr = function () {
        var _this = this;
        var chechResult = _this.collectApplicationEditAttrData();
        if (chechResult == true) {
            if ($.isEmptyObject(_this.appNewValue)) {
                _this.hideApplicationEditModal();
                return;
            }
            var body = "";
            var table = $("<table>", { "class": "col-12 table table-hover" });
            var tbody = $("<tbody>");
            for (var nKey in _this.appNewValue) {
                var text = getConfirmText(nKey);
                tbody.append($("<tr>").append($("<th>", { "scope": "row", text: text.text }))
                    .append($("<td>", { text: text.old }))
                    .append($("<td>", { text: text["new"] })));
            }
            table.append($("<thead>").append($("<tr>").append($("<th>", { "scope": "col", text: "Name" }))
                .append($("<th>", { "scope": "col", text: "Old Value" }))
                .append($("<th>", { "scope": "col", text: "New Value" })))).append(tbody);
            // Check if the user wants to change the address, then update all the values
            var addressKey = ["region", "city", "suburb"];
            for (var newKey in _this.appNewValue) {
                if ($.inArray(newKey, addressKey) > -1) {
                    addressKey.forEach(function (aKey) {
                        _this.appNewValue[aKey] = $("input#" + aKey).val();
                    });
                    break;
                }
            }
            new Dialog({ dialogSize: "lg" }).show().setBody(table)
                .addCss("style", "top:5%;")
                .setHeader("Please confirm the following changes:")
                .addCloseBtn(function () { _this.applicationEditModalAttr.css("z-index", 1050); }, "Go back")
                .setBtnClick(function () {
                var dialog = new Dialog({ disabledBtn: true, processBar: true }).show("Uploading data, please wait.");
                _this.appNewValue.pro_id = _this.applicationEditModalAttr.data("pro_id");
                Query.sendRequest("POST", "/dashboard/updateProperty", JSON.stringify(_this.appNewValue), function (result) {
                    if (result.status) {
                        dialog.changeText("Uploading finished.").disabledBtn(false).setBtnClick(function () {
                            _this.hideApplicationEditModal();
                            _this.applicationEditModalAttr.css("z-index", 1050);
                        }).removeAnimation();
                    }
                    else {
                        dialog.changeText(result.message ? result.message : "Error").disabledBtn(false).setBtnClick(function () {
                            _this.clickApplicationTab();
                            _this.applicationEditModalAttr.css("z-index", 1050);
                        }).removeAnimation();
                    }
                });
            });
            _this.applicationEditModalAttr.css("z-index", 1030);
        }
        else {
            $("#details_tab a").click();
            $('.modal.fade.show').animate({
                scrollTop: $("#" + chechResult).offset().top
            }, 500);
        }
        function getConfirmText(key) {
            function format(id) {
                var value = "";
                $("#listing-property-type a").each(function () {
                    if ($(this).data("value") == id) {
                        value = $(this).text();
                    }
                });
                return value;
            }
            if (key === "category_id")
                return { text: "Category", old: format(_this.appOldValue[key]), "new": format(_this.appNewValue[key]) };
            else
                return { text: Utilities.capitalizeTxt(key.replace(/_/g, " ")), old: _this.appOldValue[nKey], "new": _this.appNewValue[key] };
        }
    };
    // appication Modal Attribute close click
    DashboardApp.prototype.closeApplicationEditModalAttr = function () {
        var _this = this;
        _this.collectApplicationEditAttrData();
        if (!$.isEmptyObject(_this.appNewValue)) {
            new Dialog().show("You have not save the changes, you want to discard the changes?")
                .addCloseBtn(function () {
                _this.applicationEditModalAttr.css("z-index", 1050);
            }).setBtnClick(function () {
                _this.hideApplicationEditModal();
                _this.applicationEditModalAttr.css("z-index", 1050);
            });
            _this.applicationEditModalAttr.css("z-index", 1030);
            return;
        }
        _this.hideApplicationEditModal();
    };
    // Hide appication Modal
    DashboardApp.prototype.hideApplicationEditModal = function () {
        this.appNewValue = {};
        $("body").attr("style", "");
        $(".bg-warning").removeClass("bg-warning");
        this.applicationEditModalAttr.data("pro_id", "");
        this.applicationEditModalAttr.find("h5.modal-title").html("<span class='text-primary'>No property selected.</span>");
        this.applicationEditModalAttr.find("li a").first().click();
        this.applicationEditModalAttr.modal('hide');
    };
    // Edit image click, fill the information from database
    DashboardApp.prototype.editPropertyImage = function (element) {
        var _this = this;
        _this.appImg = { removed: [], modified: {} };
        var pro_id = $(element).data("id");
        var eleAddress = $(element).parent().parent().parent().parent().find("td").first().text();
        $(_this.imageUploadId).addClass("dropzone");
        _this.dashMyDropZone = new MyDropZone(_this.imageUploadId, {
            removeFileCallback: function (file) { _this.setFileCount(); },
            onUploadedFileRemove: function (id) {
                var img_id = id;
            },
            addFileCallback: function () { _this.setFileCount(); },
            uploadedFileRemoveCallback: function (file) {
                _this.appImg.removed.push(file.id);
                if (_this.appImg.modified[file.id])
                    delete _this.appImg.modified[file.id];
            }
        });
        _this.applicationEditModalImg.find("h5.modal-title").html("Editing " + "<span class='text-primary'>" + eleAddress + "</span>");
        Query.sendRequest("POST", "/dashboard/getImages", pro_id, function (result) {
            console.log(result);
            if (result.status) {
                _this.applicationEditModalImg.data("pro_id", pro_id);
                result.data[0].img.forEach(function (aImage) {
                    _this.appOldImg[aImage.id] = {
                        caption: aImage.caption ? aImage.caption : "",
                        is_cover: aImage.is_cover == "true" ? "true" : "false",
                        img_type: "" + aImage.img_type
                    };
                });
                _this.dashMyDropZone.mockFile(result.data[0].img);
                // Set iamge count
                _this.setFileCount();
                _this.applicationEditModalImg.modal('show');
            }
            else {
                new Dialog().show("Error!").setBtnClick(null);
            }
        });
    };
    DashboardApp.prototype.collectApplicationEditImage = function () {
        var _this = this;
        _this.dashMyDropZone.dropzone.files.forEach(function (aFile) {
            if (aFile.uploaded) {
                var newData = {
                    caption: $(aFile._captionBox).val(),
                    is_cover: $(aFile._coverImgCheck).find("input[name='cover_image_radio']").is(':checked') == true ? "true" : "false",
                    img_type: $(aFile._typeOptions).val()
                };
                if (JSON.stringify(_this.appOldImg[aFile.id]) !== JSON.stringify(newData)) {
                    _this.appImg.modified[aFile.id] = newData;
                }
            }
        });
        console.log(_this.appImg);
    };
    DashboardApp.prototype.saveApplicationEditImage = function (ele) {
        var _this = this;
        _this.collectApplicationEditImage();
        var pro_id = _this.applicationEditModalImg.data("pro_id");
        if (!$.isEmptyObject(_this.appImg.removed) || !$.isEmptyObject(_this.appImg.modified) || _this.dashMyDropZone.validFileLength() > 0) {
            var dialog = new Dialog({ disabledBtn: true, processBar: true }).show('Uploading Data ...');
            Query.sendRequest("POST", "/dashboard/modifiyImage", JSON.stringify(_this.appImg), function (result) {
                if (result.status == true) {
                    _this.dashMyDropZone.uploadImage(pro_id, {
                        success: function () {
                            dialog.changeText("Finished.").disabledBtn(false).setBtnClick(function () {
                                _this.applicationEditModalImg.css("z-index", 1050);
                                _this.hideApplicationEditModalImage();
                            }).removeAnimation();
                        }
                    });
                }
                else {
                    dialog.changeText("Error").disabledBtn(false).setBtnClick(function () {
                        _this.applicationEditModalImg.css("z-index", 1050);
                    }).removeAnimation();
                }
            });
            _this.applicationEditModalImg.css("z-index", 1030);
        }
        else {
            _this.hideApplicationEditModalImage();
        }
    };
    // appication Modal Attribute close click
    DashboardApp.prototype.closeApplicationEditModalImage = function () {
        var _this = this;
        _this.collectApplicationEditImage();
        if (_this.appImg.removed.length || !$.isEmptyObject(_this.appImg.modified) || _this.dashMyDropZone.validFileLength() > 0) {
            new Dialog().show("You have not save the changes, you want to discard the changes?")
                .addCloseBtn(function () {
                _this.applicationEditModalImg.css("z-index", 1050);
            }).setBtnClick(function () {
                _this.hideApplicationEditModalImage();
                _this.applicationEditModalImg.css("z-index", 1050);
            });
            _this.applicationEditModalImg.css("z-index", 1030);
            return;
        }
        _this.hideApplicationEditModalImage();
    };
    // appication Modal Image close click
    DashboardApp.prototype.hideApplicationEditModalImage = function () {
        var _this = this;
        // Destroy the dropzone
        this.dashMyDropZone.dropzone.destroy();
        $(this.imageUploadId).removeClass("dropzone");
        this.applicationEditModalImg.data("pro_id", "");
        this.applicationEditModalImg.modal('hide');
        this.appImg = { removed: [], modified: {} };
        this.appOldImg = {};
    };
    // Show the message how many image
    DashboardApp.prototype.setFileCount = function () {
        var count = this.dashMyDropZone.dropzone.options.maxFiles - this.dashMyDropZone.dropzone.files.length;
        count = count < 0 ? 0 : count;
        this.applicationEditModalImg.find("#file_count_message").text("You can upload " + count + " more images.");
    };
    DashboardApp.prototype.changePrppertyStatus = function (element) {
        var _this = this;
        var eleAddress = $(element).parent().parent().parent().parent().find("td").first().text();
        var button = $(element).parent().parent().find("button");
        var pro_id = button.data("id");
        var status = button.data("current-value") ? "true" : "false";
        var newStatus = $(element).data("new-value") ? "true" : "false";
        if (status !== newStatus) {
            new Dialog().show('Are you sure you want to <span class="text-primary">' + $(element).text() + "</span> " + eleAddress + " ?")
                .addCloseBtn(function () {
                _this.clickApplicationTab();
            }).setBtnClick(function () {
                var dialog = new Dialog({ disabledBtn: true, processBar: true }).show("Changing the <span class='text-primary>" + eleAddress + " status</span>");
                Query.sendRequest("POST", "/dashboard/changeProStatus", JSON.stringify({ id: pro_id, status: status, newStatus: newStatus }), function (result) {
                    if (result.status) {
                        dialog.changeText('Finished!');
                    }
                    else {
                        dialog.changeText('Error');
                    }
                    dialog.disabledBtn(false).setBtnClick(function () {
                        _this.clickApplicationTab();
                    }).removeAnimation();
                });
            });
        }
    };
    // Remove property
    DashboardApp.prototype.removeProperty = function (element) {
        var _this = this;
        var pro_id = $(element).data("id");
        var eleAddress = $(element).parent().parent().find("td").first().text();
        new Dialog().show('Are you sure you want to delete this property located in <br /><span class="text-primary">' + eleAddress + '</span> ?')
            .addCloseBtn().setBtnClick(function () {
            var dialog = new Dialog({ disabledBtn: true, processBar: true }).show('Deleting property, please do not close this window.');
            Query.sendRequest("POST", "/dashboard/remove", pro_id, function (result) {
                if (result.status) {
                    dialog.changeText('Property deleted!');
                }
                else {
                    dialog.changeText('Error');
                }
                dialog.disabledBtn(false).setBtnClick(function () {
                    _this.clickApplicationTab();
                }).removeAnimation();
            });
        });
    };
    // Sort select click
    DashboardApp.prototype.handleSortOptionClick = function () {
        var _this = this;
        $("#sort_by").change(function () {
            if (_this.applications.status) {
                _this.sortTable(_this);
            }
        });
    };
    // Sort table
    DashboardApp.prototype.sortTable = function (_this) {
        var sort = $("#sort_by").val();
        if (sort == 1)
            _this.table.sortBy(function (d) { return d.listFormatedDate; });
        if (sort == 2)
            _this.table.sortBy(function (d) { return -d.listFormatedDate; });
        if (sort == 3)
            _this.table.sortBy(function (d) { return d.avaFormatedDate; });
        if (sort == 4)
            _this.table.sortBy(function (d) { return -d.avaFormatedDate; });
        _this.table.redraw();
    };
    DashboardApp.prototype.clickApplicationTab = function () {
        $('#application_tab a').click();
    };
    return DashboardApp;
}());
var MatchTenants = /** @class */ (function () {
    function MatchTenants(id) {
        this.modal = $("#application_match_tenants_modal");
        this.tabs = this.modal.find("#match_tenants_tab");
        this.contents = this.modal.find("#match_tenants_contents");
        this.tanantsTab = this.modal.find("#tenants_tab a");
        this.tenantsContent = $(this.tanantsTab.data("target"));
        this.detailsTab = this.modal.find("#tenants_details_tab a");
        this.detailsContent = $(this.detailsTab.data("target"));
        this.currentDetail = [];
        this.newTabs = {};
        this.newContents = {};
        this.modal = $(id);
    }
    MatchTenants.prototype.show = function (ele) {
        var _this = this;
        _this.modal.modal('show');
        _this.tanantsTab.click();
        var pro_id = $(ele).data("id"), table = $("<table>", { "class": "col-12 table table-hover" }), thead = $("<thead>", { "class": "thead-light" }), tbody = $("<tbody>");
        _this.tenantsContent.append(table.append(thead).append(tbody));
        Query.sendRequest("POST", "/dashboard/matchTenants", pro_id, function (result) {
            if (result.status) {
                _this.currentDetail = result.data;
                thead.append($("<tr>").append($("<th>", { "scope": "col", text: "Name" }))
                    .append($("<th>", { "scope": "col", text: "Email" }))
                    .append($("<th>", { "scope": "col", text: "Mobile" }))
                    .append($("<th>", { "scope": "col", text: "Landline" })));
                _this.currentDetail.forEach(function (element) {
                    element.contentId = element.tenant_id + "_tenant";
                    element._nameLink = $("<a>", { href: "#", text: element.first_name + " " + element.last_name });
                    tbody.append($("<tr>").append($("<td>", { scope: "row" }).append(element._nameLink))
                        .append($("<td>", { text: element.email_address }))
                        .append($("<td>", { text: element.mobile_number }))
                        .append($("<td>", { text: element.landline_number })));
                    element._nameLink.click(function () {
                        if (_this.newTabs[element.contentId]) {
                            _this.newTabs[element.contentId].find("a").click();
                            return;
                        }
                        // console.log(element.tenant_id);
                        Query.sendRequest("POST", "/dashDocument/getAll", element.tenant_id, function (documents) {
                            if (documents.status) {
                                var newTab = $("<li>", { "class": "nav-item" }).append($("<a>", { "data-target": "#" + element.contentId, html: '<button class="close closeTab" type="button" >×</button>' + element._nameLink.text(), href: "#", "class": "nav-link small text-uppercase", "data-toggle": "tab" }));
                                var newContent = $("<div>", { id: element.contentId, "class": "col-12 tab-pane fade" });
                                _this.newTabs[element.contentId] = newTab;
                                ; //Save this new tab
                                _this.newContents[element.contentId] = newContent; //Save this new content
                                _this.tabs.append(newTab); //Append new tab
                                _this.contents.append(newContent); //Append new content
                                newTab.find("a").click(); //Click this new tab
                                // Register the close button click
                                newTab.find("button").click(function (e) {
                                    e.stopPropagation();
                                    if (newTab.find("a").hasClass("active")) {
                                        _this.tanantsTab.click();
                                        _this.tenantsContent.addClass("active show");
                                    }
                                    _this.newTabs[element.contentId].remove();
                                    _this.newContents[element.contentId].remove();
                                    delete _this.newTabs[element.contentId];
                                    delete _this.newContents[element.contentId];
                                });
                                if (!documents.data.length) {
                                    newContent.append("<span>No more data</span>");
                                    return;
                                }
                                var formatedDocs = {};
                                for (var i = 0; i < documents.data.length; i++) {
                                    if (!formatedDocs[documents.data[i].name]) {
                                        formatedDocs[documents.data[i].name] = { name: documents.data[i].name, values: [] };
                                    }
                                    formatedDocs[documents.data[i].name].values.push(documents.data[i]);
                                }
                                if (element.about_me) {
                                    newContent.append($("<div>", { "class": "row" })
                                        .append($("<span>", { "class": "col-2", text: "About me: " }))
                                        .append($("<span>", { "class": "col-10 word-wrap", text: element.about_me }))).append($("<hr>"));
                                }
                                for (var kKey in formatedDocs) {
                                    // console.log(formatedDocs[kKey]);
                                    var eleObject = formatedDocs[kKey];
                                    var values = formatedDocs[kKey].values;
                                    for (var v = 0; v < values.length; v++) {
                                        if (!eleObject.container) {
                                            if (kKey === "CV" || kKey === "Other") {
                                                eleObject.container = $("<div>", { "class": "row" })
                                                    .append($("<span>", { "class": "col-2", text: kKey + ":" }))
                                                    .append($("<ul>", { "class": "col-10 word-wrap" }));
                                            }
                                            else {
                                                eleObject.id = values[v].id;
                                                newContent.append($("<h5>", { text: kKey }));
                                                eleObject.container = $("<div>", { id: eleObject.id, "class": "carousel slide", "data-ride": "carousel", "data-interval": "false" })
                                                    .append($("<ol>", { "class": "carousel-indicators" }))
                                                    .append($("<div>", { "class": "carousel-inner" }))
                                                    .append($("<a>", { "class": "carousel-control-prev", href: "#" + eleObject.id, "data-slide": "prev", role: "button" })
                                                    .append($("<span>", { "class": "carousel-control-prev-icon", "aria-hidden": "true" }))
                                                    .append($("<span>", { "class": "sr-only", text: "Previous" })))
                                                    .append($("<a>", { "class": "carousel-control-next", href: "#" + eleObject.id, "data-slide": "next", role: "button" })
                                                    .append($("<span>", { "class": "carousel-control-next-icon", "aria-hidden": "true" }))
                                                    .append($("<span>", { "class": "sr-only", text: "Next" })));
                                            }
                                            newContent.append(eleObject.container, $("<hr>"));
                                        }
                                        if (kKey === "CV" || kKey === "Other") {
                                            eleObject.container.find("ul").append($("<li>").append($("<a>", { target: "_blank", href: values[v].url, text: kKey + " " + (v + 1) })));
                                        }
                                        else {
                                            eleObject.container.find("ol.carousel-indicators")
                                                .append($("<li>", { "data-target": "#" + eleObject.id, "data-slide-to": v, "class": (v == 0 ? "active" : "") }));
                                            var copyLink = $("<button>", { "class": "btn btn-light", text: "Copy link" });
                                            eleObject.container.find("div.carousel-inner")
                                                .append($("<div>", { "class": (v == 0 ? "carousel-item active" : "carousel-item") })
                                                .append($("<img>", { style: "height: 450px;", src: values[v].url, "class": "rounded img-fluid d-block" }))
                                                .append($("<div>", { "class": "carousel-caption d-none d-md-block" }).append(copyLink)));
                                            copyLink.click(function () {
                                                var url = $(this).parent().parent().find("img").attr("src");
                                                Utilities.copyToClipboard(url);
                                            });
                                        }
                                    }
                                }
                            }
                        });
                    });
                });
            }
        });
    };
    // Match tenants Modal close click
    MatchTenants.prototype.hide = function () {
        var _this = this;
        _this.tenantsContent.empty();
        _this.detailsContent.empty();
        _this.modal.modal('hide');
        [_this.newTabs, _this.newContents].forEach(function (aObject) {
            for (var kKey in aObject) {
                aObject[kKey].remove();
            }
        });
        _this.newTabs = {};
        _this.newContents = {};
        _this.currentDetail = [];
    };
    return MatchTenants;
}());
