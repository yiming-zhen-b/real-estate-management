var TenantPreferences = /** @class */ (function () {
    function TenantPreferences() {
        this.preferences = [];
        this.preferenceTab = $("#preference_tab a");
        this.preferenceContent = $(this.preferenceTab.data("target"));
        this.container = this.preferenceContent.find(".tenant_preference");
        var _this = this;
        _this.preferenceTab.click(function () {
            _this.container.empty();
            _this.table = $("<table>", { "class": "col-12 table table-hover" });
            _this.thead = $("<thead>", { "class": "thead-light" });
            _this.tbody = $("<tbody>");
            Query.sendRequest("POST", "/dashInfo/isLooking", "", function (data) {
                $("#preference_content input[name='is_looking']").prop('checked', data.is_looking === "true" ? true : false);
                $("input[name='is_looking']:checkbox").change(function () {
                    Query.sendRequest("POST", "/dashInfo/update", JSON.stringify({ is_looking: $(this).is(':checked') ? "true" : "false" }), function (data) {
                        if (data.status) {
                            new DialogSmall().show("Updated!");
                        }
                        else {
                            new DialogSmall().show("Error!");
                        }
                    });
                });
            });
            _this.thead.append($("<tr>").append($("<th>", { "scope": "col", text: "Region/District" }))
                .append($("<th>", { "scope": "col", text: "Town/City" }))
                .append($("<th>", { "scope": "col", text: "Suburb" }))
                .append($("<th>", { "scope": "col", "class": "fit" })
                .append($("<div>", { "class": "row" })
                .append($("<span>", { text: "Bedroom", "class": "col-12 text-center" }))
                .append($("<div>", { "class": "col-12" })
                .append($("<div>", { "class": "row" })
                .append($("<span>", { text: "Min", "class": "col-6 text-center" }))
                .append($("<span>", { text: "Max", "class": "col-6 text-center" }))))))
                .append($("<th>", { "scope": "col", text: "Max Rent($)", "class": "fit" }))
                .append($("<th>", { "scope": "col", text: "" })));
            Query.sendRequest("POST", "/dashPreference/getAll", "", function (data) {
                if (data.status) {
                    data.data.forEach(function (element) {
                        element.saved = true;
                        for (var eKey in element) {
                            if (!element[eKey])
                                element[eKey] = "";
                        }
                        _this.addPreference(element);
                    });
                    _this.container.append(_this.table.append(_this.thead).append(_this.tbody));
                }
                _this.addEmptyPreference();
                console.log(data.data);
            });
        });
    }
    TenantPreferences.prototype.addPreference = function (element) {
        var _this = this;
        _this.preferences.push(element);
        element.newData = {};
        element._tr = $("<tr>");
        element._region = $("<select>", { name: "region_id", "class": "form-control form-control-sm", "data-style": "btn-outline-secondary" });
        element._city = $("<select>", { name: "city_id", "class": "form-control form-control-sm", "data-style": "btn-outline-secondary" });
        element._suburb = $("<select>", { name: "suburb_id", "class": "form-control form-control-sm", "data-style": "btn-outline-secondary" });
        element._bedroom_min = $("<input>", { type: "number", name: "bedroom_min", min: 0, step: 1, value: element.bedroom_min, "class": "col-6 form-control form-control-sm" });
        element._bedroom_max = $("<input>", { type: "number", name: "bedroom_max", min: 0, step: 1, value: element.bedroom_max, "class": "col-6 form-control form-control-sm" });
        element._max_rent_pw = $("<input>", { type: "number", name: "max_rent_pw", min: 0, step: 1, value: element.max_rent_pw, "class": "form-control form-control-sm" });
        element._remove_button = $("<button>", { text: "Remove", "class": "btn btn-primary btn-cus " + (element.saved ? "" : "d-none") });
        element._update_button = $("<button>", { text: "Update", "class": "btn btn-secondary btn-cus d-none" });
        element._save_button = $("<button>", { text: "Save", "class": "btn btn-secondary btn-cus " + (element.saved ? "d-none" : "") });
        this.tbody.append(element._tr.append($("<td>", { scope: "row" }).append(element._region))
            .append($("<td>").append(element._city))
            .append($("<td>").append(element._suburb))
            .append($("<td>", { "class": "row" }).append(element._bedroom_min).append(element._bedroom_max))
            .append($("<td>").append(element._max_rent_pw))
            .append($("<td>").append(element._remove_button).append(element._save_button).append(element._update_button)));
        Query.sendRequest("GET", "/getPropertyAttr/getRegion", "", function (data) {
            element._region.append($("<option>", { value: "", text: "All region", selected: (element.region_id ? false : true) }));
            data.forEach(function (v) {
                element._region.append($("<option>", { value: v.region_id, text: v.region_name, selected: (v.region_id === element.region_id ? true : false) }));
            });
        });
        fillPropertyAttr(element._city, "/getCity", "city_id", "city_name", "All City", element.region_id);
        fillPropertyAttr(element._suburb, "/getSuburb", "suburb_id", "suburb_name", "All Suburb", element.city_id);
        element._region.change(function () {
            $(this).removeClass("bg-warning");
            checkValue($(this));
            resetSelectAndCheckValue(element._city, "All City");
            resetSelectAndCheckValue(element._suburb, "All Suburb");
            fillPropertyAttr(element._city, "/getCity", "city_id", "city_name", "All City", $(this).val());
            fillPropertyAttr(element._suburb, "/getSuburb", "suburb_id", "suburb_name", "All Suburb", "");
        });
        element._city.change(function () {
            checkValue($(this));
            resetSelectAndCheckValue(element._suburb, "All Suburb");
            fillPropertyAttr(element._suburb, "/getSuburb", "suburb_id", "suburb_name", "All Suburb", $(this).val());
        });
        element._suburb.change(function () {
            checkValue($(this));
        });
        element._bedroom_min.on("input", function () {
            checkValue($(this));
        });
        element._bedroom_max.on("input", function () {
            checkValue($(this));
        });
        element._max_rent_pw.on("input", function () {
            checkValue($(this));
        });
        element._remove_button.click(function () {
            Query.sendRequest("POST", "/dashPreference/remove", element.preference_id, function (data) {
                if (data.status) {
                    _this.preferences = _this.preferences.filter(function (value, index, arr) {
                        return value.preference_id != element.preference_id;
                    });
                    element._tr.remove();
                    new DialogSmall().show("Removed!");
                }
                else {
                    new DialogSmall().show("Error!");
                }
            });
        });
        element._update_button.click(function () {
            if (!element._region.val()) {
                element._region.addClass("bg-warning");
            }
            else {
                if (!$.isEmptyObject(element.newData)) {
                    var nData = JSON.parse(JSON.stringify(element.newData));
                    nData.preference_id = element.preference_id;
                    Query.sendRequest("POST", "/dashPreference/update", JSON.stringify(nData), function (data) {
                        if (data.status) {
                            element._remove_button.removeClass("d-none");
                            element._update_button.addClass("d-none");
                            element.region_id = element._region.val();
                            element.city_id = element._city.val();
                            element.suburb_id = element._suburb.val();
                            element.newData = {};
                            new DialogSmall().show("Updated!");
                        }
                        else {
                            new DialogSmall().show("Error!");
                        }
                    });
                }
            }
        });
        element._save_button.click(function () {
            // if (element._region.val() == 0 && element._city.val() == 0 && element._suburb.val() == 0 ) {
            //     [element._region, element._city, element._suburb].forEach(aEle => {
            //         if (aEle.val() == 0) {
            //             aEle.addClass("bg-warning");
            //             return;
            //         }
            //     });
            // } 
            if (!element._region.val()) {
                element._region.addClass("bg-warning");
            }
            else {
                var nData = {};
                [element._region, element._city, element._suburb, element._bedroom_min, element._bedroom_max, element._max_rent_pw].forEach(function (aEle) {
                    nData[aEle.attr("name")] = aEle.val();
                    element[aEle.attr("name")] = aEle.val();
                });
                Query.sendRequest("POST", "/dashPreference/save", JSON.stringify(nData), function (data) {
                    if (data.status) {
                        _this.addEmptyPreference();
                        element.saved = true;
                        element.preference_id = data.preference_id;
                        element._remove_button.removeClass("d-none");
                        element._save_button.addClass("d-none");
                        element._region.removeClass("bg-warning");
                        new DialogSmall().show("Saved!");
                    }
                });
            }
        });
        function removeWarring() {
            [element._region, element._city, element._suburb].forEach(function (aEle) { aEle.removeClass("bg-warning"); });
        }
        function resetSelectAndCheckValue(elementObject, defaultText) {
            elementObject.prop("disabled", true);
            elementObject.empty();
            elementObject.append($("<option>", { value: "", text: defaultText, selected: true }));
            checkValue(elementObject);
        }
        function fillPropertyAttr(elementObject, api, vKey, tkey, defaultText, queryVal) {
            if (queryVal) {
                elementObject.empty();
                Query.sendRequest("GET", "/getPropertyAttr" + api, queryVal, function (data) {
                    elementObject.append($("<option>", { value: "", text: defaultText, selected: true }));
                    elementObject.prop("disabled", false);
                    data.forEach(function (v) {
                        elementObject.append($("<option>", { value: v[vKey], text: v[tkey], selected: (v[vKey] === element[vKey] ? true : false) }));
                    });
                });
            }
            else {
                resetSelectAndCheckValue(elementObject, defaultText);
            }
        }
        function checkValue(ele) {
            if (element.saved) {
                var key = ele.attr("name");
                var value = ele.val();
                if (element[key] != value) {
                    element.newData[key] = value;
                }
                else {
                    delete element.newData[key];
                }
                if ($.isEmptyObject(element.newData)) {
                    element._remove_button.removeClass("d-none");
                    element._update_button.addClass("d-none");
                }
                else {
                    element._remove_button.addClass("d-none");
                    element._update_button.removeClass("d-none");
                }
            }
        }
    };
    TenantPreferences.prototype.addEmptyPreference = function () {
        var _this = this;
        this.addPreference({
            bedroom_max: "",
            bedroom_min: "",
            city_id: "",
            max_rent_pw: "",
            preference_id: "",
            region_id: "",
            suburb_id: "",
            saved: false
        });
    };
    return TenantPreferences;
}());
