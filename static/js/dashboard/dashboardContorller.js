var dashboardApp, dashboardPreferences, matchTenants;
nav.checkLoginStatus(function (data) {
    if (!data.status) {
        new Dialog().show(data.message).setBtnClick(function () { Query.redirect("/login.html"); });
    }
    else {
        if (data.role.is_agent || data.role.is_landlord) {
            $(".agent_div").removeClass("d-none");
            dashboardApp = new DashboardApp();
            matchTenants = new MatchTenants("#application_match_tenants_modal");
            dashboardApp.handleApplicationTabClick();
            dashboardApp.clickApplicationTab();
            dashboardApp.handleSortOptionClick();
            var dashboardInfo = new DashboardInfo({
                createUserInfo: function (oldData) {
                    // $("#info_content #agent_div .input_agent_radio_data").each(function () {
                    //     if (oldData[$(this).attr("name")]) {
                    //         $("#info_content input[name='" + $(this).attr("name") + "'][value='" + oldData[$(this).attr("name")] + "']").prop('checked', true);
                    //         var checkField = $('input' + $(this).data("check-field"));
                    //         checkField.val(oldData[checkField.attr("name")]);
                    //     }
                    // });
                    $("#on_hold").text(oldData.on_hold === "true" ? "Pendding" : "Approved");
                },
                saveUserInfo: function (newData) {
                    // $("#info_content #agent_div .input_agent_radio_data").each(function () {
                    //     if (newData[$(this).attr("name")]) {
                    //         var checkedVal = $("#info_content input[name='" + $(this).attr("name") + "']:checked").val();
                    //         var checkField = $('input' + $(this).data("check-field"));
                    //         if (checkedVal === "true" && !checkField.val()) {
                    //             $(checkField.data("error-field")).addClass("bg-warning");
                    //             return;
                    //         }
                    //         dashboardInfo.checkValue($(this).attr("name"), checkedVal);
                    //         dashboardInfo.checkValue(checkField.attr("name"), checkField.val());
                    //         checkField.attr("disabled", true);
                    //         $("#info_content input[name='" + $(this).attr("name") + "']").attr("disabled", true);
                    //         $(checkField.data("error-field")).removeClass("bg-warning");
                    //     }
                    // });
                },
                ini: function () {
                    // $("input[name='is_agent']:radio").change(function () {
                    //     var element = $(this);
                    //     if (element.is(':checked')) {
                    //         var checkFieldId = element.data("check-field");
                    //         if (element.val() === "true") {
                    //             $(checkFieldId).data("required", true);
                    //         } else {
                    //             $(checkFieldId).data("required", false);
                    //             $(checkFieldId).val("");
                    //             $($(checkFieldId).data("error-field")).removeClass("bg-warning");
                    //         }
                    //     }
                    // });
                }
            });
        }
        if (data.role.is_tenant) {
            $(".tenant_div").removeClass("d-none");
            var tenantInfoDiv = $(".tenant_info_div");
            var dashboardInfo_1 = new DashboardInfo({
                createUserInfo: function (oldData) {
                    Query.sendRequest("POST", "/dashInfo/getAllAccessCon", "", function (data) {
                        var select = tenantInfoDiv.find("#access_control_select");
                        select.empty();
                        data.forEach(function (v) {
                            select.append($("<option>", { value: v.id, text: v.des }));
                        });
                        select.val(oldData.access_control);
                        select.selectpicker("refresh");
                        select.change(function () {
                            dashboardInfo_1.checkValue($(this).attr("name"), $(this).val());
                        });
                    });
                },
                saveUserInfo: function () {
                },
                ini: function () {
                }
            });
            var dashboardDocument = new DashboardDocument();
            dashboardPreferences = new TenantPreferences();
        }
        var dashboardSecurity = new DashboardSecurity();
    }
});
