var DashboardInfo = /** @class */ (function () {
    function DashboardInfo(callback) {
        this.infoContentSaveBtn = $("#info_content_save_btn");
        this.callback = { createUserInfo: null, saveUserInfo: null, ini: null };
        var _this = this;
        _this.callback = callback;
        if (_this.callback.ini)
            _this.callback.ini();
        // $(".info_input_data").change(function() {
        //     _this.checkValue($(this).attr("name"), $(this).val());
        // });
        $(".info_input_data").on("input", function () {
            _this.checkValue($(this).attr("name"), $(this).val());
        });
        $('#info_tab a').click(function () {
            _this.newInfoData = {};
            _this.infoContentSaveBtn.attr("disabled", true);
            _this.createUserInfo();
        });
        $('#info_content_save_btn').click(function () {
            if (_this.callback.saveUserInfo)
                _this.callback.saveUserInfo(_this.newInfoData);
            if (!$.isEmptyObject(_this.newInfoData)) {
                Query.sendRequest("POST", "/dashInfo/update", JSON.stringify(_this.newInfoData), function (data) {
                    if (data.status) {
                        new Dialog().show("Uploaded!").setBtnClick(null);
                        $("#info_tab a").click();
                    }
                });
            }
        });
    }
    DashboardInfo.prototype.createUserInfo = function () {
        var _this = this;
        Query.sendRequest("POST", "/dashInfo/get", "", function (data) {
            if (data.status) {
                _this.oldInfoData = data.data[0];
                console.log(_this.oldInfoData);
                $("#info_content .info_input_data").each(function () {
                    $(this).val(_this.oldInfoData[$(this).attr("name")]);
                });
                if (_this.callback.createUserInfo)
                    _this.callback.createUserInfo(_this.oldInfoData);
                $("#info_email").text(_this.oldInfoData.email_address);
            }
        });
    };
    DashboardInfo.prototype.checkValue = function (key, value) {
        if (this.oldInfoData[key] != value) {
            this.newInfoData[key] = value;
        }
        else {
            delete this.newInfoData[key];
        }
        if ($.isEmptyObject(this.newInfoData)) {
            this.infoContentSaveBtn.attr("disabled", true);
        }
        else {
            this.infoContentSaveBtn.attr("disabled", false);
        }
    };
    return DashboardInfo;
}());
