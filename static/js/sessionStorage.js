var checkedLoginStatus = false;
var SessionStorage;
(function (SessionStorage) {
    function setItem(data) {
        if (checkSessionStorage) {
            for (var key in data) {
                sessionStorage.setItem(key, data[key]);
            }
        }
    }
    SessionStorage.setItem = setItem;
    function isEmpty() {
        return sessionStorage.length == 0;
    }
    SessionStorage.isEmpty = isEmpty;
    function hasItem(key) {
        return sessionStorage.hasOwnProperty(key);
    }
    SessionStorage.hasItem = hasItem;
    function getItem(name) {
        return sessionStorage.getItem(name);
    }
    SessionStorage.getItem = getItem;
    function removeItem(name) {
        sessionStorage.removeItem(name);
    }
    SessionStorage.removeItem = removeItem;
    function checkSessionStorage() {
        if (typeof (Storage) !== "undefined") {
            return true;
        }
        else {
            alert("Error");
        }
    }
    SessionStorage.checkSessionStorage = checkSessionStorage;
})(SessionStorage || (SessionStorage = {}));
shareSessionStorageBetweenTabs();
function shareSessionStorageBetweenTabs() {
    if (!sessionStorage.length && SessionStorage.checkSessionStorage()) {
        // Ask other tabs for session storage
        localStorage.setItem('getSessionStorage', Date.now().toString());
    }
    window.addEventListener('storage', function (event) {
        // if (event.key == 'sessionStorage')
        //     console.log('storage event', event);
        if (event.key == 'getSessionStorage') {
            // Some tab asked for the sessionStorage -> send it
            localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
            localStorage.removeItem('sessionStorage');
        }
        else if (event.key == 'sessionStorage' && !sessionStorage.length) {
            // sessionStorage is empty -> fill it
            var data = JSON.parse(event.newValue);
            for (var key in data) {
                sessionStorage.setItem(key, data[key]);
            }
            localStorage.removeItem('getSessionStorage');
            var callback = null;
            if (window.location.pathname === "/listingProperty.html") {
                callback = function (data) {
                    if (!data.status) {
                        new Dialog().show(data.message).setBtnClick(function () { Query.redirect("/login.html"); });
                    }
                    else {
                        $(".modal-backdrop").remove();
                        $(".modal").remove();
                        if (!data.role.is_agent && !data.role.is_landlord) {
                            new Dialog().show("Please wait for registration approval").setBtnClick(function () { Query.redirect("/"); });
                        }
                    }
                };
            }
            if (window.location.pathname === "/login.html") {
                callback = function (data) {
                    if (data.status) {
                        Query.redirect("/");
                    }
                };
            }
            nav.checkLoginStatus(callback);
        }
    });
}
