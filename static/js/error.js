var _a;
var Err = /** @class */ (function () {
    function Err() {
    }
    Err.prototype.getImg = function () {
        Query.sendRequest("POST", "/staticImg/getFOFI", "", function (data) {
            if (data) {
                $(".notfound-404 h1").css("background-image", "url('" + data + "')");
            }
        });
    };
    Err.prototype.handleGoToHomePageClick = function () {
        $("#go_to_home_page").on("click", function () {
            Query.redirect("/");
        });
    };
    return Err;
}());
var err = new Err();
err.getImg();
err.handleGoToHomePageClick();
SessionStorage.setItem((_a = {}, _a[Config.sessionKey.donotGoBack] = true, _a));
