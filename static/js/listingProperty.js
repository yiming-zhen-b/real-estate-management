var ListingProperty = /** @class */ (function () {
    function ListingProperty() {
        this.propertyValues = {};
        var _this = this;
        _this.listMyDropZone = new MyDropZone("div#upload_images");
        // get property types
        Query.sendRequest("GET", "/getPropertyAttr/getPropertyType", "", function (data) {
            var propertyType = $("#listing-property-type");
            data.forEach(function (v) {
                propertyType.append($("<a>", {
                    "class": "list-group-item list-group-item-action",
                    "data-toggle": "list",
                    "data-value": v.category_id,
                    href: "",
                    text: v.category_name
                }));
            });
        });
        Query.sendRequest("POST", "/dashInfo/getUserDetail", "", function (data) {
            console.log(data);
            if (data[0].mobile_number) {
                $("#mobile-phone-number").val(data[0].mobile_number);
            }
            else {
                setupShowPhoneNumberCheckbox("input[name='show_mobile']:checkbox");
            }
            if (data[0].landline_number) {
                $("#landline-phone-number").val(data[0].landline_number);
            }
            else {
                setupShowPhoneNumberCheckbox("input[name='show_landline']:checkbox");
            }
            if (data[0].is_agent == "true") {
                $("input[name='is_agent'][value='true']").prop('checked', true);
                $("#licence_number").val(data[0].licence_number);
            }
            else {
                $("input[name='is_agent']:radio").attr("disabled", false);
                $("input[name='is_agent']:radio").change(function () {
                    var element = $(this);
                    if (element.is(':checked')) {
                        var checkFieldId = element.data("check-field").split(",");
                        if (element.val() === "true") {
                            for (var id in checkFieldId) {
                                $(checkFieldId[id]).data("required", true);
                                $(checkFieldId[id]).prop("disabled", false);
                            }
                        }
                        else {
                            for (var id in checkFieldId) {
                                $(checkFieldId[id]).data("required", false);
                                $(checkFieldId[id]).prop("disabled", true);
                                $(checkFieldId[id]).val("");
                                $($(checkFieldId[id]).data("error-field")).removeClass("bg-warning");
                            }
                        }
                    }
                });
            }
            function setupShowPhoneNumberCheckbox(ele) {
                $(ele).change(function () {
                    var element = $(this);
                    var checkFieldId = element.data("check-field");
                    if (element.is(':checked')) {
                        $(checkFieldId).data("required", true);
                        $($(checkFieldId)).attr("disabled", false);
                    }
                    else {
                        $(checkFieldId).data("required", false);
                        $($(checkFieldId)).attr("disabled", true);
                        $($(checkFieldId)).val("");
                        $($(checkFieldId).data("error-field")).removeClass("bg-warning");
                    }
                });
            }
        });
        $('#datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    }
    ListingProperty.prototype.nextBtnClick = function () {
        var _this = this;
        $('#list_pro_next_btn').click(function () {
            var currentTab = $("#list-pro-tabs a.nav-link.small.text-uppercase.active");
            if (_this.checkListingPropertyValues(currentTab)) {
                $("div#list-pro-tabs-content div.tab-pane.fade").removeClass("active show");
                var nextTab = $(currentTab.data("next-tab-target") + " a");
                nextTab.removeClass("disabled");
                nextTab.click();
                if (nextTab.parent().attr("id") == "confirm_tab") {
                    _this.fillConfirmTabContent();
                }
            }
            // console.log(_this.propertyValues);
            // _this.uploadProperty();
        });
    };
    ListingProperty.prototype.uploadProperty = function () {
        var _this = this;
        var dialog = new Dialog({ disabledBtn: true, processBar: true }).show('Uploading property, please do not close this window.');
        console.log(_this.propertyValues);
        Query.sendRequest("POST", "/listingProperty/upload", JSON.stringify(_this.propertyValues), function (data) {
            console.log(data);
            if (data.status == true) {
                dialog.changeText("Uploading images, please do not close this window.");
                _this.listMyDropZone.uploadImage(data.data, {
                    success: function () {
                        if (data.show_matched_tenants) {
                            dialog.setBtnText("Check it out!");
                            showFinalMessage(data.message + "<p>This property has " + data.matchedTenants + " matched tenants.", function () {
                                SessionStorage.setItem({ show_matched_tenants: data.data });
                                Query.redirect("/dashboard.html");
                            });
                        }
                        else {
                            showFinalMessage(data.message, function () { Query.redirect("/"); });
                        }
                    }
                });
            }
            else {
                if (data.login_alert) {
                    showFinalMessage(data.message, function () { Query.redirect("/login.html"); });
                }
                else {
                    showFinalMessage(data.message, function () { Query.redirect("/"); });
                }
            }
        });
        function showFinalMessage(message, callback) {
            dialog.changeText(message).addCloseBtn(function () { Query.redirect("/listingProperty.html"); }, "Upload new one")
                .disabledBtn(false).setBtnClick(callback).removeAnimation();
        }
    };
    ListingProperty.prototype.checkListingPropertyValues = function (currentTab) {
        var tabId = currentTab.parent()[0].id;
        switch (tabId) {
            case "category_tab":
                return this.savePropertyType();
            case "details_tab":
                return this.savePropertyDetail();
            case "photos_tab":
                return this.savePropertyVideoLinks();
            case "extras_tab":
                return this.savePropertyExtras();
            case "confirm_tab":
                this.uploadProperty();
                return;
            default:
                return true;
        }
    };
    ListingProperty.prototype.savePropertyType = function () {
        var _this = this, retValue = true;
        ["#listing-property-for", "#listing-property-type"].forEach(function (ele) {
            var parent = $(ele), child = parent.find("a.active");
            if (child.length === 1) {
                _this.propertyValues[parent.attr("name")] = child.data("value");
                $(parent.data("error-field")).removeClass("bg-warning");
            }
            else {
                $(parent.data("error-field")).addClass("bg-warning");
                retValue = false;
            }
        });
        return retValue;
    };
    ListingProperty.prototype.savePropertyDetail = function () {
        var _this = this;
        // Collect the text input and textarea values
        var retValue = _this.saveTextInput(["#details_content .property_text_data", "#details_content .property_textarea_data"], _this);
        // Collect the radio values
        $("#details_content .property_radio_data").each(function () {
            _this.propertyValues[$(this).attr("name")] = $("#details_content input[name='" + $(this).attr("name") + "']:checked").val();
        });
        return retValue;
    };
    ListingProperty.prototype.savePropertyVideoLinks = function () {
        var _this = this;
        return _this.saveTextInput(["#photos_content .property_text_data", "#photos_content .property_textarea_data"], _this);
    };
    ListingProperty.prototype.saveTextInput = function (elements, _this) {
        var retValue = true;
        elements.forEach(function (ele) {
            $(ele).each(function () {
                if ($(this).data("required") == true && !$(this).val()) {
                    $($(this).data("error-field")).addClass("bg-warning");
                    retValue = false;
                }
                else {
                    $($(this).data("error-field")).removeClass("bg-warning");
                }
                _this.propertyValues[$(this).attr("name")] = $(this).val();
            });
        });
        return retValue;
    };
    ListingProperty.prototype.savePropertyExtras = function () {
        var _this = this;
        var extras = [];
        $.each($("input[name='extra-checkbox']:checked"), function () {
            extras.push($(this).val());
        });
        _this["_extras"] = extras;
        return true;
    };
    ListingProperty.prototype.fillConfirmTabContent = function () {
        var _this = this;
        var confirmContent = $("#confirm_content");
        for (var vKey in _this.propertyValues) {
            confirmContent.find("." + vKey).text(_this.propertyValues[vKey]);
        }
        confirmContent.find(".region-city-suburb").text([_this.propertyValues["suburb"], _this.propertyValues["city"], _this.propertyValues["region"]].join(", "));
        confirmContent.find(".street-number").text([_this.propertyValues["street_number"], _this.propertyValues["unit_flat_number"], _this.propertyValues["street_name"]].join(", "));
        confirmContent.find(".phone-display").text("");
        [{ checkKey: "show_landline", valueKey: "landline_number", text: "Landline:" },
            { checkKey: "show_mobile", valueKey: "mobile_number", text: "Mobile:" }
        ].forEach(function (ele) {
            if (_this.propertyValues[ele.checkKey] == "true") {
                var row = $("<div>", { "class": "row" });
                row.append($("<span>", { "class": "col-4", text: ele.text }));
                row.append($("<span>", { "class": "col-8", text: _this.propertyValues[ele.valueKey] }));
                confirmContent.find(".phone-display").append(row);
            }
        });
    };
    ListingProperty.prototype.handleTabsClick = function () {
        $("#list-pro-tabs a").on("click", function () {
            var currentChildIndex = $("#list-pro-tabs a").index($(this));
            var allTabs = $("#list-pro-tabs a");
            for (var i = currentChildIndex + 1; i < allTabs.length; i++) {
                $(allTabs[i]).addClass("disabled");
            }
        });
    };
    return ListingProperty;
}());
var listingPro = new ListingProperty();
listingPro.handleTabsClick();
listingPro.nextBtnClick();
$(document).ready(function () {
    $('#rent-per-week').val("120");
    $('#street-number').val("24");
    $('#unit-flat-number').val("D");
    $('#street-name').val("Hogan St");
    $('#region').val("Waikato");
    $('#city').val("Hamilton");
    $('#suburb').val("Hamilton East");
    $('#available-from').val("11/11/2018");
    $('#bedrooms').val("5");
    $('#bathrooms').val("2");
});
nav.checkLoginStatus(function (data) {
    if (!data.status) {
        new Dialog().show(data.message).setBtnClick(function () { Query.redirect("/login.html"); });
    }
    else {
        if (!data.role.is_agent && !data.role.is_landlord) {
            new Dialog().show("Please wait for registration approval").setBtnClick(function () { Query.redirect("/"); });
        }
    }
});
