var Home = /** @class */ (function () {
    function Home() {
    }
    Home.prototype.getSliderImg = function () {
        // <div class="carousel-item active" style="background-image: url('http://localhost:8088/simg/home/s/s1.jpg')">
        //     <div class="carousel-caption d-none d-md-block">
        //         <h3>First Slide</h3>
        //         <p>This is a description for the first slide.</p>
        //     </div>
        // </div>
        Query.sendRequest("POST", "/staticImg/getHSI", "", function (data) {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $(".home_slider_header .carousel-indicators")
                        .append($("<li>", { "data-target": "#carouselExampleIndicators", "data-slide-to": i, "class": i === 0 ? "active" : "" }));
                    $(".home_slider_header .carousel-inner")
                        .append($("<div>", { style: "background-image: url('" + data[i] + "')", "class": i === 0 ? "carousel-item active" : "carousel-item" })
                        .append($("<div>")));
                }
            }
        });
    };
    Home.prototype.getBodyImg = function () {
        Query.sendRequest("POST", "/staticImg/getHBI", "", function (data) {
            if (data.length > 0) {
                data.sort();
                $(".showcase .showcase-img").each(function (index) {
                    $(this).css("background-image", "url('" + data[index] + "')");
                });
            }
        });
    };
    Home.prototype.handleSearch = function () {
        var _this = this;
        var searchResult = $('#search_result');
        $("#search_input").keyup(function () {
            searchResult.empty();
            searchResult.css("display", "");
            var val = $(this).val();
            if (val.length > 2) {
                console.log(val);
                Query.sendRequest("POST", "/home/fuzzySearch", val, function (result) {
                    if (result.status) {
                        searchResult.css("display", "block");
                        for (var key in result.data) {
                            var value = result.data[key];
                            for (var i = 0; i < value.length; i++) {
                                var li = $($("<li>", { "class": "search_result_li list-group-item list-group-item-action cursor_pointer", "data-value": JSON.stringify(value[i]) }));
                                var vKey = Utilities.getJSONKey(value[i]);
                                var rest = [];
                                for (var v = 0; v < vKey.length; v++) {
                                    if (v === 0) {
                                        li.append($("<span>", { "class": "row font-weight-bold", text: value[i][vKey[v]] }));
                                    }
                                    else {
                                        rest.push(value[i][vKey[v]]);
                                    }
                                }
                                searchResult.append(li.append($("<span>", { "class": "row", text: rest.join(", ") })));
                            }
                        }
                        _this.handleSearchResultClick();
                    }
                });
            }
        })
            .focusout(function () {
            searchResult.css("display", "");
        })
            .focus(function () {
            if (searchResult.children().length > 0) {
                searchResult.css("display", "block");
            }
        });
        $("#search_button").on("click", function () {
        });
    };
    Home.prototype.handleSearchResultClick = function () {
        $(".search_result_li").mousedown(function () { Query.redirectNewTab("/search.html", $(this).data("value")); });
    };
    return Home;
}());
var home = new Home();
home.getSliderImg();
home.getBodyImg();
home.handleSearch();
